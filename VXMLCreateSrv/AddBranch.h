///************************************************************************/
///* Name     : VXMLMenuParser\AddBranch.h                                */
///* Author   : Andrey Alekseev                                           */
///* Project  : VXMLCreateSrv                                             */
///* Company  : Forte-CT                                                  */
///* Date     : 02 Set 2015                                               */
///************************************************************************/
#pragma once
//
#include "..\ado\ADODatabase.h"
//#include "logger.h"
#include "Log/SystemLog.h"
#include <string>

//typedef struct Content_RecordSet
//{
//	_variant_t vTreeId;
//	_variant_t vParentId;
//	_variant_t vMenuName;
//	_variant_t vMenuType;
//	_variant_t vMenuText;
//	_variant_t vDtmf;
//	_variant_t vCodeName;
//	_variant_t vCode;
//	_variant_t vComment;
//	_variant_t vWavName;
//	_variant_t vWavText;
//	_variant_t vSubDialogId;
//	_variant_t vIsVisible;
//	_variant_t vRepeatCnt;
//	_variant_t vGotoRepeat;
//	_variant_t vGotoBack;
//	_variant_t vGoto;
//	_variant_t vCityId;
//	_variant_t vWavBreak;
//	_variant_t vDtmfWait;
//	_variant_t vGotoRoot;
//} Content_RecordSet_type;

class CAddBranch : public CADOStoredProc
{
public:
	CAddBranch(const std::string& _connectionString);
	virtual ~CAddBranch();

	bool AdAllParam();
	bool SetAllParam(
		const int& iTreeId,
		const int& iParentId,
		const std::wstring& sMenuName,
		const int& iMenuType,
		const std::wstring& sMenuText,
		const std::wstring& sDtmf,
		const std::wstring& sCodeName,
		const std::wstring& sCode,
		const std::wstring& sComment,
		const std::wstring& sWavName,
		const std::wstring& sWavText,
		const int& iSubDialogId,
		const bool& bIsVisible,
		const int& iRepeatCnt,
		const std::wstring& sGotoRepeat,
		const std::wstring& sGotoBack,
		const std::wstring& sGoto,
		const int& iCityId,
		const std::wstring& sWavBreak,
		const int& iDtmfWait,
		const std::wstring& sGotoRoot,
		const std::wstring& sTermChar,
		const int& iTimeOut,
		const bool& bIsRoot,
		const bool& bIsCondition
		);

	bool Connect(CADODatabase *pDB = NULL);


	int Call();

	//bool GetNextRecordSet(Content_RecordSet_type &recSet);

private:
	std::string m_connectionString;
};
//
///******************************* eof *************************************/