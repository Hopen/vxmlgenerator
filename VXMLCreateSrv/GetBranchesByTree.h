/************************************************************************/
/* Name     : VXMLCreateSrv\GetBranchesByTree.h                         */
/* Author   : Andrey Alekseev                                           */
/* Project  : VXMLCreateSrv                                             */
/* Company  : Forte-CT                                                  */
/* Date     : 05 Aug 2015                                               */
/************************************************************************/
#pragma once

#include "..\ado\ADODatabase.h"
//#include "logger.h"
#include "Configuration\SystemConfiguration.h"
#include "Log/SystemLog.h"

typedef struct Content_RecordSet
{
	_variant_t vId;
	_variant_t vTreeId;
	_variant_t vParentId;
	_variant_t vMenuName;
	_variant_t vMenuType;
	_variant_t vMenuText;
	_variant_t vDtmf;
	_variant_t vCodeName;
	_variant_t vCode;
	_variant_t vComment;
	_variant_t vWavName;
	_variant_t vWavText;
	_variant_t vSubDialogId;
	_variant_t vIsVisible;
	_variant_t vRepeatCnt;
	_variant_t vGotoRepeat;
	_variant_t vGotoBack;
	_variant_t vGoto;
	_variant_t vCityId;
	_variant_t vWavBreak;
	_variant_t vDtmfWait;
	_variant_t vGotoRoot;
	_variant_t vTermChar;
	_variant_t vTimeOut;
	_variant_t vRoot;
	_variant_t vCondition;
	_variant_t vAddToHistory;
	_variant_t vFile1;
	_variant_t vFile2;
	_variant_t vFile3;
	_variant_t vFile4;
	_variant_t vFileText1;
	_variant_t vFileText2;
	_variant_t vFileText3;
	_variant_t vFileText4;
	_variant_t vFuncName;
	_variant_t vFuncType;
} Content_RecordSet_type;

class CGetBranchesByTree : public CADOStoredProc
{
public:
	CGetBranchesByTree(CSystemLog * pLog, const std::string& _connectionString);
	virtual ~CGetBranchesByTree();

	bool AdAllParam();
	bool SetAllParam(
		const int& _tree_id
		);

	bool Connect(CADODatabase *pDB = NULL);


	int Call();

	bool GetNextRecordSet(Content_RecordSet_type &recSet);

private:
	CSystemLog * m_pLog;
	std::string m_connectionString;
};

/******************************* eof *************************************/