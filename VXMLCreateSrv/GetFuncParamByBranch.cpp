/************************************************************************/
/* Name     : VXMLCreateSrv\GetFuncParamByBranch.cpp                    */
/* Author   : Andrey Alekseev                                           */
/* Project  : VXMLCreateSrv                                             */
/* Company  : Expert Solutions                                          */
/* Date     : 23 Dec 2015                                               */
/************************************************************************/

#include "stdafx.h"
#include "GetFuncParamByBranch.h"
#include "smartADODataBase.h"

CGetFuncParamByBranch::CGetFuncParamByBranch(const std::string& _connectionString)
	: m_connectionString(_connectionString)
{
}


CGetFuncParamByBranch::~CGetFuncParamByBranch()
{
}

int CGetFuncParamByBranch::Call()
{

	if (Execute()) {
		//return GetRcParam();
		return 0;
	}
	return -1;
}

bool CGetFuncParamByBranch::AdAllParam()
{
	bool bRc = true;
	//bRc &= AdParam( _T("RETURN_VALUE"),    adInteger, adParamReturnValue, 4);

	bRc &= AdParam(_T("branch_id"), adInteger, adParamInput, 11);
	return bRc;

}


bool CGetFuncParamByBranch::SetAllParam(
	const int& _branch_id)
{
	singleton_auto_pointer<CSystemLog> log;

	log->LogString(LEVEL_INFO, L"SetAllParam");

	bool bRc = true;
	bRc &= SetParam(_T("branch_id"), _branch_id);

	return bRc;

}


bool CGetFuncParamByBranch::Connect(CADODatabase *pDB /* = NULL */)
{
	singleton_auto_pointer<CSystemLog> log;
	try
	{
		return __super::Connect(L"get_func_param_by_branch", m_connectionString.c_str(), pDB);
	}
	catch (_com_error& err)
	{
		log->LogString(LEVEL_INFO, L"Connect to database failed with error code: 0x%p : %s ", err.Error(), err.Description().GetBSTR());
	}
	catch (...)
	{
		log->LogString(LEVEL_INFO, L"Unknown exception when try to connect to database");
	}
	return false;
}

bool CGetFuncParamByBranch::GetNextRecordSet(FuncParam_RecordSet_type &recSet)
{
	if (m_pRS) {
		singleton_auto_pointer<CSystemLog> log;

		//m_pLog->LogString(LEVEL_INFO, L"record set count = %i", m_pRS->Fields->GetCount());

		if (m_pRS->EndOfFile != VARIANT_TRUE) {
			try {
				recSet.vParamName  = GetValue(_T("param_name"), L"");
				recSet.vParamValue = GetValue(_T("param_value"), L"");
				recSet.vParamType  = GetValue(_T("param_type"), L"");

				m_pRS->MoveNext();
				return true;
			}
			catch (_com_error& err)
			{
				log->LogString(LEVEL_INFO, L"\"get_func_param_by_branch\" recordset failed with error code: 0x%p : %s ", err.Error(), err.Description().GetBSTR());
			}
			catch (...)
			{
				log->LogString(LEVEL_INFO, L"Unknown exception when try to get \"get_func_param_by_branch\" recordset");
			}
		}
	}
	return false;

}


CGetFuncParamDataHelper::FuncParamCollector CGetFuncParamDataHelper::GetFuncParamByBranch(const int& _branch_id)
{
	CGetFuncParamDataHelper::FuncParamCollector out;

	singleton_auto_pointer<CSystemLog> log;
	std::shared_ptr<CGetFuncParamByBranch> pGetProc;
	std::string connectionString;

	SystemConfig settings;
	connectionString = wtos(settings->safe_get_config_value(L"ConnectionString", std::wstring(L"")));

	CSmartADODataBase database(connectionString);

	if (database.get())
	{
		log->LogString(LEVEL_INFO, L"Connecting \"get_func_param_by_branch\" (%i) to database", _branch_id);

		pGetProc.reset(new CGetFuncParamByBranch(connectionString));
		if (!pGetProc->Initialize(L"get_func_param_by_branch", database.get()))
		{
			log->LogString(LEVEL_INFO, L"Cannot Initialize \"get_func_param_by_branch\" proc");
			return out;
		}

		log->LogString(LEVEL_INFO, L"\"get_func_param_by_branch\" Initializing completed");

		if (!pGetProc->AdAllParam())
		{
			log->LogString(LEVEL_INFO, L"Cannot AdAllParam \"get_func_param_by_branch\"");
			return out;
		}

		if (!pGetProc->Connect(database.get()))
		{
			throw CString(L"\"get_func_param_by_branch\" proc: cannot connect to database");
		}

		if (!pGetProc->SetAllParam(_branch_id))
		{
			// cannot set upload_data proc params
			CString Err;
			Err.Format(L"\"get_func_param_by_branch\" proc - error in incoming params");
			throw Err;
		}

		log->LogString(LEVEL_INFO, L"\"get_func_param_by_branch\" call");
		if (pGetProc->Call() == -1)
		{
			throw CString(L"\"get_func_param_by_branch\" proc - execution failed");
		}

		FuncParam_RecordSet record;
		while (pGetProc->GetNextRecordSet(record))
		{
			log->LogString(LEVEL_INFO, L"ParamName = %s, ParamValue = %s, ParamType = %s",
				std::wstring(record.vParamName.bstrVal).c_str(), std::wstring(record.vParamValue.bstrVal).c_str(), std::wstring(record.vParamType.bstrVal).c_str());

			out.emplace_back(record.vParamName.bstrVal, record.vParamValue.bstrVal, record.vParamType.bstrVal);
		}
	}

	return out;
}
/******************************* eof *************************************/