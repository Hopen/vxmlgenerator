/************************************************************************/
/* Name     : CDRLog\Timer.h                                            */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-CT                                                  */
/* Date     : 19 Feb 2015                                               */
/************************************************************************/
#pragma once

#include <boost/thread.hpp>
#include <boost/asio.hpp>

class CTimer
{
public:
	typedef void(*SimpleCallbackFunc)();

	CTimer(std::shared_ptr<boost::asio::io_service>_io/*boost::asio::io_service& _io*/, SimpleCallbackFunc _callback, int _checkTime);
	~CTimer();
	void CancelTimer();
private:
	void Loop();

private:
	boost::asio::strand m_strand;
	SimpleCallbackFunc m_callBackFunc;
	boost::posix_time::minutes m_checkTime;
	boost::asio::deadline_timer m_timer;
};

/******************************* eof *************************************/