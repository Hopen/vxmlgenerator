/************************************************************************/
/* Name     : VXMLCreateSrv\MainCreator.h                               */
/* Author   : Andrey Alekseev                                           */
/* Project  : VXMLCreateSrv                                             */
/* Company  : Forte-CT                                                  */
/* Date     : 04 Aug 2015                                               */
/************************************************************************/
#pragma once

#include "Patterns/singleton.h"
#include "Timer.h"
//#include "EvtModel.h"
#include "Router\router_compatibility.h"

//class CSystemLog;
//class CRouterManager;
////class CLIENT_ADDRESS;
//class CMessage;


class CMainCreator : public singleton < CMainCreator >
{
	friend class singleton < CMainCreator >;
public:
	CMainCreator();
	~CMainCreator();

	long Init(CSystemLog* pLog);

	bool LoadTree(const int& _iTreeId, const std::wstring& finalURI);
	bool LoadTree();

	bool IsDataBaseChange() = delete;

	static bool TestWriteDatabase();
	void InitTimer(/*const unsigned int& msec*/);

private:

	//router api
	void RouterConnectHandler();
	void RouterDeliverErrorHandler(const CMessage& _msg, const CLIENT_ADDRESS &_from, const CLIENT_ADDRESS &_to);
	void AppStatusChange(DWORD dwClient, /*int*/core::ClientStatus	nStatus);
	void SubscribeOnEvents();

protected:
	void IVRD2VGR_MakeTreeEventHandler(CMessage _msg, const CLIENT_ADDRESS& _from, const CLIENT_ADDRESS& _to);

	template<typename Func>
	void subscribe(const std::wstring& _messageName, Func&& func)
	{
		core::SubscribeMessage(m_router, _messageName, this, std::forward<Func>(func));
	}

private:
	CSystemLog * m_log;

	std::shared_ptr<CTimer> m_timer1;
	std::shared_ptr<boost::asio::io_service> m_io;

	boost::mutex m_mutex;
	std::string m_strConnectionString;

	CRouterManager	m_router;

};

/******************************* eof *************************************/