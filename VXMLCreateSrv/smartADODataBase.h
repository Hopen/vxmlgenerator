/************************************************************************/
/* Name     : VXMLCreateSrv\CGetBranchesByTree.h                         */
/* Author   : Andrey Alekseev                                           */
/* Project  : VXMLCreateSrv                                             */
/* Company  : Forte-CT                                                  */
/* Date     : 05 Aug 2015                                               */
/************************************************************************/
#pragma once

#include <boost/shared_ptr.hpp>
#include "..\ado\ADODatabase.h"
//#include "logger.h"
#include "Log\SystemLog.h"
#include "Configuration/SystemConfiguration.h"


class CSmartADODataBase
{
public:
	CSmartADODataBase::CSmartADODataBase(const std::string& _connectionString)
	{
		//CConfigurationSettings settings;
		// Initiate database
		try
		{
			m_pLog->LogString(LEVEL_SEVERE, L"new CADODatabase", L"");
			m_database = std::shared_ptr<CADODatabase>(new CADODatabase());
			m_pLog->LogString(LEVEL_SEVERE, L"m_database->Open", L"");
			m_database->Open(_connectionString.c_str(), "", "", "", adUseClient);
			m_pLog->LogString(LEVEL_SEVERE, L"OK", L"");
		}
		catch (const core::configuration_error& error)
		{
			m_pLog->LogString(LEVEL_SEVERE, L"Connect database string config param reading failed: %s", stow(error.what()));
			throw;
		}
		catch (_com_error& err)
		{
			m_pLog->LogString(LEVEL_SEVERE, L"Connect to database failed with error code: 0x%p : %s ", err.Error(), err.Description().GetBSTR());
			throw;
		}
		catch (...)
		{
			m_pLog->LogString(LEVEL_SEVERE, L"Unknown exception when try to connect to database");
			throw;
		}
	}
	CSmartADODataBase::~CSmartADODataBase()
	{
		m_database->Close();
	}

	CADODatabase* get()
	{
		return m_database.get();
	}
	bool Execute(LPCTSTR lpstrExec)
	{
		return m_database->Execute(lpstrExec);
	}
private:
	std::shared_ptr<CADODatabase>     m_database;
	singleton_auto_pointer<CSystemLog>	m_pLog;
};

/**************************** CSmartADODataBase **************************/