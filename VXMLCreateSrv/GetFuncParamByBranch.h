/************************************************************************/
/* Name     : VXMLCreateSrv\GetFuncParamByBranch.h                      */
/* Author   : Andrey Alekseev                                           */
/* Project  : VXMLCreateSrv                                             */
/* Company  : Expert Solutions                                          */
/* Date     : 23 Dec 2015                                               */
/************************************************************************/
#pragma once

#include <list>
#include "..\ado\ADODatabase.h"
//#include "logger.h"
#include "Configuration\SystemConfiguration.h"

typedef struct FuncParam_RecordSet
{
	_variant_t vParamName;
	_variant_t vParamValue;
	_variant_t vParamType;
} FuncParam_RecordSet_type;

class CGetFuncParamByBranch : public CADOStoredProc
{
public:
	CGetFuncParamByBranch(const std::string& _connectionString);
	virtual ~CGetFuncParamByBranch();

	bool AdAllParam();
	bool SetAllParam(
		const int& _branch_id
		);

	bool Connect(CADODatabase *pDB = NULL);


	int Call();

	bool GetNextRecordSet(FuncParam_RecordSet_type &recSet);

private:
	//CSystemLog * m_pLog;
	std::string m_connectionString;
};


class CGetFuncParamDataHelper
{
public:
	class CFuncParam
	{
	public:
		CFuncParam(
			const std::wstring& param_name, 
			const std::wstring& param_value,
			const std::wstring& param_type
			)
			:
			m_sParamName(param_name),
			m_sParamValue(param_value),
			m_sParamType(param_type)
		{}
		std::wstring GetParamName()const noexcept { return m_sParamName; }
		std::wstring GetParamValue()const noexcept { return m_sParamValue; }
		std::wstring GetParamType()const noexcept { return m_sParamType; }
	private:
		std::wstring m_sParamName;
		std::wstring m_sParamValue;
		std::wstring m_sParamType;
	};

	typedef std::list<CFuncParam> FuncParamCollector;

	FuncParamCollector GetFuncParamByBranch(const int& _branch_id);

	CGetFuncParamDataHelper()
	{}
};
