/************************************************************************/
/* Name     : Utils.cpp                                                 */
/* Author   : Andrey Alekseev                                           */
/* Project  : IS3AutoUpdateProject                                      */
/* Company  : Forte-CT                                                  */
/* Date     : 19 Jul 2011                                               */
/************************************************************************/
#include "stdafx.h"
#include "utils.h"

namespace Utils
{
	int GetReg(const HKEY HKEYROOT, const CString szKeyPath, CString szValueName, CString &szValue)
	{
		CRegKey key;

		//if (key.Create( HKEYROOT, szKeyPath ) != ERROR_SUCCESS)
		//	return 1;
		if (key.Open(HKEYROOT,szKeyPath))
			return 1;

		DWORD dwLen;
		LPTSTR lpStr;
		if(key.QueryStringValue(szValueName, NULL, &dwLen)==ERROR_SUCCESS)
		{
			lpStr=szValue.GetBuffer(dwLen+1);
			key.QueryStringValue(LPCTSTR(szValueName), lpStr, &dwLen);
			szValue.ReleaseBuffer();
		}

		return 0;
	}

	int SetReg(const HKEY HKEYROOT, const CString szKeyPath, CString szValueName, CString szValue)
	{
		CRegKey key;

		if (key.Create( HKEYROOT, szKeyPath ) != ERROR_SUCCESS)
			return 1;

		DWORD dwLen = szValue.GetLength();
		key.SetStringValue(szValueName,szValue);

		return 0;
	}

	int GetReg(const HKEY HKEYROOT, const CString szKeyPath, CString szValueName, DWORD &dValue)
	{
		CRegKey key;

		//if (key.Create( HKEYROOT, szKeyPath) != ERROR_SUCCESS)
		//	return 1;
		if (key.Open(HKEYROOT,szKeyPath))
			return 1;

		key.QueryDWORDValue(LPCTSTR(szValueName),dValue);

		return 0;
	}

	int SetReg(const HKEY HKEYROOT, const CString szKeyPath, CString szValueName, DWORD dValue)
	{
		CRegKey key;

		if (key.Create( HKEYROOT, szKeyPath ) != ERROR_SUCCESS)
			return 1;

		key.SetDWORDValue(szValueName,dValue);

		return 0;
	}


	int DelRegKey(const HKEY HKEYROOT, const CString szKeyPath, const CString szDeleteName)
	{
		CRegKey key;
		if (key.Open(HKEYROOT,szKeyPath))
			return 1;
		key.DeleteSubKey(szDeleteName);
		return 0;
	}

	int GetMultiReg(const HKEY HKEYROOT, const CString szKeyPath, CString szValueName, CString &szValue)
	{
		CRegKey key;

		if (key.Create( HKEYROOT, szKeyPath ) != ERROR_SUCCESS)
			return 1;

		DWORD dwLen;
		LPTSTR lpStr;
		if(key.QueryMultiStringValue(szValueName, NULL, &dwLen)==ERROR_SUCCESS)
		{
			lpStr=szValue.GetBuffer(dwLen+1);
			key.QueryMultiStringValue(szValueName, lpStr, &dwLen);
			szValue.ReleaseBuffer();
		}

		return 0;
	}

	int SetMultiReg(const HKEY HKEYROOT, const CString szKeyPath, CString szValueName, CString szValue)
	{
		CRegKey key;

		if (key.Create( HKEYROOT, szKeyPath ) != ERROR_SUCCESS)
			return 1;
		DWORD dwLen = szValue.GetLength();
		key.SetMultiStringValue(szValueName, szValue);

		return 0;
	}

	//BOOL CreateDirectoryNested(
	//	LPCTSTR lpPathName,
	//	LPSECURITY_ATTRIBUTES lpSecurityAttributes
	//	)
	//{
	//	if (!PathFileExists(lpPathName))
	//	{
	//		ATL::CString strPath = lpPathName;
	//		if (strPath.Right(1) == _T("\\"))
	//			strPath = strPath.Left(strPath.GetLength() - 1);

	//		strPath = strPath.Left(strPath.ReverseFind('\\'));
	//		CreateDirectoryNested(strPath, lpSecurityAttributes);
	//	}
	//	return CreateDirectory(lpPathName, lpSecurityAttributes);
	//}


} // namespace Utils

/******************************* eof *************************************/