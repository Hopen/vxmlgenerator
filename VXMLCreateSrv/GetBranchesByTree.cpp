/************************************************************************/
/* Name     : VXMLCreateSrv\GetBranchesByTree.cpp                       */
/* Author   : Andrey Alekseev                                           */
/* Project  : VXMLCreateSrv                                             */
/* Company  : Forte-CT                                                  */
/* Date     : 05 Aug 2015                                               */
/************************************************************************/

#include "stdafx.h"
#include "GetBranchesByTree.h"


CGetBranchesByTree::CGetBranchesByTree(CSystemLog * pLog, const std::string& _connectionString)
	: m_pLog(pLog), m_connectionString(_connectionString)
{
}


CGetBranchesByTree::~CGetBranchesByTree()
{
}

int CGetBranchesByTree::Call()
{

	if (Execute()) {
		//return GetRcParam();
		return 0;
	}
	return -1;
}

bool CGetBranchesByTree::AdAllParam()
{
	bool bRc = true;
	//bRc &= AdParam( _T("RETURN_VALUE"),    adInteger, adParamReturnValue, 4);

	bRc &= AdParam(_T( "tree_id" ), adInteger, adParamInput, 11);
	return bRc;

}


bool CGetBranchesByTree::SetAllParam(
	const int& _tree_id)
{
	m_pLog->LogString(LEVEL_INFO, L"SetAllParam");

	bool bRc = true;
	bRc &= SetParam(_T( "tree_id" ), _tree_id );

	return bRc;

}


bool CGetBranchesByTree::Connect(CADODatabase *pDB /* = NULL */)
{
	try
	{
		return __super::Connect(L"get_branches_by_tree", m_connectionString.c_str(), pDB);
	}
	catch (_com_error& err)
	{
		m_pLog->LogString(LEVEL_INFO, L"Connect to database failed with error code: 0x%p : %s ", err.Error(), err.Description().GetBSTR());
	}
	catch (...)
	{
		m_pLog->LogString(LEVEL_INFO, L"Unknown exception when try to connect to database");
	}
	return false;
}

bool CGetBranchesByTree::GetNextRecordSet(Content_RecordSet_type &recSet)
{
	if (m_pRS) {

		//m_pLog->LogString(LEVEL_INFO, L"record set count = %i", m_pRS->Fields->GetCount());

		if (m_pRS->EndOfFile != VARIANT_TRUE) {
			try {
				//recSet.vId		    = m_pRS->Fields->GetItem(_T("id"))->Value;
				//recSet.vTreeId      = m_pRS->Fields->GetItem(_T("tree_id"))->Value;
				//recSet.vParentId    = m_pRS->Fields->GetItem(_T("parent_id"))->Value;
				//recSet.vMenuName    = m_pRS->Fields->GetItem(_T("menu_name"))->Value;
				//recSet.vMenuType    = m_pRS->Fields->GetItem(_T("menu_type"))->Value;
				//recSet.vMenuText    = m_pRS->Fields->GetItem(_T("menu_text"))->Value;
				//recSet.vDtmf        = m_pRS->Fields->GetItem(_T("dtmf"))->Value;
				//recSet.vCodeName    = m_pRS->Fields->GetItem(_T("codeName"))->Value;
				//recSet.vCode        = m_pRS->Fields->GetItem(_T("code"))->Value;
				//recSet.vComment     = m_pRS->Fields->GetItem(_T("comment"))->Value;
				//recSet.vWavName     = m_pRS->Fields->GetItem(_T("wav_name"))->Value;
				//recSet.vSubDialogId = m_pRS->Fields->GetItem(_T("sub_dialog_id"))->Value;
				//recSet.vIsVisible   = m_pRS->Fields->GetItem(_T("is_visible"))->Value;
				//recSet.vRepeatCnt   = m_pRS->Fields->GetItem(_T("repeat_cnt"))->Value;
				//recSet.vGotoRepeat  = m_pRS->Fields->GetItem(_T("goto_repeat"))->Value;
				//recSet.vGotoBack    = m_pRS->Fields->GetItem(_T("goto_back"))->Value;
				//recSet.vGoto        = m_pRS->Fields->GetItem(_T("goto"))->Value;
				//recSet.vCityId      = m_pRS->Fields->GetItem(_T("city_id"))->Value;
				//recSet.vWavBreak    = m_pRS->Fields->GetItem(_T("wav_break"))->Value;
				//recSet.vDtmfWait    = m_pRS->Fields->GetItem(_T("dtmf_wait"))->Value;
				//recSet.vGotoRoot    = m_pRS->Fields->GetItem(_T("goto_root"))->Value;
				//recSet.vTermChar    = m_pRS->Fields->GetItem(_T("term_char"))->Value;
				//recSet.vTimeOut     = m_pRS->Fields->GetItem(_T("time_out"))->Value;
				//recSet.vRoot        = m_pRS->Fields->GetItem(_T("root"))->Value;
				//recSet.vCondition   = m_pRS->Fields->GetItem(_T("condition"))->Value;
				//recSet.vFile1		= GetValue(_T("file1"), L"");
				////recSet.vFile1		= m_pRS->Fields->GetItem(_T("file1"))->Value;
				//recSet.vFile2       = m_pRS->Fields->GetItem(_T("file2"))->Value;
				//recSet.vFile3       = m_pRS->Fields->GetItem(_T("file3"))->Value;
				//recSet.vFile4       = m_pRS->Fields->GetItem(_T("file4"))->Value;
				//recSet.vFileText1   = m_pRS->Fields->GetItem(_T("filetext1"))->Value;
				//recSet.vFileText2   = m_pRS->Fields->GetItem(_T("filetext2"))->Value;
				//recSet.vFileText3   = m_pRS->Fields->GetItem(_T("filetext3"))->Value;
				//recSet.vFileText4   = m_pRS->Fields->GetItem(_T("filetext4"))->Value;

				recSet.vId		    = GetValue(_T("id"), 0);
				recSet.vTreeId      = GetValue(_T("tree_id"), 0);
				recSet.vParentId    = GetValue(_T("parent_id"), 0); 
				recSet.vMenuName    = GetValue(_T("menu_name"), L"");
				recSet.vMenuType    = GetValue(_T("menu_type"), 0);
				recSet.vMenuText    = GetValue(_T("menu_text"), L"");
				recSet.vDtmf        = GetValue(_T("dtmf"), L"");
				recSet.vCodeName    = GetValue(_T("codeName"), L"");
				recSet.vCode        = GetValue(_T("code"), L"");
				recSet.vComment     = GetValue(_T("comment"), L"");
				recSet.vWavName     = GetValue(_T("wav_name"), L"");
				recSet.vSubDialogId = GetValue(_T("sub_dialog_id"), 0);
				recSet.vIsVisible   = GetValue(_T("is_visible"), 0);
				recSet.vRepeatCnt   = GetValue(_T("repeat_cnt"), 0);
				recSet.vGotoRepeat  = GetValue(_T("goto_repeat"), L"");
				recSet.vGotoBack    = GetValue(_T("goto_back"), L"");
				recSet.vGoto        = GetValue(_T("goto"), L"");
				recSet.vCityId      = GetValue(_T("city_id"), 0);
				recSet.vWavBreak    = GetValue(_T("wav_break"), L"");
				recSet.vDtmfWait    = GetValue(_T("dtmf_wait"), L"");
				recSet.vGotoRoot    = GetValue(_T("goto_root"), L"");
				recSet.vTermChar    = GetValue(_T("term_char"), L"");
				recSet.vTimeOut     = GetValue(_T("time_out"), 0);
				recSet.vRoot        = GetValue(_T("root"), 0);
				recSet.vCondition   = GetValue(_T("condition"), 0);
				recSet.vAddToHistory= GetValue(_T("add_to_history"), 0);
				recSet.vFile1		= GetValue(_T("file1"), L"");
				recSet.vFile2       = GetValue(_T("file2"), L"");
				recSet.vFile3       = GetValue(_T("file3"), L"");
				recSet.vFile4       = GetValue(_T("file4"), L"");
				recSet.vFileText1   = GetValue(_T("filetext1"), L"");
				recSet.vFileText2   = GetValue(_T("filetext2"), L"");
				recSet.vFileText3   = GetValue(_T("filetext3"), L"");
				recSet.vFileText4   = GetValue(_T("filetext4"), L"");
				recSet.vFuncName    = GetValue(_T("func_name"), L"");
				recSet.vFuncType    = GetValue(_T("func_type"), L"");
				m_pRS->MoveNext();
				return true;
			}
			catch (_com_error& err)
			{
				m_pLog->LogString(LEVEL_INFO, L"\"get_branches_by_tree\" recordset failed with error code: 0x%p : %s ", err.Error(), err.Description().GetBSTR());
			}
			catch (...)
			{
				m_pLog->LogString(LEVEL_INFO, L"Unknown exception when try to get \"get_branches_by_tree\" recordset");
			}
		}
	}
	return false;

}

/******************************* eof *************************************/