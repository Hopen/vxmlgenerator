/************************************************************************/
/* Name     : VXMLCreateSrv\branches.h                                  */
/* Author   : Andrey Alekseev                                           */
/* Project  : VXMLCreateSrv                                             */
/* Company  : Forte-CT                                                  */
/* Date     : 07 Aug 2015                                               */
/************************************************************************/
#pragma once
#include "ChoiceMenu.h"
#include "GotoMenu.h"
#include "FormSubdialog.h"
#include "Noinput.h"
#include "Nomatch.h"
#include "SmartMenu.h"

/******************************* eof *************************************/