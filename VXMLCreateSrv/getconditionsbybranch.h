/************************************************************************/
/* Name     : VXMLCreateSrv\getconditionsbybranch.h                     */
/* Author   : Andrey Alekseev                                           */
/* Project  : VXMLCreateSrv                                             */
/* Company  : Forte-CT                                                  */
/* Date     : 17 Set 2015                                               */
/************************************************************************/
#pragma once

#include <list>
#include "..\ado\ADODatabase.h"
//#include "logger.h"
#include "Configuration\SystemConfiguration.h"


typedef struct Condition_RecordSet
{
	_variant_t vId;
	_variant_t vBranchId;
	_variant_t vCondition;
	_variant_t vExp;
} Condition_RecordSet_type;

class CGetConditionsByBranch : public CADOStoredProc
{
public:
	CGetConditionsByBranch(/*CSystemLog * pLog, */const std::string& _connectionString);
	virtual ~CGetConditionsByBranch();

	bool AdAllParam();
	bool SetAllParam(
		const int& _branch_id
		);

	bool Connect(CADODatabase *pDB = NULL);


	int Call();

	bool GetNextRecordSet(Condition_RecordSet_type &recSet);

private:
	//CSystemLog * m_pLog;
	std::string m_connectionString;
};


class CGetDataHelper
{
public:
	class CGotoCondition
	{
	public:
		CGotoCondition(const std::wstring& _cond, const std::wstring& _expr)
			:m_sCondition(_cond), m_sExpression(_expr)
		{}
		std::wstring GetCondition ()const { return m_sCondition;  }
		std::wstring GetExpression()const { return m_sExpression; }
	private:
		std::wstring m_sCondition;
		std::wstring m_sExpression;
	};

	typedef std::list<CGotoCondition> ConditionCollector;

	ConditionCollector GetConditionByBranch(const int& _branch_id);

	CGetDataHelper(/*CSystemLog * pLog, const std::string& _connectionString*/)
		//: m_pLog(pLog), m_connectionString(_connectionString)
	{}

//private:
//	CSystemLog * m_pLog;
//	std::string m_connectionString;
};



/******************************* eof *************************************/