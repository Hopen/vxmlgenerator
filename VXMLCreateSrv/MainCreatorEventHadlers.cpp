/************************************************************************/
/* Name     : VXMLCreateSrv\MainCreatorEventHandlers.cpp                */
/* Author   : Andrey Alekseev                                           */
/* Project  : VXMLCreateSrv                                             */
/* Company  : Expert Solutions                                          */
/* Date     : 22 Jan 2016                                               */
/************************************************************************/
#include "stdafx.h"
#include "MainCreator.h"
#include "Log\SystemLog.h"
#include "Router\router_compatibility.h"
//#include "protocol.h"
#include "utils.h"
//#include "sv_strutils.h"

void CMainCreator::SubscribeOnEvents()
{
	//SUBSCRIBE_MESSAGE(m_router, L"IVRD2VGR_MAKE_TREE", CMainCreator::IVRD2VGR_MakeTreeEventHandler, this);
	subscribe(L"IVRD2VGR_MAKE_TREE", &CMainCreator::IVRD2VGR_MakeTreeEventHandler);
}


void CMainCreator::IVRD2VGR_MakeTreeEventHandler(CMessage _msg, const CLIENT_ADDRESS& _from, const CLIENT_ADDRESS& _to)
{
	/************************************************************************/
	/*
	Name: IVRD2VCR_MAKE_TREE; tree_id = "83"; final_uri="c://test1.vxml"
	*/
	/************************************************************************/

	//::CoInitializeEx(NULL, COINIT_MULTITHREADED);

	int tmp_tree_id = Utils::toNum<int>(_msg.SafeReadParam(L"tree_id", core::checked_type::String, L"1").AsWideStr());
	std::wstring sFinalUri = _msg.SafeReadParam(L"final_uri", core::checked_type::String, L"").AsWideStr();


	//if (const CParam* pTid = _message.ParamByName(L"tree_id"))
	//	tmp_tree_id = Utils::toNum<int>(pTid->AsWideStr());

	//if (const CParam* pUri = _message.ParamByName(L"final_uri"))
	//	sFinalUri = pUri->AsWideStr();

	m_log->LogStringModule(LEVEL_INFO, L"Incoming message", L"IVRD2VGR_MAKE_TREE, tree_id: %i, final_uri: %s", tmp_tree_id, sFinalUri.c_str());


	this->LoadTree(tmp_tree_id, sFinalUri);

	//CoUninitialize();

}


void CMainCreator::RouterConnectHandler()
{
	m_log->LogStringModule(LEVEL_INFO, L"Creator Core", L"Router has connected");
}

void CMainCreator::RouterDeliverErrorHandler(const CMessage& _msg, const CLIENT_ADDRESS &_from, const CLIENT_ADDRESS &_to)
{
	m_log->LogStringModule(LEVEL_FINE, L"Router client", _msg.Dump().c_str());
}

void CMainCreator::AppStatusChange(DWORD dwClient, /*int*/core::ClientStatus	nStatus)
{
	//if (nStatus == CS_DISCONNECTED)
	//{
	//	try
	//	{

	//	}
	//	catch (...)
	//	{

	//	}
	//}

}



/************************************************************************/