/************************************************************************/
/* Name     : VXMLCreateSrv\getconditionsbybranch.cpp                   */
/* Author   : Andrey Alekseev                                           */
/* Project  : VXMLCreateSrv                                             */
/* Company  : Forte-CT                                                  */
/* Date     : 17 Set 2015                                               */
/************************************************************************/

#include "stdafx.h"
#include "getconditionsbybranch.h"
#include "smartADODataBase.h"


CGetConditionsByBranch::CGetConditionsByBranch(/*CSystemLog * pLog, */const std::string& _connectionString)
	: /*m_pLog(pLog),*/ m_connectionString(_connectionString)
{
}


CGetConditionsByBranch::~CGetConditionsByBranch()
{
}

int CGetConditionsByBranch::Call()
{

	if (Execute()) {
		//return GetRcParam();
		return 0;
	}
	return -1;
}

bool CGetConditionsByBranch::AdAllParam()
{
	bool bRc = true;
	//bRc &= AdParam( _T("RETURN_VALUE"),    adInteger, adParamReturnValue, 4);

	bRc &= AdParam(_T("branch_id"), adInteger, adParamInput, 11);
	return bRc;

}


bool CGetConditionsByBranch::SetAllParam(
	const int& _branch_id)
{
	singleton_auto_pointer<CSystemLog> log;

	log->LogString(LEVEL_INFO, L"SetAllParam");

	bool bRc = true;
	bRc &= SetParam(_T("branch_id"), _branch_id);

	return bRc;

}


bool CGetConditionsByBranch::Connect(CADODatabase *pDB /* = NULL */)
{
	singleton_auto_pointer<CSystemLog> log;
	try
	{
		return __super::Connect(L"get_conditions_by_branch", m_connectionString.c_str(), pDB);
	}
	catch (_com_error& err)
	{
		log->LogString(LEVEL_INFO, L"Connect to database failed with error code: 0x%p : %s ", err.Error(), err.Description().GetBSTR());
	}
	catch (...)
	{
		log->LogString(LEVEL_INFO, L"Unknown exception when try to connect to database");
	}
	return false;
}

bool CGetConditionsByBranch::GetNextRecordSet(Condition_RecordSet_type &recSet)
{
	if (m_pRS) {
		singleton_auto_pointer<CSystemLog> log;

		//m_pLog->LogString(LEVEL_INFO, L"record set count = %i", m_pRS->Fields->GetCount());

		if (m_pRS->EndOfFile != VARIANT_TRUE) {
			try {
				recSet.vId        = m_pRS->Fields->GetItem(_T("id"))->Value;
				recSet.vBranchId  = m_pRS->Fields->GetItem(_T("branch_id"))->Value;
				recSet.vCondition = m_pRS->Fields->GetItem(_T("condition"))->Value;
				recSet.vExp       = m_pRS->Fields->GetItem(_T("exp"))->Value;

				m_pRS->MoveNext();
				return true;
			}
			catch (_com_error& err)
			{
				log->LogString(LEVEL_INFO, L"\"get_conditions_by_branch\" recordset failed with error code: 0x%p : %s ", err.Error(), err.Description().GetBSTR());
			}
			catch (...)
			{
				log->LogString(LEVEL_INFO, L"Unknown exception when try to get \"get_conditions_by_branch\" recordset");
			}
		}
	}
	return false;

}


CGetDataHelper::ConditionCollector CGetDataHelper::GetConditionByBranch(const int& _branch_id)
{
	CGetDataHelper::ConditionCollector out;

	singleton_auto_pointer<CSystemLog> log;
	std::shared_ptr<CGetConditionsByBranch> pGetProc;
	std::string connectionString;

	SystemConfig settings;
	connectionString = wtos(settings->safe_get_config_value(L"ConnectionString", std::wstring(L"")));

	CSmartADODataBase database(connectionString);

	if (database.get())
	{
		log->LogString(LEVEL_INFO, L"Connecting \"get_conditions_by_branch\" to database");

		pGetProc.reset(new CGetConditionsByBranch(connectionString));
		if (!pGetProc->Initialize(L"get_conditions_by_branch", database.get()))
		{
			log->LogString(LEVEL_INFO, L"Cannot Initialize \"get_conditions_by_branch\" proc");
			return out;
		}

		log->LogString(LEVEL_INFO, L"\"get_conditions_by_branch\" Initializing completed");

		if (!pGetProc->AdAllParam())
		{
			log->LogString(LEVEL_INFO, L"Cannot AdAllParam \"get_conditions_by_branch\"");
			return out;
		}

		if (!pGetProc->Connect(database.get()))
		{
			throw CString(L"\"get_conditions_by_branch\" proc: cannot connect to database");
		}

		if (!pGetProc->SetAllParam(_branch_id))
		{
			// cannot set upload_data proc params
			CString Err;
			Err.Format(L"\"get_conditions_by_branch\" proc - error in incoming params");
			throw Err;
		}

		log->LogString(LEVEL_INFO, L"\"get_conditions_by_branch\" call");
		if (pGetProc->Call() == -1)
		{
			throw CString(L"\"get_conditions_by_branch\" proc - execution failed");
		}

		Condition_RecordSet record;
		while (pGetProc->GetNextRecordSet(record))
		{
			log->LogString(LEVEL_INFO, L"Branch_id = %i, ID = %i, Cond: %s, Expr: %s",
				record.vBranchId.intVal, record.vId.intVal,
				std::wstring(record.vCondition.bstrVal).c_str(), std::wstring(record.vExp.bstrVal).c_str());

			out.push_back(CGetDataHelper::CGotoCondition(record.vCondition.bstrVal, record.vExp.bstrVal));
		}
	}

	return out;
}
/******************************* eof *************************************/