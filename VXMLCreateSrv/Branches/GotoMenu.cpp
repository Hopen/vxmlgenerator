/************************************************************************/
/* Name     : VXMLCreateSrv\Branches\GotoMenu.cpp                       */
/* Author   : Andrey Alekseev                                           */
/* Project  : VXMLCreateSrv                                             */
/* Company  : Forte-CT                                                  */
/* Date     : 07 Aug 2015                                               */
/************************************************************************/
#include "stdafx.h"
#include "GotoMenu.h"
//#include "../getconditionsbybranch.h"

CGotoMenu::CGotoMenu(const int& _iTreeId,
	const int& _iParentId,
	const std::wstring& _sMenuName,
	//const std::wstring& _sMenuText,
	const std::wstring& _sDtmf,
	const std::wstring& _sComment,
	//const std::wstring& _sWavName,
	const int& _iSubDialogId,
	//const bool& _bIsVisible,
	const std::wstring& _sGotoRepeat,
	const std::wstring& _sGotoBack,
	const std::wstring& _sGoto,
	const int& _bIsCondition,
	const int& _bAddToHistory,
	const std::wstring & _sFile1)
	: CBaseBranch(_iTreeId, _iParentId, _sMenuName, //_sMenuText,
	_sDtmf, _sComment, /*_sWavName,*/ _iSubDialogId,
	/*_bIsVisible,*/0, _sGotoRepeat, _sGotoBack, _sGoto, L"", L"empty", 0, false, _bIsCondition, _bAddToHistory, _sFile1)
{
}

bool CGotoMenu::IsValid()
{
	// the menu of this type mast have: 
	//   1. id
	//   2. wav
	//   3. goto

	if (!GetMenuName().length())
		return false;
	if (!GetFile1().length())
		return false;
	if (!GetGoto().length())
		return false;

	return true;
}

//void CGotoMenu::AddGoto(CComPtr < IXMLDOMDocument > pDoc, CComPtr <IXMLDOMElement> elemMenu, bool expression)
//{
//	// add goto
//	CComPtr <IXMLDOMElement> elemGoto = NULL;
//	HRESULT hr = pDoc->createElement(_T("goto"), &elemGoto);
//	if (SUCCEEDED(hr) && elemGoto)
//	{
//		if (expression)
//		{
//			elemGoto->setAttribute(L"expr", _variant_t(L"next"));
//		}
//		else
//		{
//			elemGoto->setAttribute(L"next", _variant_t(std::wstring(L"#" + GetGoto()).c_str()));
//		}
//
//		elemMenu->appendChild(elemGoto, NULL);
//	}
//
//
//}

CComPtr <IXMLDOMElement> CGotoMenu::CreateXMLItem(CComPtr < IXMLDOMDocument > pDoc)
{
	if (!pDoc)
		NULL;

	CComPtr <IXMLDOMElement> elemMenu = NULL;
	HRESULT hr = pDoc->createElement(_T("menu"), &elemMenu);

	if (SUCCEEDED(hr) && elemMenu)
	{
		//set attributs
		//if (GetComment().length())
		//	elemMenu->setAttribute(L"comment", _variant_t(GetComment().c_str()));
		//elemMenu->setAttribute(L"id", _variant_t(GetMenuName().c_str()));
		SetMenuAttributes(GetMenuName(), elemMenu);

		////add prompt+audio
		//CComPtr <IXMLDOMElement> elemPrompt = NULL;
		//hr = pDoc->createElement(_T("prompt"), &elemPrompt);
		//if (SUCCEEDED(hr) && elemPrompt)
		//{
		//	CComPtr <IXMLDOMElement> elemAudio = NULL;
		//	hr = pDoc->createElement(_T("audio"), &elemAudio);
		//	if (SUCCEEDED(hr) && elemAudio)
		//	{
		//		elemAudio->setAttribute(L"src", _variant_t(GetWavName().c_str()));
		//		elemPrompt->appendChild(elemAudio, NULL);
		//	}

		//	elemMenu->appendChild(elemPrompt, NULL);
		//}
		//AddPromptAudio(GetWavName(), pDoc, elemMenu);

		//CGetDataHelper::ConditionCollector conditions;
		//// if condition
		//if (0)
		//{
		//	if (this->GetIsCondition())
		//	{
		//		CGetDataHelper collector;
		//		conditions = collector.GetConditionByBranch(this->GetId());
		//	}
		//}

		//if (1)
		//{
		//	conditions.push_back(CGetDataHelper::CGotoCondition(L"menu_1.utterance = 1", L"menu_11"));
		//	conditions.push_back(CGetDataHelper::CGotoCondition(L"menu_1.utterance = 2", L"menu_12"));
		//}

		//if (!conditions.empty())
		//{
		//	CComPtr <IXMLDOMElement> elemVar = NULL;
		//	hr = pDoc->createElement(_T("var"), &elemVar);
		//	if (SUCCEEDED(hr) && elemVar)
		//	{
		//		std::wstring expr(L"'#" + GetGoto() + L"'");
		//		elemVar->setAttribute(L"expr", _variant_t(expr.c_str()));
		//		elemVar->setAttribute(L"name", _variant_t(L"next"));
		//		elemMenu->appendChild(elemVar, NULL);
		//	}

		//	bool bFirst = true;;
		//	CComPtr <IXMLDOMElement> elemIf = NULL;
		//	hr = pDoc->createElement(_T("if"), &elemIf);
		//	if (SUCCEEDED(hr) && elemIf)
		//	{
		//		for each (CGetDataHelper::CGotoCondition cond in conditions)
		//		{
		//			CComPtr <IXMLDOMElement> elemElseif = NULL;
		//			if (bFirst)
		//			{
		//				bFirst = false;
		//				elemIf->setAttribute(L"cond", _variant_t(cond.GetCondition().c_str()));
		//			}
		//			else
		//			{
		//				hr = pDoc->createElement(_T("elseif"), &elemElseif);
		//				if (SUCCEEDED(hr) && elemIf)
		//				{
		//					elemElseif->setAttribute(L"cond", _variant_t(cond.GetCondition().c_str()));
		//					elemIf->appendChild(elemElseif, NULL);
		//				}
		//			}

		//			
		//			//expression
		//			CComPtr <IXMLDOMElement> elemAssign = NULL;
		//			hr = pDoc->createElement(_T("assign"), &elemAssign);
		//			if (SUCCEEDED(hr) && elemAssign)
		//			{
		//				elemAssign->setAttribute(L"expr", _variant_t(cond.GetExpression().c_str()));
		//				elemAssign->setAttribute(L"name", _variant_t(L"next"));
		//				elemIf->appendChild(elemAssign, NULL);
		//			}
		//		}
		//		elemMenu->appendChild(elemIf, NULL);
		//	}

		//}
		bool bConditionExist = AddConditions(pDoc, elemMenu);

		//// add goto
		//CComPtr <IXMLDOMElement> elemGoto = NULL;
		//hr = pDoc->createElement(_T("goto"), &elemGoto);
		//if (SUCCEEDED(hr) && elemGoto)
		//{
		//	if (!conditions.empty())
		//	{
		//		elemGoto->setAttribute(L"expr", _variant_t(L"next"));
		//	}
		//	else
		//	{
		//		elemGoto->setAttribute(L"next", _variant_t(std::wstring(L"#" + GetGoto()).c_str()));
		//	}

		//	elemMenu->appendChild(elemGoto, NULL);
		//}
		if (GetAddToHistory())
		{
			AddAddToHistory(pDoc, elemMenu);
		}


		AddGoto(pDoc, elemMenu, bConditionExist);


	}
	return elemMenu;
}

//void CGotoMenu::PushVXMLDOMItems(CComPtr < IXMLDOMDocument > pDoc, CComPtr <IXMLDOMElement> elemVXML)
//{
//	CComPtr <IXMLDOMElement> itemMenu = CreateXMLItem(pDoc);
//	if (itemMenu)
//	{
//		elemVXML->appendChild(itemMenu, NULL);
//	}
//}
//
//std::wstring CGotoMenu::GetLocalRoot()
//{
//	if (GetIsRoot())
//		return GetMenuName();
//
//	if (GetParentPtr())
//		return GetParentPtr()->GetLocalRoot();
//
//	return GetMenuName();
//}


/******************************* eof *************************************/