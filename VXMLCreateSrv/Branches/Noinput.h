/************************************************************************/
/* Name     : VXMLCreateSrv\Branches\Noinput.h                          */
/* Author   : Andrey Alekseev                                           */
/* Project  : VXMLCreateSrv                                             */
/* Company  : Forte-CT                                                  */
/* Date     : 15 Set 2015                                               */
/************************************************************************/
#pragma once
#include "BaseBranch.h"

class CNoinputMenu : public CBaseBranch //<CNoinputMenu>
{
	//friend class CBaseBranch < CNoinputMenu > ;
public:
	CNoinputMenu(const int& _iTreeId,
		const int& _iParentId,
		const std::wstring& _sMenuName,
		const std::wstring& _sDtmf,
		const std::wstring& _sComment,
		//const std::wstring& _sWavName,
		const int& _iSubDialogId,
		const int& _iRepeatCnt,
		const std::wstring& _sGoto,
		const int& _bIsCondition,
		const std::wstring & _sFile1);

public:
	virtual int GetTypeMenu() { return MT_NOINPUT; }

	//void AddAudio(CComPtr < IXMLDOMDocument > pDoc, CComPtr <IXMLDOMElement> elemMenu);

	//void AddChild(BranchPtr child){  }
	CComPtr <IXMLDOMElement> CreateXMLItem(CComPtr < IXMLDOMDocument > pDoc);
	bool IsValid();

	//void PushVXMLDOMItems(CComPtr < IXMLDOMDocument > pDoc, CComPtr <IXMLDOMElement> elemVXML);
	//std::wstring GetLocalRoot();
};


/******************************* eof *************************************/