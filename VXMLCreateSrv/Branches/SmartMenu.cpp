/************************************************************************/
/* Name     : VXMLCreateSrv\Branches\SmartMenu.cpp                      */
/* Author   : Andrey Alekseev                                           */
/* Project  : VXMLCreateSrv                                             */
/* Company  : Forte-CT                                                  */
/* Date     : 29 Set 2015                                               */
/************************************************************************/
#include "stdafx.h"
#include "SmartMenu.h"


CSmartMenu::CSmartMenu(const int& _iTreeId,
	const int& _iParentId,
	const std::wstring& _sMenuName,
	const std::wstring& _sDtmf,
	const std::wstring& _sComment,
	const std::wstring& _sGotoRepeat,
	const std::wstring& _sGotoBack,
	const std::wstring& _sGoto,
	const std::wstring& _sGotoRoot,
	const int& _bRoot,
	const int& _bAddoHistory,
	const std::wstring & _sFile1,
	const std::wstring & _sFile2,
	const std::wstring & _sFile3,
	const std::wstring & _sFile4)
	: CBaseBranch(_iTreeId, _iParentId, _sMenuName, 
	_sDtmf, _sComment, 0,
	0, _sGotoRepeat, _sGotoBack, _sGoto, _sGotoRoot,
	L"", 0, _bRoot, false, _bAddoHistory, _sFile1),
	/*m_file1(_sFile1),*/ m_file2(_sFile2), m_file3(_sFile3), m_file4(_sFile4)
{
}

bool CSmartMenu::IsValid()
{
	// the menu of this type mast have: 
	//   1. id
	//   2. file1
	//   3. goto

	if (!GetMenuName().length())
		return false;
	if (!GetFile1().length())
		return false;
	//if (!GetGoto().length())
	//	return false;

	return true;
}


std::wstring CSmartMenu::GetLocalRoot()const
{
	if (GetIsRoot())
		return std::wstring(GetMenuName() + L"_short");

	if (GetParentPtr().lock())
		return GetParentPtr().lock()->GetLocalRoot();

	return GetMenuName();

}

CComPtr <IXMLDOMElement> CSmartMenu::CreateXMLItem(CComPtr < IXMLDOMDocument > pDoc)
{
	return NULL;
}

CComPtr <IXMLDOMElement> CSmartMenu::CreateCG(CComPtr < IXMLDOMDocument > pDoc)
{
	if (!pDoc)
		NULL;

	CComPtr <IXMLDOMElement> elemMenu = NULL;
	HRESULT hr = pDoc->createElement(_T("menu"), &elemMenu);

	if (SUCCEEDED(hr) && elemMenu)
	{
		//set attributs
		//if (GetComment().length())
		//	elemMenu->setAttribute(L"comment", _variant_t(GetComment().c_str()));
		//elemMenu->setAttribute(L"id", _variant_t(GetMenuName().c_str()));
		SetMenuAttributes(GetMenuName(), elemMenu);

		////add prompt+audio
		//CComPtr <IXMLDOMElement> elemPrompt = NULL;
		//hr = pDoc->createElement(_T("prompt"), &elemPrompt);
		//if (SUCCEEDED(hr) && elemPrompt)
		//{
		//	CComPtr <IXMLDOMElement> elemAudio = NULL;
		//	hr = pDoc->createElement(_T("audio"), &elemAudio);
		//	if (SUCCEEDED(hr) && elemAudio)
		//	{
		//		elemAudio->setAttribute(L"src", _variant_t(GetFile1().c_str()));
		//		elemPrompt->appendChild(elemAudio, NULL);
		//	}

		//	elemMenu->appendChild(elemPrompt, NULL);
		//}
		if (GetAddToHistory())
		{
			AddAddToHistory(pDoc, elemMenu);
		}


		AddPromptAudio(GetFile1(), pDoc, elemMenu);

		////add choices
		//for each (BranchPtr branch in m_children)
		//{
		//	switch (branch->GetTypeMenu())
		//	{
		//	case MT_NOINPUT:
		//	case MT_NOMATCH:
		//	{
		//		break; //skip
		//	}
		//	default:
		//	{
		//		if (branch->GetDtmf().length())
		//		{
		//			CComPtr <IXMLDOMElement> elemChoice = NULL;
		//			hr = pDoc->createElement(_T("choice"), &elemChoice);
		//			if (SUCCEEDED(hr) && elemChoice)
		//			{
		//				elemChoice->setAttribute(L"next", _variant_t(std::wstring(L"#" + branch->GetMenuName()).c_str()));
		//				elemChoice->setAttribute(L"dtmf", _variant_t(branch->GetDtmf().c_str()));
		//				elemMenu->appendChild(elemChoice, NULL);
		//			}
		//		}
		//		break;
		//	}
		//	}

		//}

		AddChoices(pDoc, elemMenu);
		////add repeat 
		//if (GetGotoRepeat().length())
		//{
		//	CComPtr <IXMLDOMElement> elemChoice = NULL;
		//	hr = pDoc->createElement(_T("choice"), &elemChoice);
		//	if (SUCCEEDED(hr) && elemChoice)
		//	{
		//		elemChoice->setAttribute(L"next", _variant_t(std::wstring(L"#" + GetMenuName()).c_str()));
		//		elemChoice->setAttribute(L"dtmf", _variant_t(GetGotoRepeat().c_str()));
		//		elemMenu->appendChild(elemChoice, NULL);
		//	}
		//}
		AddRepeat(GetMenuName(), pDoc, elemMenu);
		//add root
		//if (GetGotoRoot().length())
		//{
		//	CComPtr <IXMLDOMElement> elemChoice = NULL;
		//	hr = pDoc->createElement(_T("choice"), &elemChoice);
		//	if (SUCCEEDED(hr) && elemChoice)
		//	{
				//std::wstring rootMenuName = GetMenuName();
				//if (GetParentPtr().lock())
				//{
//					rootMenuName = GetParentPtr().lock()->GetLocalRoot();
	//			}
		//		elemChoice->setAttribute(L"next", _variant_t(std::wstring(L"#" + rootMenuName).c_str()));
		//		elemChoice->setAttribute(L"dtmf", _variant_t(GetGotoRoot().c_str()));

		//		elemMenu->appendChild(elemChoice, NULL);
		//	}
		//}

		AddRoot(/*GetLocalRoot(),*/ pDoc, elemMenu);

		////add back
		//if (GetGotoBack().length())
		//{
		//	CComPtr <IXMLDOMElement> elemChoice = NULL;
		//	hr = pDoc->createElement(_T("choice"), &elemChoice);
		//	if (SUCCEEDED(hr) && elemChoice && GetParentPtr().lock())
		//	{
		//		std::wstring backMenuName = GetParentPtr().lock()->GetMenuName();

		//		elemChoice->setAttribute(L"next", _variant_t(std::wstring(L"#" + backMenuName).c_str()));
		//		elemChoice->setAttribute(L"dtmf", _variant_t(GetGotoBack().c_str()));

		//		elemMenu->appendChild(elemChoice, NULL);
		//	}
		//}
		AddBack(pDoc, elemMenu);

		//add noinput
		if (!GetFile2().empty())
		{
			AddNoinput(1, std::wstring(L"#" + GetMenuName() + L"_noinput_1"), pDoc, elemMenu);
			//CComPtr <IXMLDOMElement> elemNoinput = NULL;
			//hr = pDoc->createElement(_T("noinput"), &elemNoinput);
			//if (SUCCEEDED(hr) && elemNoinput)
			//{
			//	elemNoinput->setAttribute(L"count", _variant_t(1));

			//	CComPtr <IXMLDOMElement> elemGoto = NULL;
			//	hr = pDoc->createElement(_T("goto"), &elemGoto);
			//	if (SUCCEEDED(hr) && elemGoto)
			//	{
			//		elemGoto->setAttribute(L"next", _variant_t(std::wstring(L"#" + GetMenuName() + L"_noinput_1").c_str()));
			//		elemNoinput->appendChild(elemGoto, NULL);
			//	}

			//	elemMenu->appendChild(elemNoinput, NULL);
			//}
		}

	}
	return elemMenu;
}

CComPtr <IXMLDOMElement> CSmartMenu::CreateCG1(CComPtr < IXMLDOMDocument > pDoc)
{
	if (!pDoc)
		NULL;

	if (GetFile2().empty())
		return NULL;


	CComPtr <IXMLDOMElement> elemMenu = NULL;
	HRESULT hr = pDoc->createElement(_T("menu"), &elemMenu);

	if (SUCCEEDED(hr) && elemMenu)
	{
		//set attributs
		//if (GetComment().length())
		//	elemMenu->setAttribute(L"comment", _variant_t(GetComment().c_str()));
		//elemMenu->setAttribute(L"id", _variant_t(std::wstring(GetMenuName() + L"_noinput_1").c_str()));
		SetMenuAttributes(std::wstring(GetMenuName() + L"_noinput_1"), elemMenu);

		////add prompt+audio
		//CComPtr <IXMLDOMElement> elemPrompt = NULL;
		//hr = pDoc->createElement(_T("prompt"), &elemPrompt);
		//if (SUCCEEDED(hr) && elemPrompt)
		//{
		//	CComPtr <IXMLDOMElement> elemAudio = NULL;
		//	hr = pDoc->createElement(_T("audio"), &elemAudio);
		//	if (SUCCEEDED(hr) && elemAudio)
		//	{
		//		elemAudio->setAttribute(L"src", _variant_t(GetFile2().c_str()));
		//		elemPrompt->appendChild(elemAudio, NULL);
		//	}

		//	elemMenu->appendChild(elemPrompt, NULL);
		//}
		AddPromptAudio(GetFile2(), pDoc, elemMenu);

		////add choices
		//for each (BranchPtr branch in m_children)
		//{
		//	switch (branch->GetTypeMenu())
		//	{
		//	case MT_NOINPUT:
		//	case MT_NOMATCH:
		//	{
		//		break; //skip
		//	}
		//	default:
		//	{
		//		if (branch->GetDtmf().length())
		//		{
		//			CComPtr <IXMLDOMElement> elemChoice = NULL;
		//			hr = pDoc->createElement(_T("choice"), &elemChoice);
		//			if (SUCCEEDED(hr) && elemChoice)
		//			{
		//				elemChoice->setAttribute(L"next", _variant_t(std::wstring(L"#" + branch->GetMenuName()).c_str()));
		//				elemChoice->setAttribute(L"dtmf", _variant_t(branch->GetDtmf().c_str()));
		//				elemMenu->appendChild(elemChoice, NULL);
		//			}
		//		}
		//		break;
		//	}
		//	}

		//}
		AddChoices(pDoc, elemMenu);
		//add repeat 
		//if (GetGotoRepeat().length())
		//{
		//	CComPtr <IXMLDOMElement> elemChoice = NULL;
		//	hr = pDoc->createElement(_T("choice"), &elemChoice);
		//	if (SUCCEEDED(hr) && elemChoice)
		//	{
		//		if (!GetFile3().empty())
		//		{
		//			elemChoice->setAttribute(L"next", _variant_t(std::wstring(L"#" + GetMenuName() + L"_short").c_str()));
		//		}
		//		else
		//		{
		//			elemChoice->setAttribute(L"next", _variant_t(std::wstring(L"#" + GetMenuName() + L"_noinput_1").c_str()));
		//		}
		//		
		//		elemChoice->setAttribute(L"dtmf", _variant_t(GetGotoRepeat().c_str()));
		//		elemMenu->appendChild(elemChoice, NULL);
		//	}
		//}
		AddRepeat(!GetFile3().empty() ? std::wstring(GetMenuName() + L"_short") : std::wstring(GetMenuName() + L"_noinput_1"), pDoc, elemMenu);
		////add root
		//if (GetGotoRoot().length())
		//{
		//	CComPtr <IXMLDOMElement> elemChoice = NULL;
		//	hr = pDoc->createElement(_T("choice"), &elemChoice);
		//	if (SUCCEEDED(hr) && elemChoice)
		//	{
		//		std::wstring rootMenuName = GetMenuName();
		//		if (GetParentPtr().lock())
		//		{
		//			rootMenuName = GetParentPtr().lock()->GetLocalRoot();
		//		}
		//		elemChoice->setAttribute(L"next", _variant_t(std::wstring(L"#" + rootMenuName).c_str()));
		//		elemChoice->setAttribute(L"dtmf", _variant_t(GetGotoRoot().c_str()));

		//		elemMenu->appendChild(elemChoice, NULL);
		//	}
		//}
		AddRoot(/*GetMenuName(),*/ pDoc, elemMenu);

		////add back
		//if (GetGotoBack().length())
		//{
		//	CComPtr <IXMLDOMElement> elemChoice = NULL;
		//	hr = pDoc->createElement(_T("choice"), &elemChoice);
		//	if (SUCCEEDED(hr) && elemChoice && GetParentPtr().lock())
		//	{
		//		elemChoice->setAttribute(L"next", _variant_t(std::wstring(L"#" + GetMenuName()).c_str()));
		//		elemChoice->setAttribute(L"dtmf", _variant_t(GetGotoBack().c_str()));

		//		elemMenu->appendChild(elemChoice, NULL);
		//	}
		//}
		AddBack(pDoc, elemMenu);

		//add noinput
		if (!GetFile3().empty())
		{
			//CComPtr <IXMLDOMElement> elemNoinput = NULL;
			//hr = pDoc->createElement(_T("noinput"), &elemNoinput);
			//if (SUCCEEDED(hr) && elemNoinput)
			//{
			//	elemNoinput->setAttribute(L"count", _variant_t(1));

			//	CComPtr <IXMLDOMElement> elemGoto = NULL;
			//	hr = pDoc->createElement(_T("goto"), &elemGoto);
			//	if (SUCCEEDED(hr) && elemGoto)
			//	{
			//		elemGoto->setAttribute(L"next", _variant_t(std::wstring(L"#" + GetMenuName() + L"_noinput_2").c_str()));
			//		elemNoinput->appendChild(elemGoto, NULL);
			//	}

			//	elemMenu->appendChild(elemNoinput, NULL);
			//}
			AddNoinput(1, std::wstring(L"#" + GetMenuName() + L"_noinput_2"), pDoc, elemMenu);
		}

	}
	return elemMenu;
}

CComPtr <IXMLDOMElement> CSmartMenu::CreateCG2(CComPtr < IXMLDOMDocument > pDoc)
{
	if (!pDoc)
		NULL;

	if (GetFile3().empty())
		return NULL;

	CComPtr <IXMLDOMElement> elemMenu = NULL;
	HRESULT hr = pDoc->createElement(_T("menu"), &elemMenu);

	if (SUCCEEDED(hr) && elemMenu)
	{
		////set attributs
		//if (GetComment().length())
		//	elemMenu->setAttribute(L"comment", _variant_t(GetComment().c_str()));
		//elemMenu->setAttribute(L"id", _variant_t(std::wstring(GetMenuName() + L"_noinput_2").c_str()));
		SetMenuAttributes(std::wstring(GetMenuName() + L"_noinput_2"), elemMenu);

		////add prompt+audio
		//CComPtr <IXMLDOMElement> elemPrompt = NULL;
		//hr = pDoc->createElement(_T("prompt"), &elemPrompt);
		//if (SUCCEEDED(hr) && elemPrompt)
		//{
		//	CComPtr <IXMLDOMElement> elemAudio = NULL;
		//	hr = pDoc->createElement(_T("audio"), &elemAudio);
		//	if (SUCCEEDED(hr) && elemAudio)
		//	{
		//		elemAudio->setAttribute(L"src", _variant_t(GetFile3().c_str()));
		//		elemPrompt->appendChild(elemAudio, NULL);
		//	}

		//	elemMenu->appendChild(elemPrompt, NULL);
		//}

		AddPromptAudio(GetFile3(), pDoc, elemMenu);


		//// add goto
		//CComPtr <IXMLDOMElement> elemGoto = NULL;
		//hr = pDoc->createElement(_T("goto"), &elemGoto);
		//if (SUCCEEDED(hr) && elemGoto)
		//{
		//	elemGoto->setAttribute(L"next", _variant_t(std::wstring(L"#" + GetGoto()).c_str()));

		//	elemMenu->appendChild(elemGoto, NULL);
		//}
		AddGoto(pDoc, elemMenu, false);
	}

	return elemMenu;
}

CComPtr <IXMLDOMElement> CSmartMenu::CreateCG3(CComPtr < IXMLDOMDocument > pDoc)
{
	if (!pDoc)
		NULL;

	CComPtr <IXMLDOMElement> elemMenu = NULL;
	HRESULT hr = pDoc->createElement(_T("menu"), &elemMenu);

	if (SUCCEEDED(hr) && elemMenu)
	{
		////set attributs
		//if (GetComment().length())
		//	elemMenu->setAttribute(L"comment", _variant_t(GetComment().c_str()));
		//elemMenu->setAttribute(L"id", _variant_t(std::wstring(GetMenuName() + L"_short").c_str()));
		SetMenuAttributes(std::wstring(GetMenuName() + L"_short"), elemMenu);

		////add prompt+audio
		//CComPtr <IXMLDOMElement> elemPrompt = NULL;
		//hr = pDoc->createElement(_T("prompt"), &elemPrompt);
		//if (SUCCEEDED(hr) && elemPrompt)
		//{
		//	CComPtr <IXMLDOMElement> elemAudio = NULL;
		//	hr = pDoc->createElement(_T("audio"), &elemAudio);
		//	if (SUCCEEDED(hr) && elemAudio)
		//	{
		//		elemAudio->setAttribute(L"src", _variant_t(GetFile4().c_str()));
		//		elemPrompt->appendChild(elemAudio, NULL);
		//	}

		//	elemMenu->appendChild(elemPrompt, NULL);
		//}
		AddPromptAudio(GetFile4(), pDoc, elemMenu);

		////add choices
		//for each (BranchPtr branch in m_children)
		//{
		//	switch (branch->GetTypeMenu())
		//	{
		//	case MT_NOINPUT:
		//	case MT_NOMATCH:
		//	{
		//		break; //skip
		//	}
		//	default:
		//	{
		//		if (branch->GetDtmf().length())
		//		{
		//			CComPtr <IXMLDOMElement> elemChoice = NULL;
		//			hr = pDoc->createElement(_T("choice"), &elemChoice);
		//			if (SUCCEEDED(hr) && elemChoice)
		//			{
		//				elemChoice->setAttribute(L"next", _variant_t(std::wstring(L"#" + branch->GetMenuName()).c_str()));
		//				elemChoice->setAttribute(L"dtmf", _variant_t(branch->GetDtmf().c_str()));
		//				elemMenu->appendChild(elemChoice, NULL);
		//			}
		//		}
		//		break;
		//	}
		//	}

		//}

		AddChoices(pDoc, elemMenu);
		////add repeat 
		//if (GetGotoRepeat().length())
		//{
		//	CComPtr <IXMLDOMElement> elemChoice = NULL;
		//	hr = pDoc->createElement(_T("choice"), &elemChoice);
		//	if (SUCCEEDED(hr) && elemChoice)
		//	{
		//		elemChoice->setAttribute(L"next", _variant_t(std::wstring(L"#" + GetMenuName() + L"_short").c_str()));
		//		elemChoice->setAttribute(L"dtmf", _variant_t(GetGotoRepeat().c_str()));
		//		elemMenu->appendChild(elemChoice, NULL);
		//	}
		//}
		AddRepeat(std::wstring(GetMenuName() + L"_short"), pDoc, elemMenu);
		//add root
		//if (GetGotoRoot().length())
		//{
		//	CComPtr <IXMLDOMElement> elemChoice = NULL;
		//	hr = pDoc->createElement(_T("choice"), &elemChoice);
		//	if (SUCCEEDED(hr) && elemChoice)
		//	{
		//		std::wstring rootMenuName = GetMenuName();
		//		if (GetParentPtr().lock())
		//		{
		//			rootMenuName = GetParentPtr().lock()->GetLocalRoot();
		//		}
		//		elemChoice->setAttribute(L"next", _variant_t(std::wstring(L"#" + rootMenuName).c_str()));
		//		elemChoice->setAttribute(L"dtmf", _variant_t(GetGotoRoot().c_str()));

		//		elemMenu->appendChild(elemChoice, NULL);
		//	}
		//}
		AddRoot(/*GetMenuName(),*/ pDoc, elemMenu);

		////add back
		//if (GetGotoBack().length())
		//{
		//	CComPtr <IXMLDOMElement> elemChoice = NULL;
		//	hr = pDoc->createElement(_T("choice"), &elemChoice);
		//	if (SUCCEEDED(hr) && elemChoice && GetParentPtr().lock())
		//	{
		//		std::wstring backMenuName = GetParentPtr().lock()->GetMenuName();

		//		elemChoice->setAttribute(L"next", _variant_t(std::wstring(L"#" + backMenuName).c_str()));
		//		elemChoice->setAttribute(L"dtmf", _variant_t(GetGotoBack().c_str()));

		//		elemMenu->appendChild(elemChoice, NULL);
		//	}
		//}
		AddBack(pDoc, elemMenu);

		//add noinput
		if (!GetFile3().empty())
		{
			//CComPtr <IXMLDOMElement> elemNoinput = NULL;
			//hr = pDoc->createElement(_T("noinput"), &elemNoinput);
			//if (SUCCEEDED(hr) && elemNoinput)
			//{
			//	elemNoinput->setAttribute(L"count", _variant_t(1));

			//	CComPtr <IXMLDOMElement> elemGoto = NULL;
			//	hr = pDoc->createElement(_T("goto"), &elemGoto);
			//	if (SUCCEEDED(hr) && elemGoto)
			//	{
			//		elemGoto->setAttribute(L"next", _variant_t(std::wstring(L"#" + GetMenuName() + L"_noinput_2").c_str()));
			//		elemNoinput->appendChild(elemGoto, NULL);
			//	}

			//	elemMenu->appendChild(elemNoinput, NULL);
			//}
			AddNoinput(1, std::wstring(L"#" + GetMenuName() + L"_noinput_2"), pDoc, elemMenu);
		}

	}
	return elemMenu;
}


void CSmartMenu::PushVXMLDOMItems(CComPtr < IXMLDOMDocument > pDoc, CComPtr <IXMLDOMElement> elemVXML)
{
	CComPtr <IXMLDOMElement> itemCG = CreateCG(pDoc);
	if (itemCG)
	{
		elemVXML->appendChild(itemCG, NULL);
	}

	CComPtr <IXMLDOMElement> itemCG1 = CreateCG1(pDoc);
	if (itemCG1)
	{
		elemVXML->appendChild(itemCG1, NULL);
	}

	CComPtr <IXMLDOMElement> itemCG2 = CreateCG2(pDoc);
	if (itemCG1)
	{
		elemVXML->appendChild(itemCG2, NULL);
	}

	CComPtr <IXMLDOMElement> itemCG3 = CreateCG3(pDoc);
	if (itemCG3)
	{
		elemVXML->appendChild(itemCG3, NULL);
	}
}


/******************************* eof *************************************/