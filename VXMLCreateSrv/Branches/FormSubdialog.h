/************************************************************************/
/* Name     : VXMLCreateSrv\Branches\FormSubdialog.h                    */
/* Author   : Andrey Alekseev                                           */
/* Project  : VXMLCreateSrv                                             */
/* Company  : Forte-CT                                                  */
/* Date     : 07 Aug 2015                                               */
/************************************************************************/
#pragma once
#include "BaseBranch.h"
#include "../GetFuncParamByBranch.h"

enum class E_SUBDIALOGTYPE
{
	SDT_VXML = 0,
	SDT_ST3
};

class CFormSubdialog : public CBaseBranch //<CFormSubdialog>
{
	//friend class CBaseBranch < CFormSubdialog > ;
public:
	CFormSubdialog(const int& _iTreeId,
		const int& _iParentId,
		const std::wstring& _sMenuName,
		//const std::wstring& _sMenuText,
		const std::wstring& _sDtmf,
		const std::wstring& _sComment,
		//const std::wstring& _sWavName,
		const int& _iSubDialogId,
		//const bool& _bIsVisible,
		const std::wstring& _sGotoRepeat,
		const std::wstring& _sGotoBack,
		const std::wstring& _sGoto,
		const int& _bIsCondition,
		const std::wstring & _sFile1,
		const std::wstring& _sSubDialogName,
		const int& _iSubDialogType
		);

public:
	virtual int GetTypeMenu() { return MT_FORM_SUBDIALOG; }

	//void AddChild(BranchPtr child){}
	CComPtr <IXMLDOMElement> CreateXMLItem(CComPtr < IXMLDOMDocument > pDoc);
	virtual void PushVXMLDOMItems(CComPtr < IXMLDOMDocument > pDoc, CComPtr <IXMLDOMElement> elemVXML);
	bool IsValid();

	CComPtr <IXMLDOMElement> CreateMainForm(CComPtr < IXMLDOMDocument > pDoc, const CGetFuncParamDataHelper::FuncParamCollector& params);
	CComPtr <IXMLDOMElement> CreateDependentForm(CComPtr < IXMLDOMDocument > pDoc, const CGetFuncParamDataHelper::FuncParamCollector& params);

	//void PushVXMLDOMItems(CComPtr < IXMLDOMDocument > pDoc, CComPtr <IXMLDOMElement> elemVXML);
	//std::wstring GetLocalRoot();

private:
	std::wstring GetSubDialogName()const noexcept { return m_sSubDialogName; }
	int          GetSubDialogType()const noexcept { return m_iSubDialogType; }

private:
	std::wstring m_sSubDialogName;
	int m_iSubDialogType;

};


/******************************* eof *************************************/