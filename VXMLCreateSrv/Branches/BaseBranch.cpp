/************************************************************************/
/* Name     : VXMLCreateSrv\Branches\BaseBranch.cpp                     */
/* Author   : Andrey Alekseev                                           */
/* Project  : VXMLCreateSrv                                             */
/* Company  : Forte-CT                                                  */
/* Date     : 05 Aug 2015                                               */
/************************************************************************/

#include "stdafx.h"
#include <boost/format.hpp>
#include "BaseBranch.h"
#include "../getconditionsbybranch.h"


//template <class Derived>
//CBaseBranch<Derived>::CBaseBranch(const int& _iTreeId,
//	const int& _iParentId,
//	const std::wstring& _sMenuName,
//	//const std::wstring& _sMenuText,
//	const std::wstring& _sDtmf,
//	const std::wstring& _sComment,
//	const std::wstring& _sWavName,
//	const int& _iSubDialogId,
//	//const bool& _bIsVisible,
//	const int& _iRepeatCnt,
//	const std::wstring& _sGotoRepeat,
//	const std::wstring& _sGotoBack,
//	const std::wstring& _sGoto,
//	const std::wstring& _sGotoRoot,
//	const std::wstring& _sTermChar,
//	const int& _iTimeOut,
//	const int& _bRoot,
//	const int& _bIsCondition)
//	: m_iTreeId(_iTreeId), m_iParentId(_iParentId), m_sMenuName(_sMenuName), //m_sMenuText(_sMenuText), 
//	m_sDtmf(_sDtmf), m_sComment(_sComment),	m_sWavName(_sWavName), m_iSubDialogId(_iSubDialogId), 
//	/*m_bIsVisible(_bIsVisible),*/ m_iRepeatCnt(_iRepeatCnt), m_sGotoRepeat(_sGotoRepeat), m_sGotoBack(_sGotoBack), m_sGoto(_sGoto),
//	m_sGotoRoot(_sGotoRoot), m_sTermChar(_sTermChar), m_iTimeOut(_iTimeOut), m_bIsRoot(_bRoot?true:false),
//	m_bIsCondition(_bIsCondition?true:false)
//{
//}

//
//CBaseBranch::~CBaseBranch()
//{
//}

std::wstring CBaseBranch::GetLocalRoot()const
{
	if (m_bIsRoot)
		return GetMenuName();

	if (GetParentPtr().lock())
		return GetParentPtr().lock()->GetLocalRoot();

	return GetMenuName();
}

void CBaseBranch::PushVXMLDOMItems(CComPtr < IXMLDOMDocument > pDoc, CComPtr <IXMLDOMElement> elemVXML)
{
	CComPtr <IXMLDOMElement> itemMenu = CreateXMLItem(pDoc);
	if (itemMenu)
	{
		elemVXML->appendChild(itemMenu, NULL);
	}
}


void CBaseBranch::SetMenuAttributes(const std::wstring & _menuName, CComPtr <IXMLDOMElement> elemMenu)
{
	if (GetComment().length())
		elemMenu->setAttribute(L"comment", _variant_t(GetComment().c_str()));
	elemMenu->setAttribute(L"id", _variant_t(_menuName.c_str()));
}

void CBaseBranch::AddPromptAudio(const std::wstring & _audioName, CComPtr < IXMLDOMDocument > pDoc, CComPtr <IXMLDOMElement> elemMenu)
{
	//add prompt+audio
	CComPtr <IXMLDOMElement> elemPrompt = NULL;
	HRESULT hr = pDoc->createElement(_T("prompt"), &elemPrompt);
	if (SUCCEEDED(hr) && elemPrompt)
	{
		CComPtr <IXMLDOMElement> elemAudio = NULL;
		hr = pDoc->createElement(_T("audio"), &elemAudio);
		if (SUCCEEDED(hr) && elemAudio)
		{
			elemAudio->setAttribute(L"src", _variant_t(_audioName.c_str()));
			elemPrompt->appendChild(elemAudio, NULL);
		}

		elemMenu->appendChild(elemPrompt, NULL);
	}

}

void CBaseBranch::AddGoto(CComPtr < IXMLDOMDocument > pDoc, CComPtr <IXMLDOMElement> elemMenu, bool expression)
{
	// add goto
	CComPtr <IXMLDOMElement> elemGoto = NULL;
	HRESULT hr = pDoc->createElement(_T("goto"), &elemGoto);
	if (SUCCEEDED(hr) && elemGoto)
	{
		if (expression)
		{
			elemGoto->setAttribute(L"expr", _variant_t(L"next"));
		}
		else
		{
			elemGoto->setAttribute(L"next", _variant_t(std::wstring(L"#" + GetGoto()).c_str()));
		}

		elemMenu->appendChild(elemGoto, NULL);
	}


}

void CBaseBranch::AddAudio(CComPtr < IXMLDOMDocument > pDoc, CComPtr <IXMLDOMElement> elemMenu)
{
	//add audio
	if (!GetFile1().empty())
	{
		CComPtr <IXMLDOMElement> elemAudio = NULL;
		HRESULT hr = pDoc->createElement(_T("audio"), &elemAudio);
		if (SUCCEEDED(hr) && elemAudio)
		{
			elemAudio->setAttribute(L"src", _variant_t(GetFile1().c_str()));
			elemMenu->appendChild(elemAudio, NULL);
		}
	}
}

void CBaseBranch::AddChoices(CComPtr < IXMLDOMDocument > pDoc, CComPtr <IXMLDOMElement> elemMenu)
{
	for each (SafeBranchPtr safebranch in m_children)
	{
		BranchPtr branch = safebranch.lock();
		switch (branch->GetTypeMenu())
		{
		case MT_NOINPUT:
		case MT_NOMATCH:
		{
			break;
		}
		default:
		{
			if (branch->GetDtmf().length())
			{
				CComPtr <IXMLDOMElement> elemChoice = NULL;
				HRESULT hr = pDoc->createElement(_T("choice"), &elemChoice);
				if (SUCCEEDED(hr) && elemChoice)
				{
					elemChoice->setAttribute(L"next", _variant_t(std::wstring(L"#" + branch->GetMenuName()).c_str()));
					elemChoice->setAttribute(L"dtmf", _variant_t(branch->GetDtmf().c_str()));
					elemMenu->appendChild(elemChoice, NULL);
				}
			}
			break;
		}
		}

	}
}

void CBaseBranch::AddRepeat(const std::wstring & _menuName, CComPtr < IXMLDOMDocument > pDoc, CComPtr <IXMLDOMElement> elemMenu)
{
	if (GetGotoRepeat().length())
	{
		CComPtr <IXMLDOMElement> elemChoice = NULL;
		HRESULT hr = pDoc->createElement(_T("choice"), &elemChoice);
		if (SUCCEEDED(hr) && elemChoice)
		{
			elemChoice->setAttribute(L"next", _variant_t(std::wstring(L"#" + _menuName).c_str()));
			elemChoice->setAttribute(L"dtmf", _variant_t(GetGotoRepeat().c_str()));
			//CComPtr <IXMLDOMElement> elemReprompt = NULL;
			//hr = pDoc->createElement(_T("reprompt"), &elemReprompt);
			//if (SUCCEEDED(hr) && elemReprompt)
			//{
			//	elemChoice->appendChild(elemReprompt, NULL);
			//}

			elemMenu->appendChild(elemChoice, NULL);

		}
	}
}

void CBaseBranch::AddRoot(/*const std::wstring & rootMenuName,*/ CComPtr < IXMLDOMDocument > pDoc, CComPtr <IXMLDOMElement> elemMenu)
{
	if (GetGotoRoot().length())
	{
		CComPtr <IXMLDOMElement> elemChoice = NULL;
		HRESULT hr = pDoc->createElement(_T("choice"), &elemChoice);
		if (SUCCEEDED(hr) && elemChoice)
		{
			std::wstring rootMenuName = GetLocalRoot();
			elemChoice->setAttribute(L"next", _variant_t(std::wstring(L"#" + rootMenuName).c_str()));
			elemChoice->setAttribute(L"dtmf", _variant_t(GetGotoRoot().c_str()));

			elemMenu->appendChild(elemChoice, NULL);
		}
	}
}

void CBaseBranch::AddBack(CComPtr < IXMLDOMDocument > pDoc, CComPtr <IXMLDOMElement> elemMenu)
{
	if (GetGotoBack().length())
	{
		CComPtr <IXMLDOMElement> elemChoice = NULL;
		HRESULT hr = pDoc->createElement(_T("choice"), &elemChoice);
		if (SUCCEEDED(hr) && elemChoice && GetParentPtr().lock())
		{
			std::wstring backMenuName = GetParentPtr().lock()->GetMenuName();

			elemChoice->setAttribute(L"next", _variant_t(std::wstring(L"#" + backMenuName).c_str()));
			elemChoice->setAttribute(L"dtmf", _variant_t(GetGotoBack().c_str()));

			elemMenu->appendChild(elemChoice, NULL);
		}
	}

}


void CBaseBranch::AddNoinput(const int& count, const std::wstring& next, CComPtr < IXMLDOMDocument > pDoc, CComPtr <IXMLDOMElement> elemMenu)
{
	CComPtr <IXMLDOMElement> elemNoinput = NULL;
	HRESULT hr = pDoc->createElement(_T("noinput"), &elemNoinput);
	if (SUCCEEDED(hr) && elemNoinput)
	{
		elemNoinput->setAttribute(L"count", _variant_t(count));

		CComPtr <IXMLDOMElement> elemGoto = NULL;
		hr = pDoc->createElement(_T("goto"), &elemGoto);
		if (SUCCEEDED(hr) && elemGoto)
		{
			elemGoto->setAttribute(L"next", _variant_t(next.c_str()));
			elemNoinput->appendChild(elemGoto, NULL);
		}

		elemMenu->appendChild(elemNoinput, NULL);
	}
}


void CBaseBranch::AddNomatch(const int& count, const std::wstring& next, CComPtr < IXMLDOMDocument > pDoc, CComPtr <IXMLDOMElement> elemMenu)
{
	CComPtr <IXMLDOMElement> elemNomatch = NULL;
	HRESULT hr = pDoc->createElement(_T("nomatch"), &elemNomatch);
	if (SUCCEEDED(hr) && elemNomatch)
	{
		elemNomatch->setAttribute(L"count", _variant_t(count));

		CComPtr <IXMLDOMElement> elemGoto = NULL;
		hr = pDoc->createElement(_T("goto"), &elemGoto);
		if (SUCCEEDED(hr) && elemGoto)
		{
			elemGoto->setAttribute(L"next", _variant_t(next.c_str()));
			elemNomatch->appendChild(elemGoto, NULL);
		}

		elemMenu->appendChild(elemNomatch, NULL);
	}
}

bool CBaseBranch::AddConditions( CComPtr<IXMLDOMDocument> pDoc, CComPtr<IXMLDOMElement> elemMenu)
{
	HRESULT hr = S_OK;

	CGetDataHelper::ConditionCollector conditions;
	// if condition

	if (this->GetIsCondition())
	{
#ifndef TEST_DATA_BASE
		if (1)
		{
			CGetDataHelper collector;
			conditions = collector.GetConditionByBranch(this->GetId());
		}
#else
		if (1) //only for test
		{
			conditions.push_back(CGetDataHelper::CGotoCondition(L"menu_1.utterance = 1", L"menu_11"));
			conditions.push_back(CGetDataHelper::CGotoCondition(L"menu_1.utterance = 2", L"menu_12"));
		}
#endif
	}



	if (conditions.empty())
		return false;

	
	CComPtr <IXMLDOMElement> elemVar = NULL;
	hr = pDoc->createElement(_T("var"), &elemVar);
	if (SUCCEEDED(hr) && elemVar)
	{
		std::wstring expr(L"'#" + GetGoto() + L"'");
		elemVar->setAttribute(L"expr", _variant_t(expr.c_str()));
		elemVar->setAttribute(L"name", _variant_t(L"next"));
		elemMenu->appendChild(elemVar, NULL);
	}

	bool bFirst = true;;
	CComPtr <IXMLDOMElement> elemIf = NULL;
	hr = pDoc->createElement(_T("if"), &elemIf);
	if (SUCCEEDED(hr) && elemIf)
	{
		for each (CGetDataHelper::CGotoCondition cond in conditions)
		{
			CComPtr <IXMLDOMElement> elemElseif = NULL;
			if (bFirst)
			{
				bFirst = false;
				elemIf->setAttribute(L"cond", _variant_t(cond.GetCondition().c_str()));
			}
			else
			{
				hr = pDoc->createElement(_T("elseif"), &elemElseif);
				if (SUCCEEDED(hr) && elemIf)
				{
					elemElseif->setAttribute(L"cond", _variant_t(cond.GetCondition().c_str()));
					elemIf->appendChild(elemElseif, NULL);
				}
			}


			//expression
			CComPtr <IXMLDOMElement> elemAssign = NULL;
			hr = pDoc->createElement(_T("assign"), &elemAssign);
			if (SUCCEEDED(hr) && elemAssign)
			{
				elemAssign->setAttribute(L"expr", _variant_t(cond.GetExpression().c_str()));
				elemAssign->setAttribute(L"name", _variant_t(L"next"));
				elemIf->appendChild(elemAssign, NULL);
			}
		}
		elemMenu->appendChild(elemIf, NULL);
	}

	return true;
}



void CBaseBranch::AddAddToHistory(CComPtr<IXMLDOMDocument> pDoc, CComPtr<IXMLDOMElement> elemMenu)
{
	CComPtr <IXMLDOMElement> elemScript = NULL;
	HRESULT hr = pDoc->createElement(_T("script"), &elemScript);
	if (SUCCEEDED(hr) && elemScript)
	{
		std::wstring scriptSource = (boost::wformat(L"addToAbonentHistory \"#%s\"") % GetMenuName().c_str()).str();

		//elemScript->put_text(_bstr_t(scriptSource.c_str()));
		CComPtr <IXMLDOMCDATASection> elemCDATA = NULL;
		hr = pDoc->createCDATASection(_bstr_t(scriptSource.c_str()), &elemCDATA);

		if (SUCCEEDED(hr) && elemCDATA)
		{
			elemScript->appendChild(elemCDATA, NULL);
		}
		elemMenu->appendChild(elemScript, NULL);
	}

}


/******************************* eof *************************************/
