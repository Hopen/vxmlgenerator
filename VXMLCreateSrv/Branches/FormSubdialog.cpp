/************************************************************************/
/* Name     : VXMLCreateSrv\Branches\FormSubdialog.cpp                  */
/* Author   : Andrey Alekseev                                           */
/* Project  : VXMLCreateSrv                                             */
/* Company  : Forte-CT                                                  */
/* Date     : 07 Aug 2015                                               */
/************************************************************************/
#include "stdafx.h"
#include <map>
#include <sstream>
#include <boost/format.hpp>
#include "FormSubdialog.h"

/******************************* SubDialog database catalog *************************************/

typedef std::map<int, std::wstring> SubDialogsContainer;

//static void MakeSubDialogsList(SubDialogsContainer& list)
//{
//	list[1] = L"pay_check";
//	//...
//}
//
//static std::wstring GetSubDialogById(const int& id)
//{
//	SubDialogsContainer list;
//	MakeSubDialogsList(list);
//
//	SubDialogsContainer::const_iterator cit = list.find(id);
//	if (cit != list.end())
//		return cit->second;
//	return L"Unknown";
//}

/******************************* CFormSubdialog *************************************/

CFormSubdialog::CFormSubdialog(const int& _iTreeId,
	const int& _iParentId,
	const std::wstring& _sMenuName,
	//const std::wstring& _sMenuText,
	const std::wstring& _sDtmf,
	const std::wstring& _sComment,
	//const std::wstring& _sWavName,
	const int& _iSubDialogId,
	//const bool& _bIsVisible,
	const std::wstring& _sGotoRepeat,
	const std::wstring& _sGotoBack,
	const std::wstring& _sGoto,
	const int& _bIsCondition,
	const std::wstring& _sFile1,
	const std::wstring& _sSubDialogName,
	const int& _iSubDialogType)
	: CBaseBranch(_iTreeId, _iParentId, _sMenuName, //_sMenuText,
	_sDtmf, _sComment, /*_sWavName,*/ _iSubDialogId,
	/*_bIsVisible,*/0, _sGotoRepeat, _sGotoBack, _sGoto, L"", L"empty", 0, false, _bIsCondition, false, _sFile1),
	m_sSubDialogName(_sSubDialogName), m_iSubDialogType(_iSubDialogType)
{
}

bool CFormSubdialog::IsValid()
{
	// the menu of this type mast have: 
	//   1. subdialog_id
	//   2. goto
	//   3. id
	//   4. sub_dialog_name

	if (!GetMenuName().length())
		return false;
	if (!GetSubDialogId())
		return false;
	if (!GetGoto().length())
		return false;
	if (!GetSubDialogName().length())
		return false;
	return true;
}

CComPtr <IXMLDOMElement> CFormSubdialog::CreateXMLItem(CComPtr < IXMLDOMDocument > pDoc)
{
	return NULL;

	//if (!pDoc)
	//	NULL;

	//std::wstring sSubDialogName = GetSubDialogById(GetSubDialogId());

	//CComPtr <IXMLDOMElement> elemForm = NULL;
	//HRESULT hr = pDoc->createElement(_T("form"), &elemForm);

	////create main subdialog
	//if (SUCCEEDED(hr) && elemForm)
	//{
	//	//set attributs
	//	elemForm->setAttribute(L"id", _variant_t(GetMenuName().c_str()));

	//	CGetFuncParamDataHelper::FuncParamCollector params;

	//	if (1)
	//	{
	//		CGetFuncParamDataHelper collector;
	//		params = collector.GetFuncParamByBranch(this->GetId());
	//	}

	//	if (0)
	//	{
	//		params.emplace_back(CGetFuncParamDataHelper::CFuncParam(L"bnum", L"901123123"));
	//		params.emplace_back(CGetFuncParamDataHelper::CFuncParam(L"anum", L"12793812"));
	//	}

	//	for each (CGetFuncParamDataHelper::CFuncParam param_pair in params)
	//	{
	//		CComPtr <IXMLDOMElement> elemVar = NULL;
	//		hr = pDoc->createElement(_T("var"), &elemVar);
	//		if (SUCCEEDED(hr) && elemVar)
	//		{
	//			elemVar->setAttribute(L"expr", _variant_t(param_pair.GetParamValue().c_str()));
	//			elemVar->setAttribute(L"name", _variant_t(param_pair.GetParamName ().c_str()));
	//			elemForm->appendChild(elemVar, NULL);
	//		}
	//	}

	//	CComPtr <IXMLDOMElement> elemSubdialog = NULL;
	//	hr = pDoc->createElement(_T("subdialog"), &elemSubdialog);

	//	if (SUCCEEDED(hr) && elemSubdialog)
	//	{
	//		//elemSubdialog->setAttribute(L"src", _variant_t(std::wstring(L"#" + sSubDialogName).c_str()));
	//		if (params.size())
	//		{
	//			std::wstring sNamelist;
	//			for each(const CGetFuncParamDataHelper::CFuncParam& param in params)
	//			{
	//				sNamelist += param.GetParamName() + L" ";
	//			}
	//			sNamelist.erase(sNamelist.size() - 1);

	//			elemSubdialog->setAttribute(L"namelist", _variant_t(sNamelist.c_str()));
	//			
	//			
	//			//std::wostringstream out;
	//			//std::copy_if(params.begin(),
	//			//	params.end(),
	//			//	std::ostream_iterator<std::wstring, wchar_t>(out, L" "),
	//			//	[](const CGetFuncParamDataHelper::CFuncParam& param) ->std::wstring
	//			//{
	//			//	return param.GetParamName();
	//			//}
	//			//	);

	//			//sNamelist = out.str();
	//			//sNamelist.erase(sNamelist.size() - 1);
	//		}

	//		//elemSubdialog->setAttribute(L"function", _variant_t(sSubDialogName.c_str()));
	//		elemSubdialog->setAttribute(L"src", _variant_t(std::wstring(L"#" + sSubDialogName).c_str()));
	//		elemSubdialog->setAttribute(L"name", _variant_t(std::wstring(L"sub_" + sSubDialogName).c_str()));


	//		CComPtr <IXMLDOMElement> elemFilled = NULL;
	//		hr = pDoc->createElement(_T("filled"), &elemFilled);
	//		if (SUCCEEDED(hr) && elemFilled)
	//		{
	//			CComPtr <IXMLDOMElement> elemGoto = NULL;
	//			hr = pDoc->createElement(_T("goto"), &elemGoto);
	//			if (SUCCEEDED(hr) && elemGoto)
	//			{
	//				elemGoto->setAttribute(L"next", _variant_t(GetGoto().c_str()));
	//				elemFilled->appendChild(elemGoto, NULL);
	//			}

	//			elemSubdialog->appendChild(elemFilled, NULL);
	//		}

	//		elemForm->appendChild(elemSubdialog, NULL);

	//	}

	//}
	//return elemForm;
}


CComPtr<IXMLDOMElement> CFormSubdialog::CreateMainForm(CComPtr<IXMLDOMDocument> pDoc, const CGetFuncParamDataHelper::FuncParamCollector& params)
{
	if (!pDoc)
		return NULL;

	CComPtr <IXMLDOMElement> elemForm = NULL;
	std::wstring sSubDialogName = GetSubDialogName();

	HRESULT hr = pDoc->createElement(_T("form"), &elemForm);

	//create main subdialog
	if (SUCCEEDED(hr) && elemForm)
	{
		//set attributs
		elemForm->setAttribute(L"id", _variant_t(GetMenuName().c_str()));



		for each (CGetFuncParamDataHelper::CFuncParam param_pair in params)
		{
			CComPtr <IXMLDOMElement> elemVar = NULL;
			hr = pDoc->createElement(_T("var"), &elemVar);
			if (SUCCEEDED(hr) && elemVar)
			{
				elemVar->setAttribute(L"expr", _variant_t(param_pair.GetParamValue().c_str()));
				elemVar->setAttribute(L"name", _variant_t(param_pair.GetParamName().c_str()));
				elemForm->appendChild(elemVar, NULL);
			}
		}

		CComPtr <IXMLDOMElement> elemSubdialog = NULL;
		hr = pDoc->createElement(_T("subdialog"), &elemSubdialog);

		if (SUCCEEDED(hr) && elemSubdialog)
		{
			//elemSubdialog->setAttribute(L"src", _variant_t(std::wstring(L"#" + sSubDialogName).c_str()));
			if (params.size())
			{
				std::wstring sNamelist;
				for each(const CGetFuncParamDataHelper::CFuncParam& param in params)
				{
					sNamelist += param.GetParamName() + L" ";
				}
				sNamelist.erase(sNamelist.size() - 1);

				elemSubdialog->setAttribute(L"namelist", _variant_t(sNamelist.c_str()));


				//std::wostringstream out;
				//std::copy_if(params.begin(),
				//	params.end(),
				//	std::ostream_iterator<std::wstring, wchar_t>(out, L" "),
				//	[](const CGetFuncParamDataHelper::CFuncParam& param) ->std::wstring
				//{
				//	return param.GetParamName();
				//}
				//	);

				//sNamelist = out.str();
				//sNamelist.erase(sNamelist.size() - 1);
			}

			if (GetSubDialogType() == static_cast<int>(E_SUBDIALOGTYPE::SDT_VXML))
			{
				elemSubdialog->setAttribute(L"src", _variant_t(std::wstring(L"#" + sSubDialogName).c_str()));
			}
			else
			{
				elemSubdialog->setAttribute(L"function", _variant_t(sSubDialogName.c_str()));
			}
			
			elemSubdialog->setAttribute(L"name", _variant_t(std::wstring(L"sub_" + sSubDialogName).c_str()));


			CComPtr <IXMLDOMElement> elemFilled = NULL;
			hr = pDoc->createElement(_T("filled"), &elemFilled);
			if (SUCCEEDED(hr) && elemFilled)
			{
				bool bConditionExist = AddConditions(pDoc, elemFilled);

				AddGoto(pDoc, elemFilled, bConditionExist);

				//CComPtr <IXMLDOMElement> elemGoto = NULL;
				//hr = pDoc->createElement(_T("goto"), &elemGoto);
				//if (SUCCEEDED(hr) && elemGoto)
				//{
				//	elemGoto->setAttribute(L"next", _variant_t(GetGoto().c_str()));
				//	elemFilled->appendChild(elemGoto, NULL);
				//}

				elemSubdialog->appendChild(elemFilled, NULL);
			}

			elemForm->appendChild(elemSubdialog, NULL);

		}

	}
	return elemForm;
}

static std::wstring GetParamList(const CGetFuncParamDataHelper::FuncParamCollector& params, wchar_t wcDeleter)
{
	std::wstring sNamelist;
	if (params.size())
	{
		for each(const CGetFuncParamDataHelper::CFuncParam& param in params)
		{
			sNamelist += param.GetParamName() + wcDeleter;
		}

		sNamelist.erase(sNamelist.size() - 1);
	}

	return sNamelist;
}

//CComPtr<IXMLDOMElement> CFormSubdialog::CreateDependentForm(CComPtr<IXMLDOMDocument> pDoc, const CGetFuncParamDataHelper::FuncParamCollector& params)
//{
//	if (!pDoc)
//		return NULL;
//
//	CComPtr <IXMLDOMElement> elemForm = NULL;
//	HRESULT hr = pDoc->createElement(_T("form"), &elemForm);
//	std::wstring sSubDialogName = GetSubDialogName();
//
//	//create main subdialog
//	if (SUCCEEDED(hr) && elemForm)
//	{
//		//set attributs
//		elemForm->setAttribute(L"id", _variant_t(sSubDialogName.c_str()));
//
//		CComPtr <IXMLDOMElement> elemField = NULL;
//		hr = pDoc->createElement(_T("field"), &elemField);
//
//		if (SUCCEEDED(hr) && elemField)
//		{
//			elemField->setAttribute(L"name", _variant_t(std::wstring(L"field_" + sSubDialogName).c_str()));
//		
//			CComPtr <IXMLDOMElement> elemScript = NULL;
//			hr = pDoc->createElement(_T("script"), &elemScript);
//
//			if (SUCCEEDED(hr) && elemScript)
//			{
//				std::wstring ParamList = GetParamList(params, ',');
//				std::wstring scriptSource = (boost::wformat(L"<![CDATA[ %s(%s) ]]>") % sSubDialogName.c_str() % ParamList.c_str()).str();
//
//				elemScript->put_text(_bstr_t(scriptSource.c_str()));
//
//				elemField->appendChild(elemScript, NULL);
//			}
//
//			CComPtr <IXMLDOMElement> elemFilled = NULL;
//			hr = pDoc->createElement(_T("filled"), &elemFilled);
//			if (SUCCEEDED(hr) && elemFilled)
//			{
//				CComPtr <IXMLDOMElement> elemReturn = NULL;
//				hr = pDoc->createElement(_T("return"), &elemReturn);
//				if (SUCCEEDED(hr) && elemReturn)
//				{
//					std::wstring ParamList = GetParamList(params, ' ');
//
//					elemReturn->setAttribute(L"namelist", _variant_t(ParamList.c_str()));
//					elemFilled->appendChild(elemReturn, NULL);
//				}
//
//
//				elemField->appendChild(elemFilled, NULL);
//			}
//
//			elemForm->appendChild(elemField, NULL);
//
//		}
//
//	}
//	return elemForm;
//}


CComPtr<IXMLDOMElement> CFormSubdialog::CreateDependentForm(CComPtr<IXMLDOMDocument> pDoc, const CGetFuncParamDataHelper::FuncParamCollector& params)
{
	if (!pDoc)
		return NULL;

	CComPtr <IXMLDOMElement> elemMenu = NULL;
	HRESULT hr = pDoc->createElement(_T("menu"), &elemMenu);
	std::wstring sSubDialogName = GetSubDialogName();

	//create main subdialog
	if (SUCCEEDED(hr) && elemMenu)
	{
		//set attributs
		elemMenu->setAttribute(L"id", _variant_t(sSubDialogName.c_str()));

		CComPtr <IXMLDOMElement> elemScript = NULL;
		hr = pDoc->createElement(_T("script"), &elemScript);

		if (SUCCEEDED(hr) && elemScript)
		{
			std::wstring ParamList = GetParamList(params, ',');
			std::wstring scriptSource = (boost::wformat(L"<![CDATA[ %s(%s) ]]>") % sSubDialogName.c_str() % ParamList.c_str()).str();

			elemScript->put_text(_bstr_t(scriptSource.c_str()));

			elemMenu->appendChild(elemScript, NULL);
		}

		CComPtr <IXMLDOMElement> elemReturn = NULL;
		hr = pDoc->createElement(_T("return"), &elemReturn);
		if (SUCCEEDED(hr) && elemReturn)
		{
			std::wstring ParamList = GetParamList(params, ' ');

			elemReturn->setAttribute(L"namelist", _variant_t(ParamList.c_str()));
			elemMenu->appendChild(elemReturn, NULL);
		}

	}
	return elemMenu;
}

void CFormSubdialog::PushVXMLDOMItems(CComPtr<IXMLDOMDocument> pDoc, CComPtr<IXMLDOMElement> elemVXML)
{
	CGetFuncParamDataHelper::FuncParamCollector params;

#ifndef TEST_DATA_BASE
	if (1)
	{
		CGetFuncParamDataHelper collector;
		params = collector.GetFuncParamByBranch(this->GetId());
	}

#else
	params.emplace_back(CGetFuncParamDataHelper::CFuncParam(L"bnum", L"901123123", L"string"));
	params.emplace_back(CGetFuncParamDataHelper::CFuncParam(L"anum", L"12793812", L"string"));
#endif

	CComPtr <IXMLDOMElement> elemForm = CreateMainForm(pDoc, params);
	if (elemForm)
	{
		elemVXML->appendChild(elemForm, NULL);
	}

	if (GetSubDialogType() == static_cast<int>(E_SUBDIALOGTYPE::SDT_VXML))
	{
		CComPtr <IXMLDOMElement> elemDependentForm = CreateDependentForm(pDoc, params);
		if (elemDependentForm)
		{
			elemVXML->appendChild(elemDependentForm, NULL);
		}
	}

	

}

//void CFormSubdialog::PushVXMLDOMItems(CComPtr < IXMLDOMDocument > pDoc, CComPtr <IXMLDOMElement> elemVXML)
//{
//	CComPtr <IXMLDOMElement> itemMenu = CreateXMLItem(pDoc);
//	if (itemMenu)
//	{
//		elemVXML->appendChild(itemMenu, NULL);
//	}
//}
//
//std::wstring CFormSubdialog::GetLocalRoot()
//{
//	if (GetIsRoot())
//		return GetMenuName();
//
//	if (GetParentPtr())
//		return GetParentPtr()->GetLocalRoot();
//
//	return GetMenuName();
//}

/******************************* eof *************************************/