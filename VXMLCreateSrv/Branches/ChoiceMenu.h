/************************************************************************/
/* Name     : VXMLCreateSrv\Branches\ChoiceMenu.h                       */
/* Author   : Andrey Alekseev                                           */
/* Project  : VXMLCreateSrv                                             */
/* Company  : Forte-CT                                                  */
/* Date     : 07 Aug 2015                                               */
/************************************************************************/
#pragma once
#include "BaseBranch.h"

class CChoiceMenu : public CBaseBranch //<CChoiceMenu>
{
	//friend class CBaseBranch<CChoiceMenu>;

public:
	CChoiceMenu(const int& _iTreeId,
		const int& _iParentId,
		const std::wstring& _sMenuName,
		//const std::wstring& _sMenuText,
		const std::wstring& _sDtmf,
		const std::wstring& _sComment,
		//const std::wstring& _sWavName,
		const int& _iSubDialogId,
		//const bool& _bIsVisible,
		const int& _iRepeatCnt,
		const std::wstring& _sGotoRepeat,
		const std::wstring& _sGotoBack,
		const std::wstring& _sGoto,
		const std::wstring& _sGotoRoot,
		const std::wstring& _sTermChar,
		const int& _iTimeOut,
		const int& _bRoot,
		const int& _bIsCondition,
		const int& _bAddToHistory,
		const std::wstring & _sFile1);

public:
	int GetTypeMenu() { return MT_CHOICE; }

	//void SetMenuAttributes(CComPtr <IXMLDOMElement> elemMenu);
	void AddTermChar(CComPtr < IXMLDOMDocument > pDoc, CComPtr <IXMLDOMElement> elemMenu);
	void AddTimeout(CComPtr < IXMLDOMDocument > pDoc, CComPtr <IXMLDOMElement> elemMenu);
	//void AddPromptAudio(CComPtr < IXMLDOMDocument > pDoc, CComPtr <IXMLDOMElement> elemMenu);
	//void AddChoices(CComPtr < IXMLDOMDocument > pDoc, CComPtr <IXMLDOMElement> elemMenu);
	//void AddRepeat(CComPtr < IXMLDOMDocument > pDoc, CComPtr <IXMLDOMElement> elemMenu);
	//void AddRoot(CComPtr < IXMLDOMDocument > pDoc, CComPtr <IXMLDOMElement> elemMenu);
	//void AddBack(CComPtr < IXMLDOMDocument > pDoc, CComPtr <IXMLDOMElement> elemMenu);
	void AddNoinputNomatch(CComPtr < IXMLDOMDocument > pDoc, CComPtr <IXMLDOMElement> elemMenu);
	void AddFixedNoinput(CComPtr < IXMLDOMDocument > pDoc, CComPtr <IXMLDOMElement> elemMenu);

	/*void AddChild(BranchPtr child){ m_children.push_back(child); }*/
	CComPtr <IXMLDOMElement> CreateXMLItem(CComPtr < IXMLDOMDocument > pDoc);
	bool IsValid();

	//void PushVXMLDOMItems(CComPtr < IXMLDOMDocument > pDoc, CComPtr <IXMLDOMElement> elemVXML);
	//std::wstring GetLocalRoot();

//private:
//	BranchContainer m_children;
};


/******************************* eof *************************************/