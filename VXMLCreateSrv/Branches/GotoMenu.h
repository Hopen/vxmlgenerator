/************************************************************************/
/* Name     : VXMLCreateSrv\Branches\GotoMenu.h                         */
/* Author   : Andrey Alekseev                                           */
/* Project  : VXMLCreateSrv                                             */
/* Company  : Forte-CT                                                  */
/* Date     : 07 Aug 2015                                               */
/************************************************************************/
#pragma once
#include "BaseBranch.h"

class CGotoMenu : public CBaseBranch //<CGotoMenu>
{
	//friend class CBaseBranch < CGotoMenu > ;
public:
	CGotoMenu(const int& _iTreeId,
		const int& _iParentId,
		const std::wstring& _sMenuName,
		//const std::wstring& _sMenuText,
		const std::wstring& _sDtmf,
		const std::wstring& _sComment,
		//const std::wstring& _sWavName,
		const int& _iSubDialogId,
		//const bool& _bIsVisible,
		const std::wstring& _sGotoRepeat,
		const std::wstring& _sGotoBack,
		const std::wstring& _sGoto,
		const int& _bIsCondition,
		const int& _bAddToHistory,
		const std::wstring & _sFile1);

public:
	virtual int GetTypeMenu() { return MT_GOTO; }

	//void AddGoto(CComPtr < IXMLDOMDocument > pDoc, CComPtr <IXMLDOMElement> elemMenu, bool expression);

	//void AddChild(BranchPtr child){ m_children.push_back(child); }
	CComPtr <IXMLDOMElement> CreateXMLItem(CComPtr < IXMLDOMDocument > pDoc);
	bool IsValid();

	//void PushVXMLDOMItems(CComPtr < IXMLDOMDocument > pDoc, CComPtr <IXMLDOMElement> elemVXML);
	//std::wstring GetLocalRoot();
private:
	BranchContainer m_children;
};


/******************************* eof *************************************/