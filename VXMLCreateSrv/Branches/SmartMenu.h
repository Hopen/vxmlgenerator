/************************************************************************/
/* Name     : VXMLCreateSrv\Branches\SmartMenu.h                        */
/* Author   : Andrey Alekseev                                           */
/* Project  : VXMLCreateSrv                                             */
/* Company  : Forte-CT                                                  */
/* Date     : 29 Set 2015                                               */
/************************************************************************/
#pragma once
#include "BaseBranch.h"

class CSmartMenu : public CBaseBranch //<CSmartMenu>
{
//	friend class CBaseBranch < CSmartMenu > ;
public:
	CSmartMenu(const int& _iTreeId,
		const int& _iParentId,
		const std::wstring& _sMenuName,
		const std::wstring& _sDtmf,
		const std::wstring& _sComment,
		const std::wstring& _sGotoRepeat,
		const std::wstring& _sGotoBack,
		const std::wstring& _sGoto,
		const std::wstring& _sGotoRoot,
		const int& _bRoot,
		const int& _bAddoHistory,
		const std::wstring & _sFile1,
		const std::wstring & _sFile2, 
		const std::wstring & _sFile3, 
		const std::wstring & _sFile4);

public:

	virtual int GetTypeMenu() { return MT_SMART; }

	//void AddChild(BranchPtr child){ m_children.push_back(child); }
	CComPtr <IXMLDOMElement> CreateXMLItem(CComPtr < IXMLDOMDocument > pDoc);
	virtual void PushVXMLDOMItems(CComPtr < IXMLDOMDocument > pDoc, CComPtr <IXMLDOMElement> elemVXML);

	bool IsValid();
	virtual std::wstring GetLocalRoot()const;

	//std::wstring GetFile1()const { return m_file1; }
	std::wstring GetFile2()const { return m_file2; }
	std::wstring GetFile3()const { return m_file3; }
	std::wstring GetFile4()const { return m_file4; }

private:
	CComPtr <IXMLDOMElement> CreateCG(CComPtr < IXMLDOMDocument > pDoc);
	CComPtr <IXMLDOMElement> CreateCG1(CComPtr < IXMLDOMDocument > pDoc);
	CComPtr <IXMLDOMElement> CreateCG2(CComPtr < IXMLDOMDocument > pDoc);
	CComPtr <IXMLDOMElement> CreateCG3(CComPtr < IXMLDOMDocument > pDoc);

private:
	//std::wstring m_file1;
	std::wstring m_file2;
	std::wstring m_file3;
	std::wstring m_file4;

	//BranchContainer m_children;

};


/******************************* eof *************************************/