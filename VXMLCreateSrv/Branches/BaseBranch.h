/************************************************************************/
/* Name     : VXMLCreateSrv\Branches\BaseBranch.h                       */
/* Author   : Andrey Alekseev                                           */
/* Project  : VXMLCreateSrv                                             */
/* Company  : Forte-CT                                                  */
/* Date     : 05 Aug 2015                                               */
/************************************************************************/
#pragma once


#include <string>
#include <list>
#include <boost/shared_ptr.hpp>
#include <boost/weak_ptr.hpp>

enum eMenuType
{
	MT_CHOICE = 0,
	MT_GOTO,
	MT_FORM_SUBDIALOG,
	MT_NOINPUT,
	MT_NOMATCH,
	MT_SMART
};

//struct TBranch
//{
//	int          m_iId;
//	int          m_iTreeId;
//	int          m_iParentId;
//	std::wstring m_sMenuName;
//	int          m_iMenuType;
//	std::wstring m_sMenuText;
//	std::wstring m_sDtmf;
//	std::wstring m_sCodeName;
//	std::wstring m_sCode;
//	std::wstring m_sComment;
//	std::wstring m_sWavName;
//	std::wstring m_sWavText;
//	int          m_iSubDialogId;
//	bool         m_bIsVisible;
//	int          m_iRepeatCnt;
//	std::wstring m_sGotoRepeat;
//	std::wstring m_sGotoBack;
//	std::wstring m_sGoto;
//	int          m_iCityId;
//	std::wstring m_sWavBreak;
//	std::wstring m_sDtmfWait;
//	std::wstring m_sGotoRoot;
//	std::wstring m_sTermChar;
//	int          m_iTimeOut;
//	bool         m_bIsRoot;
//	bool         m_bIsCondition;
//	std::wstring m_sFile1;
//	std::wstring m_sFile2;
//	std::wstring m_sFile3;
//	std::wstring m_sFile4;
//	std::wstring m_sText1;
//	std::wstring m_sText2;
//	std::wstring m_sText3;
//	std::wstring m_sText4;
//
//	TBranch(const int& _iId,
//		const int& _iTreeId,
//		const int& _iParentId,
//		const std::wstring& _sMenuName,
//		const int& _iMenuType,
//		const std::wstring& _sMenuText,
//		const std::wstring& _sDtmf,
//		const std::wstring& _sCodeName,
//		const std::wstring& _sCode,
//		const std::wstring& _sComment,
//		const std::wstring& _sWavName,
//		const std::wstring& _sWavText,
//		const int& _iSubDialogId,
//		const bool& _bIsVisible,
//		const int& _iRepeatCnt,
//		const std::wstring& _sGotoRepeat,
//		const std::wstring& _sGotoBack,
//		const std::wstring& _sGoto,
//		const int& _iCityId,
//		const std::wstring& _sWavBreak,
//		const std::wstring& _sDtmfWait,
//		const std::wstring& _sGotoRoot,
//		const std::wstring& _sTermChar,
//		const int& _iTimeOut,
//		const int& _bRoot,
//		const int& _bIsCondition, 
//		const std::wstring& _sFile1,
//		const std::wstring& _sFile2, 
//		const std::wstring& _sFile3, 
//		const std::wstring& _sFile4, 
//		const std::wstring& _sText1, 
//		const std::wstring& _sText2, 
//		const std::wstring& _sText3, 
//		const std::wstring& _sText4 ):
//	m_iId(_iId), m_iTreeId(_iTreeId), m_iParentId(_iParentId), m_sMenuName(_sMenuName), 
//		m_iMenuType(_iMenuType), m_sMenuText(_sMenuText), m_sDtmf(_sDtmf), m_sCodeName(_sCodeName),
//		m_sCode(_sCode), m_sComment(_sComment), m_sWavName(_sWavName), m_sWavText(_sWavText),
//		m_iSubDialogId(_iSubDialogId), m_bIsVisible(_bIsVisible), m_iRepeatCnt(_iRepeatCnt), m_sGotoRepeat(_sGotoRepeat),
//		m_sGotoBack(_sGotoBack), m_sGoto(_sGoto), m_iCityId(_iCityId), m_sWavBreak(_sWavBreak),
//		m_sDtmfWait(_sDtmfWait), m_sGotoRoot(_sGotoRoot), m_sTermChar(_sTermChar), m_iTimeOut(_iTimeOut),
//		m_bIsRoot(_bRoot), m_bIsCondition(_bIsCondition), 
//		m_sFile1(_sFile1), m_sFile2(_sFile2), m_sFile3(_sFile3), m_sFile4(_sFile4),
//		m_sText1(_sText1), m_sText2(_sText2), m_sText3(_sText3), m_sText4(_sText4)
//	{}
//
//	typedef std::shared_ptr <TBranch> BranchPtr;
//	typedef std::list <BranchPtr> BranchContainer;
//
//	BranchPtr       m_ParentPtr;
//	BranchContainer m_children;
//
//	void AddChild(BranchPtr child){ m_children.push_back(child); }
//};
//

//template <class Derived>
class CBaseBranch
{
public:
	typedef std::shared_ptr <CBaseBranch > BranchPtr;
	typedef std::list <BranchPtr> BranchContainer;

	typedef std::weak_ptr <CBaseBranch > SafeBranchPtr;
	typedef std::list <SafeBranchPtr> SafeBranchContainer;

public:
	CBaseBranch(const int& _iId,
		const int& _iParentId,
		const std::wstring& _sMenuName,
		//const std::wstring& _sMenuText,
		const std::wstring& _sDtmf,
		const std::wstring& _sComment,
		//const std::wstring& _sWavName,
		const int& _iSubDialogId,
		//const bool& _bIsVisible,
		const int& _iRepeatCnt,
		const std::wstring& _sGotoRepeat,
		const std::wstring& _sGotoBack,
		const std::wstring& _sGoto,
		const std::wstring& _sGotoRoot,
		const std::wstring& _sTermChar,
		const int& _iTimeOut,
		const int& _bRoot,
		const int& _bIsCondition,
		const int& _bAddTohistory,
		const std::wstring & _sFile1)
		: m_iId(_iId), m_iParentId(_iParentId), m_sMenuName(_sMenuName), //m_sMenuText(_sMenuText), 
		m_sDtmf(_sDtmf), m_sComment(_sComment), /*m_sWavName(_sWavName),*/ m_iSubDialogId(_iSubDialogId),
		/*m_bIsVisible(_bIsVisible),*/ m_iRepeatCnt(_iRepeatCnt), m_sGotoRepeat(_sGotoRepeat), m_sGotoBack(_sGotoBack), m_sGoto(_sGoto),
		m_sGotoRoot(_sGotoRoot), m_sTermChar(_sTermChar), m_iTimeOut(_iTimeOut), m_bIsRoot(_bRoot ? true : false),
		m_bIsCondition(_bIsCondition ? true : false), m_file1(_sFile1), m_bAddToHistory(_bAddTohistory == 1? true: false)
	{

	}

	virtual ~CBaseBranch() {}
	virtual int GetTypeMenu() = 0;
	//virtual void AddChild(BranchPtr child) = 0;
	virtual CComPtr <IXMLDOMElement> CreateXMLItem(CComPtr < IXMLDOMDocument > pDoc) = 0;
	virtual void PushVXMLDOMItems(CComPtr < IXMLDOMDocument > pDoc, CComPtr <IXMLDOMElement> elemVXML);
	virtual bool IsValid() = 0;
	virtual std::wstring GetLocalRoot()const;

	void SetMenuAttributes(const std::wstring & _menuName, CComPtr <IXMLDOMElement> elemMenu);
	void AddAudio(CComPtr < IXMLDOMDocument > pDoc, CComPtr <IXMLDOMElement> elemMenu);
	void AddPromptAudio(const std::wstring & _audioName, CComPtr < IXMLDOMDocument > pDoc, CComPtr <IXMLDOMElement> elemMenu);
	void AddGoto(CComPtr < IXMLDOMDocument > pDoc, CComPtr <IXMLDOMElement> elemMenu, bool expression);
	void AddChoices(CComPtr < IXMLDOMDocument > pDoc, CComPtr <IXMLDOMElement> elemMenu);
	void AddRepeat(const std::wstring & _menuName, CComPtr < IXMLDOMDocument > pDoc, CComPtr <IXMLDOMElement> elemMenu);
	void AddRoot(/*const std::wstring & _menuName, */CComPtr < IXMLDOMDocument > pDoc, CComPtr <IXMLDOMElement> elemMenu);
	void AddBack(CComPtr < IXMLDOMDocument > pDoc, CComPtr <IXMLDOMElement> elemMenu);
	void AddNoinput(const int& count, const std::wstring& next, CComPtr < IXMLDOMDocument > pDoc, CComPtr <IXMLDOMElement> elemMenu);
	void AddNomatch(const int& count, const std::wstring& next, CComPtr < IXMLDOMDocument > pDoc, CComPtr <IXMLDOMElement> elemMenu);
	bool AddConditions(CComPtr < IXMLDOMDocument > pDoc, CComPtr <IXMLDOMElement> elemMenu);
	void AddAddToHistory(CComPtr < IXMLDOMDocument > pDoc, CComPtr <IXMLDOMElement> elemMenu);


	void AddChild(BranchPtr child){ m_children.push_back(child); }

	//int GetTypeMenu()
	//{
	//	return self()->GetTypeMenu();
	//}

	//void AddChild(BranchPtr child)
	//{
	//	self()->AddChild(child);
	//}

	////CComPtr <IXMLDOMElement> CreateXMLItem(CComPtr < IXMLDOMDocument > pDoc)
	////{
	////	return self()->CreateXMLItem(pDoc);
	////}

	//void PushVXMLDOMItems(CComPtr < IXMLDOMDocument > pDoc, CComPtr <IXMLDOMElement> elemVXML)
	//{
	//	self()->PushVXMLDOMItems(pDoc, elemVXML);
	//}

	//bool IsValid()
	//{
	//	return self()->IsValid();
	//}

	//std::wstring GetLocalRoot()
	//{
	//	return self()->GetLocalRoot();
	//}

	int GetId()const { return m_iId; }
	int GetParentId()const { return m_iParentId; }
	std::wstring GetMenuName()const { return m_sMenuName; }
	//std::wstring GetMenuText()const { return m_sMenuText; }
	std::wstring GetDtmf()const { return m_sDtmf; }
	std::wstring GetComment()const { return m_sComment; }
	//std::wstring GetWavName()const { return m_sWavName; }
	int GetSubDialogId()const { return m_iSubDialogId; }
	//bool IsVisible()const { return m_bIsVisible; }
	int GetRepeatCnt()const { return m_iRepeatCnt; }
	std::wstring GetGotoRepeat()const { return m_sGotoRepeat; }
	std::wstring GetGotoBack()const { return m_sGotoBack; }
	std::wstring GetGoto()const { return m_sGoto; }
	std::wstring GetGotoRoot()const { return m_sGotoRoot; }
	std::wstring GetTermChar()const { return m_sTermChar; }
	int GetTimeOut()const { return m_iTimeOut; }
	
	bool GetIsRoot()const { return m_bIsRoot; }
	bool GetIsCondition()const { return m_bIsCondition; }
	bool GetAddToHistory()const { return m_bAddToHistory; }
	std::wstring GetFile1()const { return m_file1; }

	SafeBranchPtr GetParentPtr()const{ return m_ParentPtr; }
	void SetParentPtr(BranchPtr ParentPtr){ m_ParentPtr = ParentPtr; }
	void SetParentPtr(SafeBranchPtr ParentPtr){ m_ParentPtr = ParentPtr; }

	//Derived * self()
	//{
	//	return static_cast<Derived*>(this);
	//}

private:
	int          m_iId;
	int          m_iParentId;
	std::wstring m_sMenuName;
	//int          m_iMenuType;
	//std::wstring m_sMenuText;
	std::wstring m_sDtmf;
	//std::wstring m_sCodeName;
	//std::wstring m_sCode;
	std::wstring m_sComment;
	//std::wstring m_sWavName;
	//std::wstring m_sWavText;
	int          m_iSubDialogId;
	//bool         m_bIsVisible;
	int          m_iRepeatCnt;
	std::wstring m_sGotoRepeat;
	std::wstring m_sGotoBack;
	std::wstring m_sGoto;
	//int          m_iCityId;
	//std::wstring m_sWavBreak;
	//std::wstring m_sDtmfWait;
	std::wstring m_sGotoRoot;
	std::wstring m_sTermChar;
	int          m_iTimeOut;
	bool         m_bIsRoot;
	bool         m_bIsCondition;
	bool         m_bAddToHistory;
	std::wstring m_file1;



	SafeBranchPtr    m_ParentPtr;

protected:
	SafeBranchContainer m_children;
};

template <class T>
CComPtr <IXMLDOMElement> CreateXMLItem(CComPtr < IXMLDOMDocument > pDoc, std::shared_ptr < T > t)
{
	if (!pDoc)
		NULL;

	CComPtr <IXMLDOMElement> elemMenu = NULL;
	HRESULT hr = pDoc->createElement(_T("menu"), &elemMenu);

	if (SUCCEEDED(hr) && elemMenu)
	{
		t->SetMenuAttributes(GetMenuName(), elemMenu);
		t->AddTermChar(pDoc, elemMenu);
		t->AddTimeout(pDoc, elemMenu);
		t->AddPromptAudio(GetFile1(), pDoc, elemMenu);
		t->AddChoices(pDoc, elemMenu);
		t->AddRepeat(GetMenuName(), pDoc, elemMenu);
		t->AddRoot(pDoc, elemMenu);
		t->AddBack(pDoc, elemMenu);
		t->AddNoinputNomatch(pDoc, elemMenu);
		t->AddFixedNoinput(pDoc, elemMenu);
	}
	return elemMenu;

}

/******************************* eof *************************************/