/************************************************************************/
/* Name     : VXMLCreateSrv\Branches\ChoiceMenu.cpp                     */
/* Author   : Andrey Alekseev                                           */
/* Project  : VXMLCreateSrv                                             */
/* Company  : Forte-CT                                                  */
/* Date     : 07 Aug 2015                                               */
/************************************************************************/
#include "stdafx.h"
#include "ChoiceMenu.h"

CChoiceMenu::CChoiceMenu(const int& _iTreeId,
	const int& _iParentId,
	const std::wstring& _sMenuName,
	//const std::wstring& _sMenuText,
	const std::wstring& _sDtmf,
	const std::wstring& _sComment,
	//const std::wstring& _sWavName,
	const int& _iSubDialogId,
	//const bool& _bIsVisible,
	const int& _iRepeatCnt,
	const std::wstring& _sGotoRepeat,
	const std::wstring& _sGotoBack,
	const std::wstring& _sGoto,
	const std::wstring& _sGotoRoot,
	const std::wstring& _sTermChar,
	const int& _iTimeOut,
	const int& _bRoot,
	const int& _bIsCondition,
	const int& _bAddToHistory,
	const std::wstring & _sFile1)
	: CBaseBranch(_iTreeId, _iParentId, _sMenuName, //_sMenuText,
	_sDtmf, _sComment, /*_sWavName,*/ _iSubDialogId,
	/*_bIsVisible,*/_iRepeatCnt, _sGotoRepeat, _sGotoBack, _sGoto, _sGotoRoot, 
	_sTermChar, _iTimeOut, _bRoot, _bIsCondition, _bAddToHistory, _sFile1)
{
}

bool CChoiceMenu::IsValid()
{
	// the menu of this type mast have: 
	//   1. id
	//   2. wav

	if (!GetMenuName().length())
		return false;
	if (!GetFile1().length())
		return false;
	return true;
}

//void CChoiceMenu::SetMenuAttributes(CComPtr <IXMLDOMElement> elemMenu)
//{
//	if (GetComment().length())
//		elemMenu->setAttribute(L"comment", _variant_t(GetComment().c_str()));
//	elemMenu->setAttribute(L"id", _variant_t(GetMenuName().c_str()));
//}

void CChoiceMenu::AddTermChar(CComPtr < IXMLDOMDocument > pDoc, CComPtr <IXMLDOMElement> elemMenu)
{
	//add termchar
	std::wstring sTermChar = GetTermChar();
	if (sTermChar != L"empty")
	{
		CComPtr <IXMLDOMElement> elemProperty = NULL;
		HRESULT hr = pDoc->createElement(_T("property"), &elemProperty);
		if (SUCCEEDED(hr) && elemProperty)
		{
			elemProperty->setAttribute(L"value", _variant_t(sTermChar.c_str()));
			elemProperty->setAttribute(L"name", _variant_t(L"termchar"));

			elemMenu->appendChild(elemProperty, NULL);
		}
	}

}

void CChoiceMenu::AddTimeout(CComPtr < IXMLDOMDocument > pDoc, CComPtr <IXMLDOMElement> elemMenu)
{
	if (0 != GetTimeOut())
	{
		CComPtr <IXMLDOMElement> elemProperty = NULL;
		HRESULT hr = pDoc->createElement(_T("property"), &elemProperty);
		if (SUCCEEDED(hr) && elemProperty)
		{
			elemProperty->setAttribute(L"value", _variant_t(GetTimeOut()));
			elemProperty->setAttribute(L"name", _variant_t(L"timeout"));

			elemMenu->appendChild(elemProperty, NULL);
		}
	}
}

//void CChoiceMenu::AddPromptAudio(CComPtr < IXMLDOMDocument > pDoc, CComPtr <IXMLDOMElement> elemMenu)
//{
//	//add prompt+audio
//	CComPtr <IXMLDOMElement> elemPrompt = NULL;
//	HRESULT hr = pDoc->createElement(_T("prompt"), &elemPrompt);
//	if (SUCCEEDED(hr) && elemPrompt)
//	{
//		CComPtr <IXMLDOMElement> elemAudio = NULL;
//		hr = pDoc->createElement(_T("audio"), &elemAudio);
//		if (SUCCEEDED(hr) && elemAudio)
//		{
//			elemAudio->setAttribute(L"src", _variant_t(GetWavName().c_str()));
//			elemPrompt->appendChild(elemAudio, NULL);
//		}
//
//		elemMenu->appendChild(elemPrompt, NULL);
//	}
//
//}

void CChoiceMenu::AddNoinputNomatch(CComPtr < IXMLDOMDocument > pDoc, CComPtr <IXMLDOMElement> elemMenu)
{
	for each (SafeBranchPtr safebranch in m_children)
	{
		BranchPtr branch = safebranch.lock();
		switch (branch->GetTypeMenu())
		{

		case MT_NOINPUT:
		{
			if (branch->GetRepeatCnt() == GetRepeatCnt())
				break;

			//CComPtr <IXMLDOMElement> elemNoinput = NULL;
			//HRESULT hr = pDoc->createElement(_T("noinput"), &elemNoinput);
			//if (SUCCEEDED(hr) && elemNoinput)
			//{
			//	elemNoinput->setAttribute(L"count", _variant_t(branch->GetRepeatCnt()));

			//	CComPtr <IXMLDOMElement> elemGoto = NULL;
			//	hr = pDoc->createElement(_T("goto"), &elemGoto);
			//	if (SUCCEEDED(hr) && elemGoto)
			//	{
			//		elemGoto->setAttribute(L"next", _variant_t(std::wstring(L"#" + branch->GetMenuName()).c_str()));
			//		elemNoinput->appendChild(elemGoto, NULL);
			//	}

			//	elemMenu->appendChild(elemNoinput, NULL);
			//}
			AddNoinput(branch->GetRepeatCnt(), std::wstring(L"#" + branch->GetMenuName()), pDoc, elemMenu);
			break;
		}
		case MT_NOMATCH:
		{
			//CComPtr <IXMLDOMElement> elemNomatch = NULL;
			//HRESULT hr = pDoc->createElement(_T("nomatch"), &elemNomatch);
			//if (SUCCEEDED(hr) && elemNomatch)
			//{
			//	elemNomatch->setAttribute(L"count", _variant_t(branch->GetRepeatCnt()));

			//	CComPtr <IXMLDOMElement> elemGoto = NULL;
			//	hr = pDoc->createElement(_T("goto"), &elemGoto);
			//	if (SUCCEEDED(hr) && elemGoto)
			//	{
			//		elemGoto->setAttribute(L"next", _variant_t(std::wstring(L"#" + branch->GetMenuName()).c_str()));
			//		elemNomatch->appendChild(elemGoto, NULL);
			//	}

			//	elemMenu->appendChild(elemNomatch, NULL);
			//}
			AddNomatch(branch->GetRepeatCnt(), std::wstring(L"#" + branch->GetMenuName()), pDoc, elemMenu);
			break;
		}
		}

	}
}

//void CChoiceMenu::AddChoices(CComPtr < IXMLDOMDocument > pDoc, CComPtr <IXMLDOMElement> elemMenu)
//{
//	for each (SafeBranchPtr safebranch in m_children)
//	{
//		BranchPtr branch = safebranch.lock();
//		switch (branch->GetTypeMenu())
//		{
//		default:
//		{
//			if (branch->GetDtmf().length())
//			{
//				CComPtr <IXMLDOMElement> elemChoice = NULL;
//				HRESULT hr = pDoc->createElement(_T("choice"), &elemChoice);
//				if (SUCCEEDED(hr) && elemChoice)
//				{
//					elemChoice->setAttribute(L"next", _variant_t(std::wstring(L"#" + branch->GetMenuName()).c_str()));
//					elemChoice->setAttribute(L"dtmf", _variant_t(branch->GetDtmf().c_str()));
//					elemMenu->appendChild(elemChoice, NULL);
//				}
//			}
//			break;
//		}
//		}
//
//	}
//}

//void CChoiceMenu::AddRepeat(CComPtr < IXMLDOMDocument > pDoc, CComPtr <IXMLDOMElement> elemMenu)
//{
//	if (GetGotoRepeat().length())
//	{
//		CComPtr <IXMLDOMElement> elemChoice = NULL;
//		HRESULT hr = pDoc->createElement(_T("choice"), &elemChoice);
//		if (SUCCEEDED(hr) && elemChoice)
//		{
//			elemChoice->setAttribute(L"next", _variant_t(std::wstring(L"#" + GetMenuName()).c_str()));
//			elemChoice->setAttribute(L"dtmf", _variant_t(GetGotoRepeat().c_str()));
//			//CComPtr <IXMLDOMElement> elemReprompt = NULL;
//			//hr = pDoc->createElement(_T("reprompt"), &elemReprompt);
//			//if (SUCCEEDED(hr) && elemReprompt)
//			//{
//			//	elemChoice->appendChild(elemReprompt, NULL);
//			//}
//
//			elemMenu->appendChild(elemChoice, NULL);
//
//		}
//	}
//}

//void CChoiceMenu::AddRoot(CComPtr < IXMLDOMDocument > pDoc, CComPtr <IXMLDOMElement> elemMenu)
//{
//	if (GetGotoRoot().length())
//	{
//		CComPtr <IXMLDOMElement> elemChoice = NULL;
//		HRESULT hr = pDoc->createElement(_T("choice"), &elemChoice);
//		if (SUCCEEDED(hr) && elemChoice)
//		{
//			std::wstring rootMenuName = GetLocalRoot();
//			//BranchPtr rootPtr = GetParentPtr();
//			//while (rootPtr)
//			//{
//			//	rootMenuName = rootPtr->GetMenuName();
//			//	rootPtr = rootPtr->GetParentPtr();
//			//}
//
//			elemChoice->setAttribute(L"next", _variant_t(std::wstring(L"#" + rootMenuName).c_str()));
//			elemChoice->setAttribute(L"dtmf", _variant_t(GetGotoRoot().c_str()));
//
//			elemMenu->appendChild(elemChoice, NULL);
//		}
//	}
//}

//void CChoiceMenu::AddBack(CComPtr < IXMLDOMDocument > pDoc, CComPtr <IXMLDOMElement> elemMenu)
//{
//	if (GetGotoBack().length())
//	{
//		CComPtr <IXMLDOMElement> elemChoice = NULL;
//		HRESULT hr = pDoc->createElement(_T("choice"), &elemChoice);
//		if (SUCCEEDED(hr) && elemChoice && GetParentPtr().lock())
//		{
//			std::wstring backMenuName = GetParentPtr().lock()->GetMenuName();
//
//			elemChoice->setAttribute(L"next", _variant_t(std::wstring(L"#" + backMenuName).c_str()));
//			elemChoice->setAttribute(L"dtmf", _variant_t(GetGotoBack().c_str()));
//
//			elemMenu->appendChild(elemChoice, NULL);
//		}
//	}
//
//}

void CChoiceMenu::AddFixedNoinput(CComPtr < IXMLDOMDocument > pDoc, CComPtr <IXMLDOMElement> elemMenu)
{
	if (GetRepeatCnt() > 0)
	{
		CComPtr <IXMLDOMElement> elemNoinput = NULL;
		HRESULT hr = pDoc->createElement(_T("noinput"), &elemNoinput);
		if (SUCCEEDED(hr) && elemNoinput)
		{
			elemNoinput->setAttribute(L"count", _variant_t(GetRepeatCnt()));

			CComPtr <IXMLDOMElement> elemGoto = NULL;
			hr = pDoc->createElement(_T("goto"), &elemGoto);
			if (SUCCEEDED(hr) && elemGoto)
			{
				elemGoto->setAttribute(L"next", _variant_t(L"#exit"));
				elemNoinput->appendChild(elemGoto, NULL);
			}

			elemMenu->appendChild(elemNoinput, NULL);
		}
	}
}

CComPtr <IXMLDOMElement> CChoiceMenu::CreateXMLItem(CComPtr < IXMLDOMDocument > pDoc)
{
	if (!pDoc)
		NULL;

	CComPtr <IXMLDOMElement> elemMenu = NULL;
	HRESULT hr = pDoc->createElement(_T("menu"), &elemMenu);

	if (SUCCEEDED(hr) && elemMenu)
	{
		////set attributs
		//if (GetComment().length())
		//	elemMenu->setAttribute(L"comment", _variant_t(GetComment().c_str()));
		//elemMenu->setAttribute(L"id", _variant_t(GetMenuName().c_str()));
		SetMenuAttributes(GetMenuName(), elemMenu);

		////add termchar
		//std::wstring sTermChar = GetTermChar();
		//if (sTermChar != L"empty")
		//{
		//	CComPtr <IXMLDOMElement> elemProperty = NULL;
		//	hr = pDoc->createElement(_T("property"), &elemProperty);
		//	if (SUCCEEDED(hr) && elemProperty)
		//	{
		//		elemProperty->setAttribute(L"value", _variant_t(sTermChar.c_str()));
		//		elemProperty->setAttribute(L"name", _variant_t(L"termchar"));

		//		elemMenu->appendChild(elemProperty, NULL);
		//	}
		//}
		AddTermChar(pDoc, elemMenu);

		////add timeout
		//if (0 != GetTimeOut())
		//{
		//	CComPtr <IXMLDOMElement> elemProperty = NULL;
		//	hr = pDoc->createElement(_T("property"), &elemProperty);
		//	if (SUCCEEDED(hr) && elemProperty)
		//	{
		//		elemProperty->setAttribute(L"value", _variant_t(GetTimeOut()));
		//		elemProperty->setAttribute(L"name", _variant_t(L"timeout"));

		//		elemMenu->appendChild(elemProperty, NULL);
		//	}
		//}
		AddTimeout(pDoc, elemMenu);


		if (GetAddToHistory())
		{
			AddAddToHistory(pDoc, elemMenu);
		}

		////add prompt+audio
		//CComPtr <IXMLDOMElement> elemPrompt = NULL;
		//hr = pDoc->createElement(_T("prompt"), &elemPrompt);
		//if (SUCCEEDED(hr) && elemPrompt)
		//{
		//	CComPtr <IXMLDOMElement> elemAudio = NULL;
		//	hr = pDoc->createElement(_T("audio"), &elemAudio);
		//	if (SUCCEEDED(hr) && elemAudio)
		//	{
		//		elemAudio->setAttribute(L"src", _variant_t(GetWavName().c_str()));
		//		elemPrompt->appendChild(elemAudio, NULL);
		//	}

		//	elemMenu->appendChild(elemPrompt, NULL);
		//}
		AddPromptAudio(GetFile1(), pDoc, elemMenu);

		////add choices
		//for each (SafeBranchPtr safebranch in m_children)
		//{
		//	BranchPtr branch = safebranch.lock();
		//	switch (branch->GetTypeMenu())
		//	{

		//	case MT_NOINPUT:
		//	{
		//		if (branch->GetRepeatCnt() == GetRepeatCnt())
		//			break;

		//		CComPtr <IXMLDOMElement> elemNoinput = NULL;
		//		hr = pDoc->createElement(_T("noinput"), &elemNoinput);
		//		if (SUCCEEDED(hr) && elemNoinput)
		//		{
		//			elemNoinput->setAttribute(L"count", _variant_t(branch->GetRepeatCnt()));

		//			CComPtr <IXMLDOMElement> elemGoto = NULL;
		//			hr = pDoc->createElement(_T("goto"), &elemGoto);
		//			if (SUCCEEDED(hr) && elemGoto)
		//			{
		//				elemGoto->setAttribute(L"next", _variant_t(std::wstring(L"#" + branch->GetMenuName()).c_str()));
		//				elemNoinput->appendChild(elemGoto, NULL);
		//			}

		//			elemMenu->appendChild(elemNoinput, NULL);
		//		}
		//		break;
		//	}
		//	case MT_NOMATCH:
		//	{
		//		CComPtr <IXMLDOMElement> elemNomatch = NULL;
		//		hr = pDoc->createElement(_T("nomatch"), &elemNomatch);
		//		if (SUCCEEDED(hr) && elemNomatch)
		//		{
		//			elemNomatch->setAttribute(L"count", _variant_t(branch->GetRepeatCnt()));

		//			CComPtr <IXMLDOMElement> elemGoto = NULL;
		//			hr = pDoc->createElement(_T("goto"), &elemGoto);
		//			if (SUCCEEDED(hr) && elemGoto)
		//			{
		//				elemGoto->setAttribute(L"next", _variant_t(std::wstring(L"#" + branch->GetMenuName()).c_str()));
		//				elemNomatch->appendChild(elemGoto, NULL);
		//			}

		//			elemMenu->appendChild(elemNomatch, NULL);
		//		}
		//		break;
		//	}
		//	default:
		//	{
		//		if (branch->GetDtmf().length())
		//		{
		//			CComPtr <IXMLDOMElement> elemChoice = NULL;
		//			hr = pDoc->createElement(_T("choice"), &elemChoice);
		//			if (SUCCEEDED(hr) && elemChoice)
		//			{
		//				elemChoice->setAttribute(L"next", _variant_t(std::wstring(L"#" + branch->GetMenuName()).c_str()));
		//				elemChoice->setAttribute(L"dtmf", _variant_t(branch->GetDtmf().c_str()));
		//				elemMenu->appendChild(elemChoice, NULL);
		//			}
		//		}
		//		break;
		//	}
		//	}

		//}

		AddChoices(pDoc, elemMenu);
		////add repeat 
		//if (GetGotoRepeat().length())
		//{
		//	CComPtr <IXMLDOMElement> elemChoice = NULL;
		//	hr = pDoc->createElement(_T("choice"), &elemChoice);
		//	if (SUCCEEDED(hr) && elemChoice)
		//	{
		//		elemChoice->setAttribute(L"next", _variant_t(std::wstring(L"#" + GetMenuName()).c_str()));
		//		elemChoice->setAttribute(L"dtmf", _variant_t(GetGotoRepeat().c_str()));
		//		//CComPtr <IXMLDOMElement> elemReprompt = NULL;
		//		//hr = pDoc->createElement(_T("reprompt"), &elemReprompt);
		//		//if (SUCCEEDED(hr) && elemReprompt)
		//		//{
		//		//	elemChoice->appendChild(elemReprompt, NULL);
		//		//}

		//		elemMenu->appendChild(elemChoice, NULL);

		//	}
		//}
		AddRepeat(GetMenuName(), pDoc, elemMenu);
		////add root
		//if (GetGotoRoot().length())
		//{
		//	CComPtr <IXMLDOMElement> elemChoice = NULL;
		//	hr = pDoc->createElement(_T("choice"), &elemChoice);
		//	if (SUCCEEDED(hr) && elemChoice)
		//	{
		//		std::wstring rootMenuName = GetLocalRoot();
		//		//BranchPtr rootPtr = GetParentPtr();
		//		//while (rootPtr)
		//		//{
		//		//	rootMenuName = rootPtr->GetMenuName();
		//		//	rootPtr = rootPtr->GetParentPtr();
		//		//}

		//		elemChoice->setAttribute(L"next", _variant_t(std::wstring(L"#" + rootMenuName).c_str()));
		//		elemChoice->setAttribute(L"dtmf", _variant_t(GetGotoRoot().c_str()));

		//		elemMenu->appendChild(elemChoice, NULL);
		//	}
		//}
		AddRoot(/*GetLocalRoot(),*/ pDoc, elemMenu);

		////add back
		//if (GetGotoBack().length())
		//{
		//	CComPtr <IXMLDOMElement> elemChoice = NULL;
		//	hr = pDoc->createElement(_T("choice"), &elemChoice);
		//	if (SUCCEEDED(hr) && elemChoice && GetParentPtr().lock())
		//	{
		//		std::wstring backMenuName = GetParentPtr().lock()->GetMenuName();

		//		elemChoice->setAttribute(L"next", _variant_t(std::wstring(L"#" + backMenuName).c_str()));
		//		elemChoice->setAttribute(L"dtmf", _variant_t(GetGotoBack().c_str()));

		//		elemMenu->appendChild(elemChoice, NULL);
		//	}
		//}
		AddBack(pDoc, elemMenu);

		////add fixed noinput
		//if (GetRepeatCnt() > 0)
		//{
		//	CComPtr <IXMLDOMElement> elemNoinput = NULL;
		//	hr = pDoc->createElement(_T("noinput"), &elemNoinput);
		//	if (SUCCEEDED(hr) && elemNoinput)
		//	{
		//		elemNoinput->setAttribute(L"count", _variant_t(GetRepeatCnt()));
		//		
		//		CComPtr <IXMLDOMElement> elemGoto = NULL;
		//		hr = pDoc->createElement(_T("goto"), &elemGoto);
		//		if (SUCCEEDED(hr) && elemGoto)
		//		{
		//			elemGoto->setAttribute(L"next", _variant_t(L"#exit"));
		//			elemNoinput->appendChild(elemGoto, NULL);
		//		}

		//		elemMenu->appendChild(elemNoinput, NULL);
		//	}
		//}

		AddNoinputNomatch(pDoc, elemMenu);
		AddFixedNoinput(pDoc, elemMenu);
	}
	return elemMenu;
}

//void CChoiceMenu::PushVXMLDOMItems(CComPtr < IXMLDOMDocument > pDoc, CComPtr <IXMLDOMElement> elemVXML)
//{
//	CComPtr <IXMLDOMElement> itemMenu = CreateXMLItem(pDoc);
//	if (itemMenu)
//	{
//		elemVXML->appendChild(itemMenu, NULL);
//	}
//}
//
//std::wstring CChoiceMenu::GetLocalRoot()
//{
//	if (GetIsRoot())
//		return GetMenuName();
//
//	if (GetParentPtr())
//		return GetParentPtr()->GetLocalRoot();
//
//	return GetMenuName();
//}

/******************************* eof *************************************/