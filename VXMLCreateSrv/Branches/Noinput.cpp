/************************************************************************/
/* Name     : VXMLCreateSrv\Branches\Noinput.cpp                        */
/* Author   : Andrey Alekseev                                           */
/* Project  : VXMLCreateSrv                                             */
/* Company  : Forte-CT                                                  */
/* Date     : 15 Set 2015                                               */
/************************************************************************/
#include "stdafx.h"
#include "Noinput.h"


CNoinputMenu::CNoinputMenu(const int& _iTreeId,
	const int& _iParentId,
	const std::wstring& _sMenuName,
	const std::wstring& _sDtmf,
	const std::wstring& _sComment,
	//const std::wstring& _sWavName,
	const int& _iSubDialogId,
	const int& _iRepeatCnt,
	const std::wstring& _sGoto,
	const int& _bIsCondition,
	const std::wstring & _sFile1)
	: CBaseBranch(_iTreeId, _iParentId, _sMenuName, //_sMenuText,
	_sDtmf, _sComment, /*_sWavName,*/ _iSubDialogId,
	/*_bIsVisible,*/_iRepeatCnt, L"", L"", _sGoto, L"", L"empty", 0, false, _bIsCondition, false, _sFile1)
{
}

bool CNoinputMenu::IsValid()
{
	// the menu of this type mast have: 
	//   1. id

	//   3. goto

	if (!GetMenuName().length())
		return false;
	//if (!GetWavName().length())
	//	return false;
	if (!GetGoto().length())
		return false;

	return true;
}

//void CNoinputMenu::AddAudio(CComPtr < IXMLDOMDocument > pDoc, CComPtr <IXMLDOMElement> elemMenu)
//{
//	//add audio
//	if (!GetWavName().empty())
//	{
//		CComPtr <IXMLDOMElement> elemAudio = NULL;
//		HRESULT hr = pDoc->createElement(_T("audio"), &elemAudio);
//		if (SUCCEEDED(hr) && elemAudio)
//		{
//			elemAudio->setAttribute(L"src", _variant_t(GetWavName().c_str()));
//			elemMenu->appendChild(elemAudio, NULL);
//		}
//	}
//}

CComPtr <IXMLDOMElement> CNoinputMenu::CreateXMLItem(CComPtr < IXMLDOMDocument > pDoc)
{
	if (!pDoc)
		NULL;

	CComPtr <IXMLDOMElement> elemMenu = NULL;
	HRESULT hr = pDoc->createElement(_T("menu"), &elemMenu);

	if (SUCCEEDED(hr) && elemMenu)
	{
		//set attributs
		//if (GetComment().length())
		//	elemMenu->setAttribute(L"comment", _variant_t(GetComment().c_str()));
		//elemMenu->setAttribute(L"id", _variant_t(GetMenuName().c_str()));
		SetMenuAttributes(GetMenuName(), elemMenu);

		////add audio
		//if (!GetWavName().empty())
		//{
		//	CComPtr <IXMLDOMElement> elemAudio = NULL;
		//	hr = pDoc->createElement(_T("audio"), &elemAudio);
		//	if (SUCCEEDED(hr) && elemAudio)
		//	{
		//		elemAudio->setAttribute(L"src", _variant_t(GetWavName().c_str()));
		//		elemMenu->appendChild(elemAudio, NULL);
		//	}
		//}
		AddAudio(pDoc, elemMenu);

		////add goto
		//CComPtr <IXMLDOMElement> elemGoto = NULL;
		//hr = pDoc->createElement(_T("goto"), &elemGoto);
		//if (SUCCEEDED(hr) && elemGoto)
		//{
		//	elemGoto->setAttribute(L"next", _variant_t(std::wstring(L"#" + GetGoto()).c_str()));
		//	elemMenu->appendChild(elemGoto, NULL);
		//}
		AddGoto(pDoc, elemMenu, false);
	}
	return elemMenu;
}

//void CNoinputMenu::PushVXMLDOMItems(CComPtr < IXMLDOMDocument > pDoc, CComPtr <IXMLDOMElement> elemVXML)
//{
//	CComPtr <IXMLDOMElement> itemMenu = CreateXMLItem(pDoc);
//	if (itemMenu)
//	{
//		elemVXML->appendChild(itemMenu, NULL);
//	}
//}
//
//std::wstring CNoinputMenu::GetLocalRoot()
//{
//	if (GetIsRoot())
//		return GetMenuName();
//
//	if (GetParentPtr())
//		return GetParentPtr()->GetLocalRoot();
//
//	return GetMenuName();
//}


/******************************* eof *************************************/