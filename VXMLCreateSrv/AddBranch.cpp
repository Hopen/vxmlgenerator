///************************************************************************/
///* Name     : VXMLMenuParser\AddBranch.cpp                              */
///* Author   : Andrey Alekseev                                           */
///* Project  : VXMLCreateSrv                                             */
///* Company  : Forte-CT                                                  */
///* Date     : 02 Set 2015                                               */
///************************************************************************/
//
#include "stdafx.h"
#include "AddBranch.h"


CAddBranch::CAddBranch(const std::string& _connectionString)
	: m_connectionString(_connectionString)
{
}


CAddBranch::~CAddBranch()
{
}

int CAddBranch::Call()
{

	if (Execute()) {
		return GetRcParam();
		//return 0;
	}
	return -1;
}

bool CAddBranch::AdAllParam()
{
	bool bRc = true;
	bRc &= AdParam( _T("RETURN_VALUE"),    adInteger, adParamReturnValue, 4);

	bRc &= AdParam(_T("tree_id"       ), adInteger, adParamInput, 11);
	bRc &= AdParam(_T("parent_id"     ), adInteger, adParamInput, 11);
	bRc &= AdParam(_T("menu_name"     ), adVarChar, adParamInput, 200);
	bRc &= AdParam(_T("menu_type"     ), adInteger, adParamInput, 11);
	bRc &= AdParam(_T("menu_text"     ), adVarChar, adParamInput, 500);
	bRc &= AdParam(_T("dtmf"          ), adVarChar, adParamInput, 5);
	bRc &= AdParam(_T("codeName"      ), adVarChar, adParamInput, 50);
	bRc &= AdParam(_T("code"          ), adVarChar, adParamInput, 50);
	bRc &= AdParam(_T("comment"       ), adVarChar, adParamInput, 500);
	bRc &= AdParam(_T("wav_name"      ), adVarChar, adParamInput, 200);
	bRc &= AdParam(_T("wav_text"      ), adVarChar, adParamInput, 2000);
	bRc &= AdParam(_T("sub_dialog_id" ), adInteger, adParamInput, 11);
	bRc &= AdParam(_T("is_visible"    ), adInteger, adParamInput, 11);
	bRc &= AdParam(_T("repeat_cnt"    ), adInteger, adParamInput, 11);
	bRc &= AdParam(_T("goto_repeat"   ), adVarChar, adParamInput, 5);
	bRc &= AdParam(_T("goto_back"     ), adVarChar, adParamInput, 5);
	bRc &= AdParam(_T("goto"          ), adVarChar, adParamInput, 200);
	bRc &= AdParam(_T("city_id"       ), adInteger, adParamInput, 11);
	bRc &= AdParam(_T("wav_break"     ), adVarChar, adParamInput, 20);
	bRc &= AdParam(_T("dtmf_wait"     ), adInteger, adParamInput, 11);
	bRc &= AdParam(_T("goto_root"     ), adVarChar, adParamInput, 5);
	bRc &= AdParam(_T("term_char"	  ), adVarChar, adParamInput, 15);
	bRc &= AdParam(_T("time_out"      ), adInteger, adParamInput, 11);
	bRc &= AdParam(_T("root"          ), adInteger, adParamInput, 11);
	bRc &= AdParam(_T("condition"     ), adInteger, adParamInput, 11);
	return bRc;

}


bool CAddBranch::SetAllParam(
	const int& iTreeId,
	const int& iParentId,
	const std::wstring& sMenuName,
	const int& iMenuType,
	const std::wstring& sMenuText,
	const std::wstring& sDtmf,
	const std::wstring& sCodeName,
	const std::wstring& sCode,
	const std::wstring& sComment,
	const std::wstring& sWavName,
	const std::wstring& sWavText,
	const int& iSubDialogId,
	const bool& bIsVisible,
	const int& iRepeatCnt,
	const std::wstring& sGotoRepeat,
	const std::wstring& sGotoBack,
	const std::wstring& sGoto,
	const int& iCityId,
	const std::wstring& sWavBreak,
	const int& iDtmfWait,
	const std::wstring& sGotoRoot,
	const std::wstring& sTermChar,
	const int& iTimeOut,
	const bool& bIsRoot,
	const bool& bIsCondition
	)
{
	singleton_auto_pointer<CSystemLog> log;

	log->LogString(LEVEL_INFO, L"SetAllParam");

	bool bRc = true;
	bRc &= SetParam(_T("tree_id"), iTreeId);
	bRc &= SetParam(_T("parent_id"), iParentId);
	bRc &= SetParam(_T("menu_name"), sMenuName.c_str());
	bRc &= SetParam(_T("menu_type"), iMenuType);
	bRc &= SetParam(_T("menu_text"), sMenuText.c_str());
	bRc &= SetParam(_T("dtmf"), sDtmf.c_str());
	bRc &= SetParam(_T("codeName"), sCodeName.c_str());
	bRc &= SetParam(_T("code"), sCode.c_str());
	bRc &= SetParam(_T("comment"), sComment.c_str());
	bRc &= SetParam(_T("wav_name"), sWavName.c_str());
	bRc &= SetParam(_T("wav_text"), sWavText.c_str());
	bRc &= SetParam(_T("sub_dialog_id"), iSubDialogId);
	bRc &= SetParam(_T("is_visible"), bIsVisible?1:0);
	bRc &= SetParam(_T("repeat_cnt"), iRepeatCnt);
	bRc &= SetParam(_T("goto_repeat"), sGotoRepeat.c_str());
	bRc &= SetParam(_T("goto_back"), sGotoBack.c_str());
	bRc &= SetParam(_T("goto"), sGoto.c_str());
	bRc &= SetParam(_T("city_id"), iCityId);
	bRc &= SetParam(_T("wav_break"), sWavBreak.c_str());
	bRc &= SetParam(_T("dtmf_wait"), iDtmfWait);
	bRc &= SetParam(_T("goto_root"), sGotoRoot.c_str());
	bRc &= SetParam(_T("term_char"), sTermChar.c_str());
	bRc &= SetParam(_T("time_out"), iTimeOut);
	bRc &= SetParam(_T("root"), bIsRoot?1:0);
	bRc &= SetParam(_T("condition"), bIsCondition ? 1 : 0);

	return bRc;

}


bool CAddBranch::Connect(CADODatabase *pDB /* = NULL */)
{
	singleton_auto_pointer<CSystemLog> log;
	try
	{
		return __super::Connect(L"add_branch", m_connectionString.c_str(), pDB);
	}
	catch (_com_error& err)
	{
		log->LogString(LEVEL_INFO, L"Connect to database failed with error code: 0x%p : %s ", err.Error(), err.Description().GetBSTR());
	}
	catch (...)
	{
		log->LogString(LEVEL_INFO, L"Unknown exception when try to connect to database");
	}
	return false;
}

//bool CAddBranch::GetNextRecordSet(Content_RecordSet_type &recSet)
//{
//	if (m_pRS) {
//		if (m_pRS->EndOfFile != VARIANT_TRUE) {
//			try {
//
//				_variant_t vTreeId;
//				_variant_t vParentId;
//				_variant_t vMenuName;
//				_variant_t vMenuType;
//				_variant_t vMenuText;
//				_variant_t vDtmf;
//				_variant_t vCodeName;
//				_variant_t vCode;
//				_variant_t vComment;
//				_variant_t vWavName;
//				_variant_t vWavText;
//				_variant_t vSubDialogId;
//				_variant_t vIsVisible;
//				_variant_t vRepeatCnt;
//				_variant_t vGotoRepeat;
//				_variant_t vGotoBack;
//				_variant_t vGoto;
//				_variant_t vCityId;
//				_variant_t vWavBreak;
//				_variant_t vDtmfWait;
//				_variant_t vGotoRoot;
//
//
//				recSet.vTreeId = m_pRS->Fields->GetItem(_T("tree_id"))->Value;
//				recSet.vParentId = m_pRS->Fields->GetItem(_T("parent_id"))->Value;
//				recSet.vMenuName = m_pRS->Fields->GetItem(_T("menu_name"))->Value;
//				recSet.vMenuType = m_pRS->Fields->GetItem(_T("menu_type"))->Value;
//				recSet.vMenuText = m_pRS->Fields->GetItem(_T("menu_text"))->Value;
//				recSet.vDtmf = m_pRS->Fields->GetItem(_T("dtmf"))->Value;
//				recSet.vCodeName = m_pRS->Fields->GetItem(_T("code_name"))->Value;
//				recSet.vCode = m_pRS->Fields->GetItem(_T("code"))->Value;
//				recSet.vComment = m_pRS->Fields->GetItem(_T("comment"))->Value;
//				recSet.vWavName = m_pRS->Fields->GetItem(_T("wav_name"))->Value;
//				recSet.vSubDialogId = m_pRS->Fields->GetItem(_T("sub_dialog_id"))->Value;
//				recSet.vIsVisible = m_pRS->Fields->GetItem(_T("is_visible"))->Value;
//				recSet.vRepeatCnt = m_pRS->Fields->GetItem(_T("repeat_cnt"))->Value;
//				recSet.vGotoRepeat = m_pRS->Fields->GetItem(_T("goto_repeat"))->Value;
//				recSet.vGotoBack = m_pRS->Fields->GetItem(_T("goto_back"))->Value;
//				recSet.vGoto = m_pRS->Fields->GetItem(_T("goto"))->Value;
//				recSet.vCityId = m_pRS->Fields->GetItem(_T("city_id"))->Value;
//				recSet.vWavBreak = m_pRS->Fields->GetItem(_T("wav_break"))->Value;
//				recSet.vDtmfWait = m_pRS->Fields->GetItem(_T("dtmf_wait"))->Value;
//				recSet.vGotoRoot = m_pRS->Fields->GetItem(_T("goto_root"))->Value;
//				m_pRS->MoveNext();
//				return true;
//			}
//			catch (_com_error& err)
//			{
//				m_pLog->LogString(LEVEL_INFO, L"\"get_trees\" recordset failed with error code: 0x%p : %s ", err.Error(), err.Description().GetBSTR());
//			}
//			catch (...)
//			{
//				m_pLog->LogString(LEVEL_INFO, L"Unknown exception when try to get \"get_trees\" recordset");
//			}
//		}
//	}
//	return false;
//
//}

/******************************* eof *************************************/