/************************************************************************/
/* Name     : CDRLog\Timer.cpp                                          */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-CT                                                  */
/* Date     : 19 Feb 2015                                               */
/************************************************************************/
#include "stdafx.h"
#include "Timer.h"

//const int CHECK_TIME  = 30000;
//const int EXPIREDTIME = 30000;
const int FIRST_CHECK_TIME = 3;

CTimer::CTimer(std::shared_ptr<boost::asio::io_service>_io/*boost::asio::io_service& _io*/, SimpleCallbackFunc _callback, int _checkTime)
	: m_strand(*_io),
	m_checkTime(_checkTime),
	m_timer(*_io, boost::posix_time::seconds(FIRST_CHECK_TIME)),
	m_callBackFunc(_callback)
{
	m_timer.async_wait(m_strand.wrap(boost::bind(&CTimer::Loop, this)));
}

void CTimer::Loop()
{
	m_callBackFunc();

	m_timer.expires_at(m_timer.expires_at() + m_checkTime);
	m_timer.async_wait(m_strand.wrap(boost::bind(&CTimer::Loop, this)));

}

CTimer::~CTimer()
{
	
}

void CTimer::CancelTimer()
{
	m_timer.cancel();
}

/******************************* eof *************************************/