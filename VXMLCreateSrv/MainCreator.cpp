/************************************************************************/
/* Name     : VXMLCreateSrv\MainCreator.cpp                             */
/* Author   : Andrey Alekseev                                           */
/* Project  : VXMLCreateSrv                                             */
/* Company  : Forte-CT                                                  */
/* Date     : 04 Aug 2015                                               */
/************************************************************************/

#include "stdafx.h"
#include "boost/filesystem/operations.hpp" // includes boost/filesystem/path.hpp
#include "boost/filesystem/fstream.hpp"    
#include <boost/algorithm/string.hpp>
#include "MainCreator.h"
//#include "Log\logger.h"
#include "Configuration\SystemConfiguration.h"
//#include "..\ado\ADODatabase.h"
#include "smartADODataBase.h"
#include "GetBranchesByTree.h"
#include "AddBranch.h"
#include "branches.h"
#include "Router/router_manager.h"

using namespace boost::filesystem;

const int EXPIREDTIME = 30;


///******************************* DATABASE CATALOGS *************************************/
//
//typedef std::map<int, std::wstring> ServicesContainer;
//typedef std::map<int, std::wstring> RegionsContainer;
//typedef std::map<int, std::wstring> AppsContainer;
//
//static void MakeServicesList(ServicesContainer& list)
//{
//	list[1] = L"0611";
//	//...
//}
//
//static void MakeRegionsList(RegionsContainer& list)
//{
//	list[1] = L"Moscow";
//	//...
//}
//
//static void MakeAppsList(AppsContainer& list)
//{
//	list[1] = L"0674";
//	//...
//}
//
//
//static std::wstring GetServiceNameById(const int& id)
//{
//	ServicesContainer list;
//	MakeServicesList(list);
//
//	ServicesContainer::const_iterator cit = list.find(id);
//	if (cit != list.end())
//		return cit->second;
//	return L"Unknown";
//}
//
//static std::wstring GetRegionNameById(const int& id)
//{
//	RegionsContainer list;
//	MakeRegionsList(list);
//
//	RegionsContainer::const_iterator cit = list.find(id);
//	if (cit != list.end())
//		return cit->second;
//	return L"Unknown";
//}
//
//static std::wstring GetAppNameById(const int& id)
//{
//	AppsContainer list;
//	MakeAppsList(list);
//
//	AppsContainer::const_iterator cit = list.find(id);
//	if (cit != list.end())
//		return cit->second;
//	return L"Unknown";
//}
//
///************************************************************************/

CMainCreator::CMainCreator() : m_log(NULL), m_io(new boost::asio::io_service())
{
}


CMainCreator::~CMainCreator()
{
}

long CMainCreator::Init(CSystemLog* pLog)
{
	if (!pLog)
		return -1111;

	m_log = pLog;

	m_log->LogString(LEVEL_FINEST, L"CMainCreator initializing...");

	SystemConfig settings;
	m_strConnectionString = wtos(settings->safe_get_config_value(L"ConnectionString", std::wstring(L"")));

	//CConfigurationSettings settings;
	//try
	//{
	//	m_strConnectionString = settings[_T("ConnectionString")].ToStr();
	//}
	if (m_strConnectionString.empty())
	{
		m_log->LogString(LEVEL_FATAL, L"Exception reading ConnectionString config key");
		return -1;
	}

	//InitTimer(iFileRecorderTimer);

	m_log->LogString(LEVEL_INFO, L"Subscribe on events...");
	SubscribeOnEvents();

	m_log->LogString(LEVEL_INFO, L"Connecting to router...");
	//CRouterManager::ConnectHandler chandler = boost::bind(&CMainCreator::RouterConnectHandler, this);
	//m_router->RegisterConnectHandler(chandler);
	//CRouterManager::DeliverErrorHandler ehandler = boost::bind(&CMainCreator::RouterDeliverErrorHandler, this, _1, _2, _3);
	//m_router->RegisterDeliverErrorHandler(ehandler);

	//CRouterManager::StatusChangeHandler status_handler = boost::bind(&CMainCreator::AppStatusChange, this, _1, _2);
	//m_router->RegisterStatusHandler(status_handler);

	//m_router->Connect();

	CRouterManager::ConnectHandler chandler = boost::bind(&CMainCreator::RouterConnectHandler, this);
	m_router.RegisterConnectHandler(chandler);
	CRouterManager::DeliverErrorHandler ehandler = boost::bind(&CMainCreator::RouterDeliverErrorHandler, this, _1, _2, _3);
	m_router.RegisterDeliverErrorHandler(ehandler);

	CRouterManager::StatusChangeHandler status_handler = boost::bind(&CMainCreator::AppStatusChange, this, _1, _2);
	m_router.RegisterStatusHandler(status_handler);


	return 0;
}


static void OnTimer1Expired()
{
	singleton_auto_pointer<CMainCreator> collector;
	collector->LoadTree();
	//collector->IsDataBaseChange();

}


void CMainCreator::InitTimer(/*const unsigned int& msec*/)
{
	SystemConfig settings;

	int iFileRecorderTimer = settings->safe_get_config_value(L"ConnectionString", EXPIREDTIME);

	m_timer1.reset(new CTimer(m_io, OnTimer1Expired, iFileRecorderTimer));
	boost::thread t(boost::bind(&boost::asio::io_service::run, m_io));
}

//class CTree
//{
//public:
//	CTree(const int& _iServiceId, const int& _iRegionId, const int& _iAppId)
//		: m_iServiceId(_iServiceId), m_iRegionId(_iRegionId), m_iAppId(_iAppId)
//	{
//
//	}
//	int GetServiceId()const { return m_iServiceId; }
//	int GetRegionId()const { return m_iRegionId; }
//	int GetAppId()const { return m_iAppId; }
//private:
//	int m_iServiceId;
//	int m_iRegionId;
//	int m_iAppId;
//};
//
//typedef std::vector <CTree> TreesContainer;

//bool CMainCreator::IsDataBaseChange()
//{
//	try
//	{
//
//		m_log->LogString(LEVEL_INFO, L"Connecting to database");
//		//CSmartADODataBase database(m_strConnectionString);
//	}
//	catch (_com_error& err)
//	{
//		m_log->LogString(LEVEL_SEVERE, L"Check database failed with error code: 0x%p : %s ", err.Error(), err.Description().GetBSTR());
//		return false;
//	}
//	catch (CConfigurationLoadException& _exception)
//	{
//		m_log->LogString(LEVEL_SEVERE, L"config reading failed: %s", _exception.ToString());
//		return false;
//	}
//	catch (CString& strError)
//	{
//		m_log->LogString(LEVEL_SEVERE, L"Check database failed with error : %s ", strError);
//		return false;
//	}
//	catch (boost::system::system_error& e)
//	{
//		m_log->LogString(LEVEL_SEVERE, L"asio exception: %s", CString(e.what()).GetString());
//		return false;
//	}
//	catch (...)
//	{
//		m_log->LogString(LEVEL_SEVERE, L"Unknown exception, when trying to check database");
//		return false;
//	}
//
//	TreesContainer trees;
//	//execute procedure -> get trees
//	int tmp_service_id = 1;
//	int tmp_region_id = 1;
//	int tmp_app_id = 1;
//
//	// get trees by service, region and appl
//
//	int tmp_tree_id = 83;
//	CConfigurationSettings settings;
//	try
//	{
//		tmp_tree_id = settings[_T("tmp_tree_id")].ToInt();
//	}
//	catch (...)
//	{
//		m_log->LogString(LEVEL_CONFIG, L"Exception reading tmp_tree_id config key");
//	}
//
//
//	//std::wstring finalURI = L"c:\\is3\\Scripts\\vxml\\0611_DTI_creditk_cg.vxml";
//	std::wstring finalURI = L"d:\\tmpCreateVXMLSrv.vxml";
//	try
//	{
//		finalURI = settings[_T("final_uri")].ToString();
//	}
//	catch (...)
//	{
//		m_log->LogString(LEVEL_CONFIG, L"Exception reading final_uri config key");
//	}
//
//	//trees.push_back(CTree(tmp_service_id, tmp_region_id, tmp_app_id));
//
//	//m_log->LogString(LEVEL_INFO, L"%i - new trees has found", trees.size());
//
//	//for each(const CTree& tree in trees)
//	//{
//		if (!LoadTree(tmp_tree_id/*tree.GetServiceId(), tree.GetRegionId(), tree.GetAppId()*/, finalURI))
//		{
//			//failed
//		}
//		else
//		{
//			//successful
//		}
//	//}
//
//	return true;
//
//}

bool CMainCreator::LoadTree(const int& _iTreeId, const std::wstring& finalURI)
{
	try
	{
		CBaseBranch/*<CChoiceMenu>*/::BranchContainer branches;

#ifndef TEST_DATA_BASE
		if (1)
		{
			m_log->LogString(LEVEL_INFO, L"Connecting to database, connectionString = %s", std::wstring(_bstr_t(m_strConnectionString.c_str())).c_str());
			CSmartADODataBase database(m_strConnectionString);

			m_log->LogString(LEVEL_INFO, L"Connecting \"get_branches_by_tree\" to database");

			CGetBranchesByTree GTreesProc(m_log, m_strConnectionString);
			if (!GTreesProc.Initialize(L"get_branches_by_tree", database.get()))
			{
				m_log->LogString(LEVEL_INFO, L"Cannot Initialize \"get_branches_by_tree\" proc");
				return false;
			}

			m_log->LogString(LEVEL_INFO, L"\"get_branches_by_tree\" Initializing completed");

			if (!GTreesProc.AdAllParam())
			{
				m_log->LogString(LEVEL_INFO, L"Cannot AdAllParam \"get_branches_by_tree\"");
				return false;
			}

			if (!GTreesProc.Connect(database.get()))
			{
				throw CString(L"\"get_branches_by_tree\" proc: cannot connect to database");
			}

			m_log->LogString(LEVEL_SEVERE, L"Get Branches by id [%i]",
				_iTreeId);

			if (!GTreesProc.SetAllParam(_iTreeId))
			{
				// cannot set upload_data proc params
				CString Err;
				Err.Format(L"\"get_branches_by_tree\" proc - error in incoming params");
				throw Err;
			}

			m_log->LogString(LEVEL_INFO, L"\"get_branches_by_tree\" call");
			if (GTreesProc.Call() == -1)
			{
				throw CString(L"\"get_branches_by_tree\" proc - execution failed");
			}

			Content_RecordSet record;
			//typedef std::map <int, Content_RecordSet > RecordContainer;
			//RecordContainer branchMap;

			//while (GTreesProc.GetNextRecordSet(record))
			//{
			//	//m_log->LogString(LEVEL_INFO, L"TreeID = %i, ParentID = %i, MenuName = %s, Dtmf: %s",
			//	//	record.vTreeId.intVal, record.vParentId.intVal,
			//	//	std::wstring(record.vMenuName.bstrVal).c_str(), std::wstring(record.vDtmf.bstrVal).c_str());

			//	m_log->LogString(LEVEL_INFO, L"ID = %i, ParentID = %i, MenuName = %s, Dtmf: %s",
			//		record.vId.intVal, record.vParentId.intVal,
			//		std::wstring(record.vMenuName.bstrVal).c_str(), std::wstring(record.vDtmf.bstrVal).c_str());

			//	branchMap[record.vId.intVal] = record;
			//}

			//for each(std::pair<int, Content_RecordSet> record in branchMap)
			//{
			//	switch (record.second.vMenuType.intVal)
			//	{
			//	case MT_CHOICE:
			//	{


			//		CChoiceMenu menu(
			//			record.second.vId.intVal,
			//			record.second.vParentId.intVal,
			//			record.second.vMenuName.bstrVal,
			//			record.second.vDtmf.bstrVal,
			//			record.second.vComment.bstrVal,
			//			record.second.vWavName.bstrVal,
			//			record.second.vSubDialogId.intVal,
			//			//record.vIsVisible.boolVal,
			//			record.second.vRepeatCnt.intVal,
			//			record.second.vGotoRepeat.bstrVal,
			//			record.second.vGotoBack.bstrVal,
			//			record.second.vGoto.bstrVal,
			//			record.second.vGotoRoot.bstrVal,
			//			record.second.vTermChar.bstrVal,
			//			record.second.vTimeOut.intVal,
			//			record.second.vRoot.intVal,
			//			record.second.vCondition.intVal);
			//		break;
			//	}
			//	case MT_GOTO:
			//	{
			//		break;
			//	}
			//	default:
			//		break;
			//	}
			//}

			while (GTreesProc.GetNextRecordSet(record))
			{
				//m_log->LogString(LEVEL_INFO, L"TreeID = %i, ParentID = %i, MenuName = %s, Dtmf: %s",
				//	record.vTreeId.intVal, record.vParentId.intVal,
				//	std::wstring(record.vMenuName.bstrVal).c_str(), std::wstring(record.vDtmf.bstrVal).c_str());
				
				m_log->LogString(LEVEL_INFO, L"ID = %i, ParentID = %i, MenuName = %s, Dtmf: %s", 
					record.vId.intVal, record.vParentId.intVal,
					std::wstring(record.vMenuName.bstrVal).c_str(), std::wstring(record.vDtmf.bstrVal).c_str());


				//if (record.vMenuType.intVal == MT_SMART)
				//{
				//	m_log->LogString(LEVEL_INFO, L"_sComment = %s, ID = %i, ParentID = %i, MenuName = %s, Dtmf: %s, "\
				//		" _sGotoRepeat = %s, _sGotoBack = %s, _sGoto: %s, "\
				//		"_sGotoRoot = %s, _bRoot = %s,"\
				//		" _sFile1 = %s"/*\
				//		//" _sFile1 = %s, _sFile2: %s, "\
				//		"_sFile3 = %s, _sFile4 = %s"*/
				//		,
				//		std::wstring(record.vComment.bstrVal).c_str(),
				//		record.vId.intVal, record.vParentId.intVal,
				//		std::wstring(record.vMenuName.bstrVal).c_str(), std::wstring(record.vDtmf.bstrVal).c_str(),
				//		std::wstring(record.vGotoRepeat.bstrVal).c_str(),
				//		std::wstring(record.vGotoBack.bstrVal).c_str(),
				//		std::wstring(record.vGoto.bstrVal).c_str(),
				//		std::wstring(record.vGotoRoot.bstrVal).c_str(),
				//		record.vRoot.intVal==1?L"true":L"false",
				//		std::wstring(record.vFile1.bstrVal).c_str()/*,
				//		std::wstring(record.vFile2.bstrVal).c_str(),
				//		std::wstring(record.vFile3.bstrVal).c_str(),
				//		std::wstring(record.vFile4.bstrVal).c_str()*/
				//		);

				//}
				//else

				//m_log->LogString(LEVEL_INFO, L"ID = %i, ParentID = %i, MenuName = %s, Dtmf: %s",
				//	record.vId.intVal, record.vParentId.intVal,
				//	std::wstring(record.vMenuName.bstrVal).c_str(), std::wstring(record.vDtmf.bstrVal).c_str());


				switch (record.vMenuType.intVal)
				{
				case MT_CHOICE:
				{
					//CChoiceMenu menu(
					//	record.vId.intVal,
					//	record.vParentId.intVal,
					//	record.vMenuName.bstrVal,
					//	record.vDtmf.bstrVal,
					//	record.vComment.bstrVal,
					//	record.vWavName.bstrVal,
					//	record.vSubDialogId.intVal,
					//	//record.vIsVisible.boolVal,
					//	record.vRepeatCnt.intVal,
					//	record.vGotoRepeat.bstrVal,
					//	record.vGotoBack.bstrVal,
					//	record.vGoto.bstrVal,
					//	record.vGotoRoot.bstrVal,
					//	record.vTermChar.bstrVal,
					//	record.vTimeOut.intVal,
					//	record.vRoot.intVal,
					//	record.vCondition.intVal);

					////CBaseBranch<CChoiceMenu>::BranchPtr ptr(new CChoiceMenu(
					////	record.vId.intVal,
					////	record.vParentId.intVal,
					////	record.vMenuName.bstrVal,
					////	record.vDtmf.bstrVal,
					////	record.vComment.bstrVal,
					////	record.vWavName.bstrVal,
					////	record.vSubDialogId.intVal,
					////	//record.vIsVisible.boolVal,
					////	record.vRepeatCnt.intVal,
					////	record.vGotoRepeat.bstrVal,
					////	record.vGotoBack.bstrVal,
					////	record.vGoto.bstrVal,
					////	record.vGotoRoot.bstrVal,
					////	record.vTermChar.bstrVal,
					////	record.vTimeOut.intVal,
					////	record.vRoot.intVal,
					////	record.vCondition.intVal));

					branches.push_back(CBaseBranch::BranchPtr(new CChoiceMenu(
						record.vId.intVal,
						record.vParentId.intVal,
						record.vMenuName.bstrVal,
						record.vDtmf.bstrVal,
						record.vComment.bstrVal,
						//record.vWavName.bstrVal,
						record.vSubDialogId.intVal,
						//record.vIsVisible.boolVal,
						record.vRepeatCnt.intVal,
						record.vGotoRepeat.bstrVal,
						record.vGotoBack.bstrVal,
						record.vGoto.bstrVal,
						record.vGotoRoot.bstrVal,
						record.vTermChar.bstrVal,
						record.vTimeOut.intVal,
						record.vRoot.intVal,
						record.vCondition.intVal,
						record.vAddToHistory.intVal,
						record.vFile1.bstrVal)));
					break;
				}
				case MT_GOTO:
				{
					branches.push_back(CBaseBranch::BranchPtr(new CGotoMenu(
						record.vId.intVal,
						record.vParentId.intVal,
						record.vMenuName.bstrVal,
						record.vDtmf.bstrVal,
						record.vComment.bstrVal,
						//record.vWavName.bstrVal,
						record.vSubDialogId.intVal,
						//record.vIsVisible.boolVal,
						record.vGotoRepeat.bstrVal,
						record.vGotoBack.bstrVal,
						record.vGoto.bstrVal,
						record.vCondition.intVal,
						record.vAddToHistory.intVal,
						record.vFile1.bstrVal)));

					break;
				}
				case MT_FORM_SUBDIALOG:
				{
					branches.push_back(CBaseBranch::BranchPtr(new CFormSubdialog(
						record.vId.intVal,
						record.vParentId.intVal,
						record.vMenuName.bstrVal,
						record.vDtmf.bstrVal,
						record.vComment.bstrVal,
						//record.vWavName.bstrVal,
						record.vSubDialogId.intVal,
						//record.vIsVisible.boolVal,
						record.vGotoRepeat.bstrVal,
						record.vGotoBack.bstrVal,
						record.vGoto.bstrVal,
						record.vCondition.intVal,
						record.vFile1.bstrVal,
						record.vFuncName.bstrVal,
						record.vFuncType.intVal
						)));
					break;
				}
				case MT_NOINPUT:
				{
					branches.push_back(CBaseBranch::BranchPtr(new CNoinputMenu(
						record.vId.intVal,
						record.vParentId.intVal,
						record.vMenuName.bstrVal,
						record.vDtmf.bstrVal,
						record.vComment.bstrVal,
						//record.vWavName.bstrVal,
						record.vSubDialogId.intVal,
						//record.vIsVisible.boolVal,
						record.vRepeatCnt.intVal,
						record.vGoto.bstrVal,
						record.vCondition.intVal,
						record.vFile1.bstrVal)));
					break;
				}
				case MT_NOMATCH:
				{
					branches.push_back(CBaseBranch::BranchPtr(new CNomatchMenu(
						record.vId.intVal,
						record.vParentId.intVal,
						record.vMenuName.bstrVal,
						record.vDtmf.bstrVal,
						record.vComment.bstrVal,
						//record.vWavName.bstrVal,
						record.vSubDialogId.intVal,
						//record.vIsVisible.boolVal,
						record.vRepeatCnt.intVal,
						record.vGoto.bstrVal,
						record.vCondition.intVal,
						record.vFile1.bstrVal)));
					break;
				}
				case MT_SMART:
				{
					branches.push_back(CBaseBranch::BranchPtr(new CSmartMenu(
						record.vId.intVal,
						record.vParentId.intVal,
						record.vMenuName.bstrVal,
						record.vDtmf.bstrVal,
						record.vComment.bstrVal,
						record.vGotoRepeat.bstrVal,
						record.vGotoBack.bstrVal,
						record.vGoto.bstrVal,
						record.vGotoRoot.bstrVal,
						record.vRoot.intVal,
						record.vAddToHistory.intVal,
						record.vFile1.bstrVal,
						record.vFile2.bstrVal, 
						record.vFile3.bstrVal, 
						record.vFile4.bstrVal )));
					break;
				}
				}
			}
		}
#else


		if (1) //testing
		{
			branches.push_back(CBaseBranch::BranchPtr(new CChoiceMenu(1, 0, L"menu_1", L"", L"com", /*L"1.wav",*/ 0, /*true,*/0, L"#", L"0", L"", L"*", L"1234567890", 0, false,0, 1, L"1.wav")));
			branches.push_back(CBaseBranch::BranchPtr(new CChoiceMenu(2, 1, L"menu_11", L"1", L"com",/* L"1.1.wav",*/ 0,/* true,*/2, L"#", L"0", L"", L"*", L"empty", 0, true,0, 1,  L"1.1.wav")));
			branches.push_back(CBaseBranch::BranchPtr(new CChoiceMenu(3, 1, L"menu_12", L"2", L"com", /*L"1.2.wav",*/ 0, /*true,*/3, L"#", L"0", L"", L"*", L"empty", 0, false,0, 1, L"1.2.wav")));
			branches.push_back(CBaseBranch::BranchPtr(new CGotoMenu  (4, 2, L"menu_111", L"1", L"com", /*L"1.1.1.wav",*/ 0,/* true, */L"#", L"0", L"menu_1",1, 1, L"1.1.1.wav")));
			branches.push_back(CBaseBranch::BranchPtr(new CChoiceMenu(5, 2, L"menu_112", L"2", L"com", /*L"1.1.2.wav",*/ 0, /*true,*/0, L"#", L"0", L"", L"*", L"#", 0, false,0, 0, L"1.1.2.wav")));
			branches.push_back(CBaseBranch::BranchPtr(new CFormSubdialog(6, 5, L"CheckPayment1", L"1", L"", /*L"",*/ 1, /*true,*/ L"", L"", L"menu_112", 1, L"", L"test_func", 0 )));
			branches.push_back(CBaseBranch::BranchPtr(new CFormSubdialog(7, 2, L"CheckPayment2", L"5", L"", /*L"",*/ 1, /*true,*/ L"", L"", L"menu_111", 0, L"", L"", 1)));
			branches.push_back(CBaseBranch::BranchPtr(new CNoinputMenu(8, 1, L"menu_1_noinput", L"", L"com", /*L"1.noinput.wav",*/ 0, /*true,*/1, L"menu_112",0, L"1.noinput.wav")));
			branches.push_back(CBaseBranch::BranchPtr(new CNomatchMenu(9, 1, L"menu_1_nomatch", L"", L"com", /*L"1.nomatch.wav",*/ 0, /*true,*/1, L"menu_1",0, L"1.nomatch.wav")));
			branches.push_back(CBaseBranch::BranchPtr(new CSmartMenu(10, 1, L"menu_13", L"3", L"com", L"#", L"0", L"menu_1", L"*", false, 1, L"file1.wav", L"file2.wav", L"file3.wav", L"file4.wav" )));
			branches.push_back(CBaseBranch::BranchPtr(new CGotoMenu(11, 10, L"menu_131", L"1", L"com",/* L"1.3.1.wav",*/ 0,/* true, */L"#", L"0", L"menu_1", 1, 0,  L"1.3.1.wav")));
			branches.push_back(CBaseBranch::BranchPtr(new CGotoMenu(12, 10, L"menu_132", L"2", L"com", /*L"1.3.2.wav",*/ 0,/* true, */L"#", L"0", L"menu_1", 1, 0, L"1.3.2.wav")));

		}
#endif

		if (!branches.size())
		{
			m_log->LogString(LEVEL_SEVERE, L"There are no branches for id [%i]", _iTreeId);
			return false;
		}

		m_log->LogString(LEVEL_SEVERE, L"%i branches has found", branches.size());

		// make tree
		if (1)
		{
			typedef std::multimap <int, CBaseBranch::BranchPtr> BranchSuperContainer;
			BranchSuperContainer branchTmpMap;

			if (1)
			{
				CBaseBranch/*<CChoiceMenu>*/::BranchContainer tmpBranches;

				for each (CBaseBranch::BranchPtr branch in branches)
				{
					if (!branch->IsValid())
						continue;
					branchTmpMap.insert(std::pair<int, CBaseBranch::BranchPtr>(branch->GetParentId(), branch));
					tmpBranches.push_back(branch);
				}
				branches.swap(tmpBranches);
			}

			for each (CBaseBranch::BranchPtr branch in branches)
			{
				BranchSuperContainer::iterator it = branchTmpMap.find(branch->GetId());
				while (it != branchTmpMap.end() && (it->first == branch->GetId()))
				{
					//it->second->AddChild(branch);
					branch->AddChild(it->second);
					it->second->SetParentPtr(branch);
					++it;
				}
			}
		}

		//make xml tree
		// 1. CREATE VXML DOC
		CComPtr < IXMLDOMDocument > pVXMLDoc;
		HRESULT hr = pVXMLDoc.CoCreateInstance(CLSID_DOMDocument, NULL, CLSCTX_INPROC_SERVER);
		if (FAILED(hr))
		{
			m_log->LogString(LEVEL_WARNING, _T("Error: CoCreateInstance IXMLDOMDocument failed"));
			return false;
		}
		pVXMLDoc->put_async(VARIANT_FALSE);

		CComPtr <IXMLDOMProcessingInstruction> pPI;
		hr = pVXMLDoc->createProcessingInstruction(L"xml", L"version='1.0' encoding='utf-8'", &pPI);
		if (FAILED(hr))
		{
			m_log->LogString(LEVEL_WARNING, _T("Error: createProcessingInstruction failed"));
			return false;
		}
		pVXMLDoc->appendChild(pPI, NULL);
		
		CComPtr <IXMLDOMElement> elemVXML = NULL;
		hr = pVXMLDoc->createElement(_T("vxml"), &elemVXML);
		if (FAILED(hr) || !elemVXML)
		{
			m_log->LogString(LEVEL_WARNING, _T("Error: createElement \"vxml\" failed"));
			return false;
		}
		elemVXML->setAttribute(L"version", _variant_t(L"1.0"));
		elemVXML->setAttribute(L"xmlns", _variant_t(L"x-schema:vxml"));

		pVXMLDoc->appendChild(elemVXML, NULL);


		//2. ADD BRANCHES TO ROOT
		for each (CBaseBranch::BranchPtr branch in branches)
		{
			//CComPtr <IXMLDOMElement> itemMenu = branch->CreateXMLItem(pVXMLDoc);
			//if (itemMenu)
			//{
			//	elemVXML->appendChild(itemMenu, NULL);
			//}
			branch->PushVXMLDOMItems(pVXMLDoc, elemVXML);
		}

		//3. SAVE XML 
		if (1) // test XML
		{
			_bstr_t sXML;
			pVXMLDoc->get_xml(&sXML.GetBSTR());
			std::wstring s(sXML.GetBSTR());
		}

		// SAVE DOCUMENT
		if (1)
		{
			m_log->LogString(LEVEL_INFO, _T("Save xml to \"%s\""), finalURI.c_str());
			pVXMLDoc->save(variant_t(finalURI.c_str()));
		}

	}
	catch (_com_error& err)
	{
		m_log->LogString(LEVEL_SEVERE, L"Load tree failed with error code: 0x%p : %s ", err.Error(), err.Description().GetBSTR());
		return false;
	}
	//catch (CConfigurationLoadException& _exception)
	//{
	//	m_log->LogString(LEVEL_SEVERE, L"config reading failed: %s", _exception.ToString());
	//	return false;
	//}
	catch (CString& strError)
	{
		m_log->LogString(LEVEL_SEVERE, L"Load tree failed with error : %s ", strError);
		return false;
	}
	catch (boost::system::system_error& e)
	{
		m_log->LogString(LEVEL_SEVERE, L"asio exception: %s", CString(e.what()).GetString());
		return false;
	}
	catch (...)
	{
		m_log->LogString(LEVEL_SEVERE, L"Unknown exception, when CMainCreator try to load new tree");
		return false;
	}


	return true;

}

bool CMainCreator::LoadTree()
{
	SystemConfig settings;

	int tmp_tree_id = settings->safe_get_config_value(L"tmp_tree_id", 83);


	//std::wstring finalURI = L"c:\\is3\\Scripts\\vxml\\0611_DTI_creditk_cg.vxml";
	std::wstring finalURI = settings->safe_get_config_value(L"final_uri", std::wstring(L"d:\\tmpCreateVXMLSrv.vxml"));

	LoadTree(tmp_tree_id, finalURI);

	return false;
}

class CFakeBranch : public CBaseBranch //<CFakeBranch>
{
	//friend class CBaseBranch < CFakeBranch > ;
public:
	CFakeBranch(const int& _iMenuType,
		const int& _iTreeId,
		const int& _iParentId,
		const std::wstring& _sMenuName,
		//const std::wstring& _sMenuText,
		const std::wstring& _sDtmf,
		const std::wstring& _sComment,
		//const std::wstring& _sWavName,
		const int& _iSubDialogId,
		//const bool& _bIsVisible,
		const int& _iRepeatCnt,
		const std::wstring& _sGotoRepeat,
		const std::wstring& _sGotoBack,
		const std::wstring& _sGoto,
		const std::wstring& _sGotoRoot,
		const std::wstring& _sTermChar,
		const int& _iTimeOut,
		const int& _bRoot,
		const int& _bIsCondition,
		const int& _bAddToHistory,
		const std::wstring & _sFile1) :
		CBaseBranch(_iTreeId, _iParentId, _sMenuName, //_sMenuText,
		_sDtmf, _sComment, /*_sWavName, */_iSubDialogId,
		/*_bIsVisible,*/_iRepeatCnt, _sGotoRepeat, _sGotoBack, _sGoto, _sGotoRoot, _sTermChar, _iTimeOut, _bRoot, _bIsCondition, _bAddToHistory, _sFile1),
		m_iMenuType(_iMenuType)
	{

	}

	//interface
	virtual int GetTypeMenu(){ return m_iMenuType; }
	//virtual void AddChild(BranchPtr child){}
	virtual CComPtr <IXMLDOMElement> CreateXMLItem(CComPtr < IXMLDOMDocument > pDoc){ return NULL; }
	virtual bool IsValid(){ return false; }

	//void PushVXMLDOMItems(CComPtr < IXMLDOMDocument > pDoc, CComPtr <IXMLDOMElement> elemVXML)
	//{
	//	CComPtr <IXMLDOMElement> itemMenu = CreateXMLItem(pDoc);
	//	if (itemMenu)
	//	{
	//		elemVXML->appendChild(itemMenu, NULL);
	//	}
	//}

	//std::wstring GetLocalRoot()
	//{
	//	if (GetIsRoot())
	//		return GetMenuName();

	//	if (GetParentPtr())
	//		return GetParentPtr()->GetLocalRoot();

	//	return GetMenuName();
	//}

private:
	int m_iMenuType;
};

bool CMainCreator::TestWriteDatabase()
{
	//singleton_auto_pointer<CSystemLog> log;
	//std::shared_ptr<CAddBranch> pAddProc;
	//log->LogString(LEVEL_INFO, L"Connecting to database");

	//CConfigurationSettings settings;
	//std::string sConnectionString;
	//try
	//{
	//	sConnectionString = settings[_T("ConnectionString")].ToStr();
	//}
	//catch (...)
	//{
	//	log->LogString(LEVEL_CONFIG, L"Exception reading ConnectionString config key");
	//	return false;
	//}

	//log->LogString(LEVEL_CONFIG, L"Connection string: %s", std::wstring(_bstr_t(sConnectionString.c_str())).c_str());

	//CSmartADODataBase database(sConnectionString);

	//if (database.get())
	//{
	//	log->LogString(LEVEL_INFO, L"Connecting \"add_branch\" to database");

	//	pAddProc.reset(new CAddBranch(sConnectionString));
	//	if (!pAddProc->Initialize(L"add_branch", database.get()))
	//	{
	//		log->LogString(LEVEL_INFO, L"Cannot Initialize \"add_branch\" proc");
	//		return false;
	//	}

	//	log->LogString(LEVEL_INFO, L"\"add_branch\" Initializing completed");

	//	if (!pAddProc->AdAllParam())
	//	{
	//		log->LogString(LEVEL_INFO, L"Cannot AdAllParam \"add_branch\"");
	//		return false;
	//	}

	//	if (!pAddProc->Connect(database.get()))
	//	{
	//		throw CString(L"\"add_branch\" proc: cannot connect to database");
	//	}
	//}


	//CBaseBranch::BranchContainer branches;
	////                                                        menu_type        id, pid, name            dtmf  comment   wav             subid      repeatcnt repeat  back   goto       root   term        time  isroot iscondition
	//branches.push_back(CBaseBranch::BranchPtr(new CFakeBranch(MT_CHOICE        , 1, 0, L"menu_1"        , L"" , L"com", L"1.wav"        , 0,/*true,*/0,      L"#", L"0", L""        , L"*", L"1234567890", 0    , false, false)));
	//branches.push_back(CBaseBranch::BranchPtr(new CFakeBranch(MT_CHOICE        , 2, 1, L"menu_11"       , L"1", L"com", L"1.1.wav"      , 0,/*true,*/2,      L"#", L"0", L""        , L"*", L"empty"     , 10000, true , false)));
	//branches.push_back(CBaseBranch::BranchPtr(new CFakeBranch(MT_CHOICE        , 3, 1, L"menu_12"       , L"2", L"com", L"1.2.wav"      , 0,/*true,*/3,      L"#", L"0", L""        , L"*", L"empty"     , 0    , false, false)));
	//branches.push_back(CBaseBranch::BranchPtr(new CFakeBranch(MT_GOTO          , 4, 2, L"menu_111"      , L"1", L"com", L"1.1.1.wav"    , 0,/*true,*/0,      L"#", L"0", L"menu_1"  , L"*", L"empty"     , 0    , false, true)));
	//branches.push_back(CBaseBranch::BranchPtr(new CFakeBranch(MT_CHOICE        , 5, 2, L"menu_112"      , L"2", L"com", L"1.1.2.wav"    , 0,/*true,*/0,      L"#", L"0", L""        , L"*", L"#"         , 0    , false, false)));
	//branches.push_back(CBaseBranch::BranchPtr(new CFakeBranch(MT_FORM_SUBDIALOG, 6, 5, L"CheckPayment1" , L"1", L""   , L""             , 1,/*true,*/0,      L"" , L"" , L"menu_112", L"*", L"empty"     , 0    , false, false)));
	//branches.push_back(CBaseBranch::BranchPtr(new CFakeBranch(MT_FORM_SUBDIALOG, 7, 2, L"CheckPayment2" , L"5", L""   , L""             , 1,/*true,*/0,      L"" , L"" , L"menu_111", L"*", L"empty"     , 0    , false, false)));
	//branches.push_back(CBaseBranch::BranchPtr(new CFakeBranch(MT_NOINPUT       , 8, 1, L"menu_1_noinput", L"" , L"com", L"1.noinput.wav", 0,/*true,*/1,      L"" , L"" , L"menu_112", L"*", L"empty"     , 0    , false, false)));
	//branches.push_back(CBaseBranch::BranchPtr(new CFakeBranch(MT_NOMATCH       , 9, 1, L"menu_1_nomatch", L"" , L"com", L"1.nomatch.wav", 0,/*true,*/1,      L"" , L"" , L"menu_1"  , L"*", L"empty"     , 0    , false, false)));


	//int iMenu1 = 0,
	//	iMenu11 = 0,
	//	iMenu112 = 0;

	////int iDatabaseCount = 1001;


	//int count = 0;
	//int iLastId = 0;
	//for each (CBaseBranch::BranchPtr branch in branches)
	//{
	//	int iParentID = iLastId;
	//	if (count == 1 || count == 2 || count == 7 || count == 8)
	//		iParentID = iMenu1;
	//	if (count == 3 || count == 4 || count == 6)
	//		iParentID = iMenu11;
	//	if (count == 5)
	//		iParentID = iMenu112;

	//	log->LogString(LEVEL_INFO, L"Insert branch: %i, %i, %s, %i, %s, %s, %s, %s, %s, %s, %s, %i, %i, %i, %s, %s, %s, %i, %s, %i, %s, %s, %i, %i, %i",
	//		1, iParentID,
	//		branch->GetMenuName().c_str(),
	//		branch->GetTypeMenu(),
	//		L"",
	//		branch->GetDtmf().c_str(),
	//		L"",
	//		L"",
	//		branch->GetComment().c_str(),
	//		L"",//branch->GetWavName().c_str(),
	//		L"",
	//		branch->GetSubDialogId(),
	//		1,
	//		branch->GetRepeatCnt(),
	//		branch->GetGotoRepeat().c_str(),
	//		branch->GetGotoBack().c_str(),
	//		branch->GetGoto().c_str(),
	//		0,
	//		L"",
	//		0,
	//		branch->GetGotoRoot().c_str(),
	//		branch->GetTermChar().c_str(),
	//		branch->GetTimeOut(),
	//		branch->GetIsRoot()?1:0,
	//		branch->GetIsCondition() ? 1 : 0);

	//	if (!pAddProc->SetAllParam(1, iParentID,
	//		branch->GetMenuName(), 
	//		branch->GetTypeMenu(), 
	//		L"", 
	//		branch->GetDtmf(), 
	//		L"", 
	//		L"", 
	//		branch->GetComment(), 
	//		L"", //branch->GetWavName(),
	//		L"", 
	//		branch->GetSubDialogId(), 
	//		true, 
	//		branch->GetRepeatCnt(), 
	//		branch->GetGotoRepeat(), 
	//		branch->GetGotoBack(), 
	//		branch->GetGoto(), 
	//		0, 
	//		L"", 
	//		0, 
	//		branch->GetGotoRoot(), 
	//		branch->GetTermChar(), 
	//		branch->GetTimeOut(), 
	//		branch->GetIsRoot(),
	//		branch->GetIsCondition()))
	//	{
	//		// cannot set upload_data proc params
	//		CString Err;
	//		Err.Format(L"\"add_branch\" proc - error in incoming params");
	//		throw Err;
	//	}
	//	log->LogString(LEVEL_INFO, L"\"add_branch\" call");
	//	iLastId = /*iDatabaseCount++; */pAddProc->Call();
	//	if (iLastId == -1)
	//	{
	//		throw CString(L"\"add_branch\" proc - execution failed");
	//	}

	//	if (count == 0)
	//		iMenu1 = iLastId;
	//	if (count == 1)
	//		iMenu11 = iLastId;
	//	if (count == 4)
	//		iMenu112 = iLastId;

	//	++count;
	//}

	return true;

}


/******************************* eof *************************************/