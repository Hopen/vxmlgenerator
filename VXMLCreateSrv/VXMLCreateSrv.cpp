// VXMLCreateSrv.cpp : Implementation of WinMain


#include "stdafx.h"
#include "resource.h"
#include "VXMLCreateSrv_i.h"

#include "Patterns/singleton.h"
#include "WinAPI/mdump.h"
//#include "logger.h"
#include "Log/SystemLog.h"
#include <comdef.h>
#include "MainCreator.h"

using namespace ATL;

#include <stdio.h>

class CVXMLCreateSrvModule : public ATL::CAtlServiceModuleT< CVXMLCreateSrvModule, IDS_SERVICENAME >
{
public :
	DECLARE_LIBID(LIBID_VXMLCreateSrvLib)
	DECLARE_REGISTRY_APPID_RESOURCEID(IDR_VXMLCREATESRV, "{69C6A4DB-D652-4169-86D9-F0BBAD4B2F7C}")
		HRESULT InitializeSecurity() throw()
	{
		// TODO : Call CoInitializeSecurity and provide the appropriate security settings for your service
		// Suggested - PKT Level Authentication, 
		// Impersonation Level of RPC_C_IMP_LEVEL_IDENTIFY 
		// and an appropriate Non NULL Security Descriptor.

		return S_OK;
	}

		HRESULT Run(int nShowCmd = SW_HIDE) throw()
		{
			::CoInitializeEx(NULL, COINIT_MULTITHREADED);
			singleton_auto_pointer<CSystemLog> log;
			log->LogString(LEVEL_INFO, _T("Starting run"));


			HRESULT hr = S_OK;

			singleton_auto_pointer<MiniDumper> dumper;

			CVXMLCreateSrvModule* pT = static_cast<CVXMLCreateSrvModule*>(this);

			try
			{
				hr = pT->PreMessageLoop(nShowCmd);
				if (hr != S_OK)
					throw _com_error(hr);

			}
			catch (_com_error& error)
			{
				log->LogString(LEVEL_INFO, _T("Failed to Run service. Error code: 0x%08x. %s"), error.Error(), error.ErrorMessage());
			}


			DWORD error = ::GetLastError();
			if (hr == S_OK)
			{
				singleton_auto_pointer<CMainCreator> creator;
				long ret = creator->Init(log->GetInstance());
				if ((ret > 0) && m_bService)
				{
					LogEvent(_T("Service started"));
					SetServiceStatus(SERVICE_RUNNING);

					log->LogString(LEVEL_INFO, /*__FUNCTIONW__,*/ _T("======================================================="));
					log->LogString(LEVEL_INFO, /*__FUNCTIONW__,*/ _T("Starting IS III VXMLGenerator service 1.0.1.0 ..."));
				}
				else
				{
					hr = ret;
				}

				pT->RunMessageLoop();
			}

			if (SUCCEEDED(hr))
			{
				log->LogString(LEVEL_INFO, /*__FUNCTIONW__,*/ _T("Stopping IS III VXMLGenerator 1.0.1.0 ..."));
				log->LogString(LEVEL_INFO, /*__FUNCTIONW__,*/ _T("======================================================="));

				hr = pT->PostMessageLoop();
			}


			::CoUninitialize();
			return hr;
		}
	};

CVXMLCreateSrvModule _AtlModule;
LPCTSTR c_lpszModuleName = _T("VXMLGenerator.exe");



//
extern "C" int WINAPI _tWinMain(HINSTANCE /*hInstance*/, HINSTANCE /*hPrevInstance*/, 
								LPTSTR lpCmdLine, int nShowCmd)
{
	//std::vector <std::wstring> params;
	//params.push_back(L"131131");
	//params.push_back(L"abdbdfb");
	//std::wstring sNamelist;
	//std::wostringstream out;
	//std::copy(params.begin(),   
	//	params.end(),
	//	std::ostream_iterator<std::wstring, wchar_t>(out, L" ")
	//	);

	//sNamelist = out.str();
	//sNamelist.erase(sNamelist.size() - 1);

	if (_tcsicmp(lpCmdLine, _T("/console")) == 0)
	{
		::CoInitializeEx(NULL, COINIT_MULTITHREADED);

		singleton_auto_pointer<MiniDumper> dumper;

		singleton_auto_pointer<CSystemLog> log;
		log->LogString(LEVEL_INFO, _T("Starting run"));

		singleton_auto_pointer<CMainCreator> creator;

		if (!creator->Init(log->GetInstance()))
		{
			creator->InitTimer();
			MSG msg;
			while (GetMessage(&msg, 0, 0, 0) > 0)
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}

		::CoUninitialize();
	}

	if (_tcsicmp(lpCmdLine, _T("/writedatabase")) == 0)
	{
		::CoInitialize(NULL);

		singleton_auto_pointer<MiniDumper> dumper;

		singleton_auto_pointer<CSystemLog> log;
		log->LogString(LEVEL_INFO, _T("Write database"));

		CMainCreator::TestWriteDatabase();

		::CoUninitialize();
	}



	return _AtlModule.WinMain(nShowCmd);
}

