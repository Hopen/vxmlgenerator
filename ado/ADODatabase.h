// ADODatabase.h: interface for the CADODatabase class.
//
//////////////////////////////////////////////////////////////////////


//	AUTHOR: Carlos Antollini 
//
//  Changed by: Reinhard Dietrich

#if !defined(AFX_ADODATABASE_H__F15140E7_8D43_11D7_90BC_204C4F4F5020__INCLUDED_)
#define AFX_ADODATABASE_H__F15140E7_8D43_11D7_90BC_204C4F4F5020__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#pragma warning (disable: 4146)
// CG : In order to use this code against a different version of ADO, the appropriate
// ADO library needs to be used in the #import statement
#import "C:\Program Files (x86)\Common Files\System\ADO\msado25.tlb" no_namespace, rename("EOF", "EndOfFile")
#pragma warning (default: 4146)
//using namespace ADODB;  
inline void TESTHR(HRESULT x) {if FAILED(x) _com_issue_error(x);};


class CADOStoredProc;
class CADORecordset;

class CADODatabase  
{

friend CADORecordset;
friend CADOStoredProc;

public:
	CADODatabase();
	virtual ~CADODatabase();

   enum DbActions
   {
      doClose,
      doBeginTrans,
      doCommitTrans,
      doRollbackTrans
   };

public:
	static void	Msg(LPCTSTR sMsg);
	bool	Open(LPCSTR strDB, LPCSTR strUID, LPCSTR strPWD, LPCSTR strProvider = "MSDASQL", enum CursorLocationEnum curLocation = adUseClient);
	inline	bool Close() { return Perform(doClose); }
	bool	IsOpen();

//transaction support:
	bool	BeginTrans()	{ return Perform(doBeginTrans); }
	bool	CommitTrans()	{ return Perform(doCommitTrans); }
	bool	RollbackTrans() { return Perform(doRollbackTrans); }
	bool    Execute(LPCTSTR lpstrExec);

private:
	bool	Perform(DbActions doAction);

public:
	_ConnectionPtr	m_pIConn;
};


class CADORecordset   
{
public:
	enum Actions
	{
		doMoveFirst,
		doMoveLast,
		doMoveNext,
		doMovePrevious,
		doAddNew,
		doUpdate,
		doDelete
	};

	CADORecordset();
	CADORecordset(CADODatabase* pDb, int nCacheSize = -1);
	virtual ~CADORecordset();

	bool Initialize(CADODatabase* pDb, int nCacheSize = -1);

	bool Open(CString strSQL);
	CADORecordset& Close()	{ if (IsOpen()) m_pRS->Close(); return *this; }
	CADORecordset& SetCacheSize(int nCacheSize) { m_pRS->CacheSize = nCacheSize; return *this; }

	inline bool IsEOF()		{ return m_pRS->EndOfFile == VARIANT_TRUE; }
	inline bool IsOpen()	{ return m_pRS ? m_pRS->GetState() != adStateClosed : false; }
	long GetFieldCount()	{ return m_pRS->Fields->GetCount(); };
	long GetRecordCount();

	bool MoveNext()		{ return Perform( doMoveNext); }
	bool MoveFirst()	{ return Perform( doMoveFirst); }
	bool MoveLast()		{ return Perform( doMoveLast); }
	bool MovePrevious() { return Perform( doMovePrevious); }
//	bool AddNew()		{ return Perform( doAddNew); }
//	bool Update()		{ return Perform( doUpdate); }
	bool Delete()		{ return Perform( doDelete); }

	bool GetFieldValue	(short nIndex, _variant_t& vtValue);
	bool GetFieldValue	(short nIndex, CString& sValue);
	bool GetFieldValue	(LPCTSTR lpFieldName, _variant_t& vtValue);
	bool GetFieldValue	(LPCTSTR lpFieldName, CString& sValue);

// Reinhard Dietrich's extensions
	CString GetFieldName(short nIndex);
	long	GetFieldType(short nIndex);
	CString GetFieldLen(short nIndex);

private:
	CADODatabase*	m_pDb;
	_RecordsetPtr	m_pRS;

private:
	bool Perform(Actions doAction);
};

// Reinhard Dietrich's extensions
class CADOStoredProc
{
public:
	CADOStoredProc();
	CADOStoredProc(LPCTSTR spName, CADODatabase* pDb);
	virtual ~CADOStoredProc();
	bool	AdParam(LPCTSTR sParName, enum DataTypeEnum iParType, enum ParameterDirectionEnum iDirection, short iParamLen = 0);
	bool	AdRcParam();
	int		GetRcParam();
	bool	SetParam(LPCTSTR sParName, const _variant_t &vVal);
	bool	GetParam(LPCTSTR sParName, _variant_t &vVal);
	bool	Execute();
	//bool    Execute(LPCTSTR spCommand);
	bool	Initialize(LPCTSTR spName, CADODatabase* pDb);
	bool	Initialize(LPCTSTR spName, LPCSTR strDB, LPCSTR strUID, LPCSTR strPWD);
	CADODatabase* GetDb() {return m_pDb;};

	//template <class T>
	////decltype(auto)
	//_variant_t&&
	//	GetValue(LPCTSTR vName, T defValue)
	//{
	//	_variant_t tmpValue = m_pRS->Fields->GetItem(std::move(vName))->Value;
	//	if (tmpValue.vt == VT_NULL)
	//		return _variant_t(std::move(defValue));

	//	return std::move(tmpValue);
	//}

	template <class T>
	_variant_t
		GetValue(LPCTSTR vName, T defValue)
	{
		_variant_t tmpValue = m_pRS->Fields->GetItem(std::move(vName))->Value;
		if (tmpValue.vt == VT_NULL)
			return _variant_t(std::move(defValue));
			//tmpValue == std::move(defValue);

		return tmpValue;
	}

private:
	bool	GetParameter(const _variant_t& vtParameterName, _variant_t * pvtValue );

protected:

	_RecordsetPtr	m_pRS;
	CADODatabase*	m_pDb;

	bool Connect(LPCTSTR _proc_name, LPCSTR _connection_str, CADODatabase *pDB = NULL);
	virtual bool AdAllParam()=0;
private:
	_CommandPtr		m_pCommand;
	bool			m_bDbPrivateConnection;
};


#endif // !defined(AFX_ADODATABASE_H__F15140E7_8D43_11D7_90BC_204C4F4F5020__INCLUDED_)
